## The Reluctant Mistress and Her Unexpected Transition

> The Reluctant Mistress and Her Unexpected Transition, by [Virginia Ford](https://femdoming.com/author/msvirginiaford/)
> 
> Originally hosted at [Femdoming.com](https://femdoming.com/fiction/reluctant-mistress/)

### Part 1

I was the reluctant Mistress. It started after we had been dating a year or so, one Sunday morning after some delightful vanilla sex, when Danny leaned his head on an elbow and gave me his serious look, those dark eyebrows forming inverted V’s and lines of worry etched on his forehead. He said he wanted to say something important, something he’d been meaning to say for a long time, but he didn’t want to freak me out.

I sat up, wrapping the sheet around my breasts, no doubt the expression on my face quite alarmed, thinking that he was about to tell me he had cheated on me. Or that he was secretly gay.

#### Fantasizing about being dominated by a Mistress for a long time...

He stammered, not quite stuttering but his words coming it fits and bits, that he had secret longings to be dominated by a girl, by me. I said I didn’t know what that meant. He said that I was naturally a little bossy and that I would be a natural. I took offense and slapped him on the thigh.

“See what I mean?” he said with a crooked grin.

“That’s just because you were rude.”

“I’m sorry, that wasn’t quite how I meant it,” he protested, sitting up so that we were now facing one another, eye-to-eye. I loved his liquid brown eyes, deep and mysterious. “What I meant is you know what you want and you don’t suffer fools. Like that guy at Trader Joe’s, the check-out guy.”

The check-out guy at Trader Joe’s was trying to flirt, even though Danny was right there with me in line. A total jerk. I told him that guys with bushy nasal hair turned me off.

Back to Danny.

“Just what did you have in mind?”

He told me about the websites that cater to this, that there were books, even mistress manuals to give me pointers.

“So you want to call me Mistress?” I asked him.

He squirmed.

“Uh, well . . .” He finally choked out a strangled, “Yes, that would be part of it.”

“How long have you felt this way, Danny?” I asked him, taking his trembling hands in mine.

“Since I was an adolescent,” he admitted. “My first fantasies were of being tied up and spanked by a girl, her playing with me, jerking me off, or making me service her cunt with my tongue.”

“Wow.”

#### …that is probably why he gives such great cunnilingus!

![TheresAFaceUnderThere1.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/TheresAFaceUnderThere1.jpg?lossy=1&amp;strip=1&amp;webp=0)

No wonder he gave such great cunnilingus, I thought. Danny loved to go down, more than anyone I ever dated. Confession: I had already fallen in love with this boy, the most attentive lover I ever had, ten years younger, sweet, handsome as a model (well, like the guys you see in a Target clothing ad), a lithe swimmer’s body (yup, he swam in high school). We met at the auto repair shop where I took my Civic, the same strip mall as the bank where I am a loan manager. No wonder he was so eager to please, with those sort of fantasies running around in his head.

“You want me to wear leather outfits and whip you, then?” I said. Of course, I’ve seen the images. 

“Only if you want to.”

“I see.”

#### Even if I am a little bossy, do I want to be his Mistress?

Well, what a surprise. I was a normal girl with a normal sexual appetite, hoping to meet the guy I would marry and have kids with. Maybe I was a little bossy, but I grew up with three bratty brothers and babysat all the way through college. I knew how to handle misbehaving boys.

I gathered my thoughts.

“Danny, I like wearing lingerie as much as the next girl, but I’m not into leather and whips. I just can’t see myself being a dominatrix.”

He looked disappointed.

“Well, it’s not so much you playing a dominatrix as you expressing yourself in assertive ways without thinking twice,” he said. “It’s more about being assertive, and knowing that I’ll love it when you are.”

I looked at him dubiously.

“Just me being assertive will do it for you... but you said you have fantasies of being spanked and used.”

He squirmed again, blushing. I loved it when he blushed.

“Being spanked by you would be better than when you frown at me and look disappointed. I’d rather you just spank me and tell me why you’re mad. I hate it when I have to guess.”

Thoughtfully I looked at him. I was as guilty as the next girl about expecting a man to read my mind. The articles in women’s magazines said we should voice our true feelings. I loved Danny’s pert white ass, so well-defined by dark tan lines.

“So if I spank you when I’m mad at you, you’ll be happy?”

He swallowed hard.

“Only if you tell me what I did wrong when you spank me, make me repeat it and promise not to do it again.”

#### He wants me to spank him! So, let’s do it!…

God, I loved this boy, so eager to please. I knew he loved me back. It was written all over his face.

I made a decision.

“Okay, lay on my lap, face down.”

“Right now?”

“Right now,” I ordered in my sternest babysitter’s voice.

Danny looked shocked, but he did what I told him, crawling over my thighs and positioning himself so that he lay perpendicular to me, his head to my right. It didn’t seem right.

“Flip the other way, I want your head to my left, legs to the right, so I can use my right hand.”

He turned his head and looked at me inquisitively.

“Really?”

“Really,” I said firmly. “Do it.”

![OverVanessasKnee1.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/OverVanessasKnee1.jpg?lossy=1&strip=1&webp=0)

He gave a sigh, sort of a disbelieving chuckle but got on his knees and rotated the other way. I saw that his cock, so recently spent, was growing hard again. Fancy that—I’d never seen him recover so fast.

Now Danny was prostrate on my lap, his penis hard against my thigh, his butt in the air, breathing much harder than normal. I ran through the catalog of recent offenses; the worst I could think of was that last night at dinner he left crumbs on the floor, as he often did when he ate. He seemed to have a fork-to-mouth disconnect. He never thought to clean his crumbs off the floor. I had swept them up this morning while the coffee brewed. It was all I could think of at the moment.

#### Strangely, I enjoy a lot spanking him!

I drew up my courage and swatted him on the ass as hard as I dared.

“That’s for leaving crumbs on the floor when you eat.”

His body jerked with surprise. A red mark welled on his butt, but silence from the victim.

“What do you say?” I demanded, using the same voice I used on my babysitting charges.

“I’m sorry.”

That wouldn’t do. I swatted him again, this time with my palm cupped so that it made a satisfying harsh “thwack” noise. He flinched harder.

“When I spank you, say ‘Sorry, Mistress, what have I done wrong?’”

Pause.

“Sorry, Mistress. What have I done wrong?”

My hand stung a little, but strangely, I liked it. It was amazing, I felt exhilarated, letting it all come out.

“You eat like a pig and leave crumbs on the floor!”

I smacked him again. He flinched again, then blurted,

“I eat like a pig and leave crumbs on the floor.”

I slapped his butt again.

“No, that’s wrong. Say, ‘Sorry, Mistress. I won’t eat like a pig or leave crumbs on the floor ever again. I’ll pick up after myself.’”

“Sorry, Mistress. I won’t eat like a pig and I promise to pick up my crumbs from the floor.”

“Better.”

I rubbed his ass, feeling a little guilty about the red blotch. But his cock was rock hard, I could feel it throbbing against my crotch. Obviously, spanking aroused him. Even more surprising, I felt myself growing wet. Surprise, surprise.

#### Surprise, surprise, I feel myself growing wet!

“There’s another thing,” I said, my hand raised above his ass.

Danny looked back at me, his face flushed. He saw my arm lifted in the air and the expression was almost terror. I suppressed the urge to laugh; here was a grown man, half again my size, afraid of me? Nevertheless, I brought my hand down just as hard as I could. A loud “thwack” reverberated against the walls of my small bedroom. His ass jerked up violently and he yelped in pain, buried his face in the sheets. A large welt rose upon his right cheek.

“Well?”

He lifted his head.

“Yes, Mistress?” he cried out, confused.

I leaned close, putting my lips against his ear. Softly I murmured,

“My sweet boy, that was for keeping your fantasies from me for over a year. From here on out, you are going to be completely honest with me, aren’t you? You’re going to tell me your every fantasy, aren’t you?”

He turned his head. 

“Yes, Vanessa,” he squeaked, sounding like a boy.

I spanked him again, this time on the left cheek. He cried out in shock.

“Yes, Mistress!” I corrected. “Look me in the eye and say it like a man.”

Those deep brown eyes locked on mine. I saw fear, but also, a look that could only be interpreted as gratitude, maybe even joy.

“Yes, Mistress,” he said formally, the way you’d speak to a police officer.

#### Now, I Own his cock!

“Roll over, Danny.” I wanted us both to see his throbbing erection.

“Yes, Mistress.”

He rolled over. His cock stood eight inches at rigid attention. I took it in my hand and squeezed it hard.

“And who owns this?” I demanded.

“You own it, Mistress,” he choked.

“That’s right, I do,” I said. “Your cock is mine from this day forward.”

I said those words as if reading a script that I had long ago memorized. It was one of those “aha!” moments when destiny assigns you a role you never knew was yours. Maybe I wasn’t a dominatrix, but I had never seen Danny so hard, and I liked it. A message for us both. I came to a decision.

“Danny, you’ve longed for this moment since the day we met, haven’t you?”

#### I have His Full submission!…

His chagrined expression was all the admission I needed to know it was true.

“Say it,” I spoke, squeezing his rock-hard member.

“I . . . I’ve always wanted this . . . yes, Mistress.”

“You want me to be in charge, you want me to own your sex. Say it.”

“I want you to be in charge of my sex, Vanessa, yes.”

I slapped his balls, making him wince. He grimaced.

“I want you to be in charge, Mistress.”

“That’s better.”

I climbed to my knees and carefully mounted Danny’s engorged cock. Looking down at his enraptured face I said,

“This is for my pleasure only. You may not cum without my permission, understood?”

#### No more Cumming without my Permission!

“Yes, Mistress,” he groaned, panting.

“If you cum, I won’t just spank you; I won’t let you have an orgasm for a month, understood?”

“I won’t cum, I promise, Mistress,” he said obediently.

“No you won’t . . . and if you do, I’ll lock you in a cage and throw away the key.”

My words shocked me; where did they come from? I didn’t even own a cage.

Danny’s eyes rolled back in his head; subspace. It was at that moment that I first tasted true feminine power, the realization that if I controlled a man’s orgasm, I controlled him, and he wanted me to have that power. It was intoxicating; a wave of sexual pleasure flowed through me. That morning I would come again and again, riding Danny’s cock. He would writhe in sweet agony, trying not to cum, and ultimately he would fail.

#### The Start to our New Life

And so we began, I the reluctant Mistress and Danny my , entwined roles that I have come to believe were destined in the stars.

### Part 2

Monday afternoon, I texted Danny a little before five p.m., just before his shift ended. The text was intentionally cryptic, “meet me” from your mistress, followed by the address of the local Tractor Supply Co. store, which is a country living store that carries just about everything imaginable, under headings like “Farm & Ranch,” “Home & Garden,” Trucks & Trailers” and “Pets & Livestock.”

It was the latter category that had caught my eye during an on-line search. To wit, I was thinking about purchasing “The Retriever Single Door Dog Crate,” a steel wire crate for pets over 70 pounds. It was just over 48 inches long, 30 inches wide, and 32 inches tall, which might serve as an adequate cage for my guy but I wanted to take a look before making the $89 investment.

#### Meeting at a local store to buy some new toys and…

Danny’s sun-faded blue Ford F-100 pickup truck was parked in front of the store when I pulled up. He got out and waved as I pulled up. I turned off the engine, released my seatbelt, and sat behind the wheel, the car speakers still playing Pandora (I know, I know, Spotify is better but I’m too lazy to switch). I waited for him to notice that I was not getting out. Eventually, he came around and looked inquiringly through the window. I rolled it down.

“Danny, a gentleman always opens the door for his lady.”

His face blanched. He looked crestfallen, then his features took on a genuinely contrite expression. He stuttered an apology as he opened the door, “Sorry, Vanessa.”

“You’ll be punished for this later . . . and call me Mistress.” The look on his face was priceless.

As we walked into the store, it struck me that Danny was not prepared for this; he was several moves behind. It was obvious that he had never really thought through what a smart girl like me might do with the reigns to a cute guy like him. Just as well, since I was winging it; one move ahead was all I needed.

I had him grab a shopping cart and instructed him to follow me. We walked down every aisle. When I saw something I thought might be useful (rope, snap links, screw-eyes, chain, a roll of wire, miniature padlocks, wooden paint stirring sticks, duct tape, dog collar), I dropped the item in our basket.

#### …A pet cage to reinforce his new submissive role!

![MenAreDogs1.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/MenAreDogs1.jpg?lossy=1&strip=1&webp=0)

Danny eyed my selections with a growing bulge in his crotch. He followed obediently. I saw a nice pair of size-7 women’s leather field boots and tried them on. They fit, so into the basket they went. No riding crops, but there was always the paint stick if my hand got sore.

Eventually, we arrived at the pet cages, stacks of them against a store sidewall. The biggest one looked just large enough. I checked the tag to make sure it was the same one I had found, reported in stock during my on-line search.

I took a deep breath and said to my love,

“Danny, why don’t you see if this cage is big enough.”

“Uh, you . . . you want me to get in?” he stammered.

After Sunday’s spanking/sexcapade, I had given serious thought as to how I was going to reinforce his new submissive role. Last night I had gone online and done a survey of BDSM sites. Humiliation was mentioned more than once. As for the term “pet,” it seemed sweet and loving. When I thought about him in the dog crate, it seemed perfect.

Using my firmest Mistress voice, I ordered:

“Yes, my pet, I want you to get in.”

His eyes darted up and down the aisles. Clearly, he was panicked that someone might see him crawl on his hands and knees into the cage. Who would do that? (Obvious answer: only a submissive.)

#### I ask him to try the dog crate to see if he fits in!

“Are you going to submit to me or not, Danny?” I said crossly. “This isn’t negotiable; either you do as I tell you when I tell you or I’m not going to be your Mistress.”

Then, I crossed my arms. I was still wearing my office power suit—knit jacket, knee-high skirt, hose, pumps with two-inch heels, a square-cut, white silk blouse that revealed just a shadow of cleavage (all I possessed). I tapped afoot for effect.

My boy opened the cage door, did a quick lookup the aisle, waited until a woman with two children moved out of sight, then swiftly got down on his hands and knees and crawled in. He was so rattled that it didn’t occur to him to turn around and back in, so his rear stuck out the wire door.

I loved the sight of his pert tush in tight blue jeans, but still, I had to suppress the urge to laugh. Clearly, he would fit in the cage, if only barely. It would be uncomfortable; he had to kneel, no lying down. Perfect.

I almost felt sorry for him, but he asked for this.

“Okay, you can come out, love.”

He backed out as fast as he could and hastily stood up. I stepped into him, pressing my body to his, taking his finely boned jaw in my hand. I murmured,

“That’s a good pet.”

I gave him a sensuous brush of his lips with my own.

“We’ll take the cage.”

At the check-out counter, I made him pay for everything.

#### Back home to assemble the dog crate in the empty bedroom

Danny followed me home. I live in the older part of town in one of those cute row houses built in the 1930s that stands tall and narrow. It features a wet basement, a dry main floor with a small master bedroom, a cozy kitchen (no dishwasher), and a dining room connected to a fairly spacious living room. A covered porch out front. Adjacent to the kitchen, a steep flight of stairs leads up to two small bedrooms on the second floor. I use one for sewing (I make most of my clothes). The other room is empty but has a thick blue carpet. Here I set up a queen-sized Aerobed for guests. This room has a dormer window that looks out on the street, a tall closet with a single clothes rod, a storage shelf above it, and a small cubby with a door to the attic, where cardboard boxes rest on plywood sheets nailed to the rafters.

I had Danny assemble the dog crate in the empty bedroom. It seemed a little flimsy so I had him wrap steel wire (women’s intuition to buy the wire; aren’t we geniuses?) around every seam and corner, and tie off the free end with a twist like I’d seen him do safety wire on critical nut and bolts. While he worked, I went downstairs to change into a pair of capris and a sexy halter, meaning my nipples showed through the fabric. Not dominatrix leather garb, but it would have to do. Upstairs, Danny had finished. He stood awkwardly next to the cage, pliers in hand. I took the pliers, gave him a lingering embrace, and then whispered in his ear,

“I want you to strip naked and get inside your cage, pet.”

#### “Get on your hands and knees and back yourself into the cage, Danny.”!

Danny stripped, trembling. His dick popped out like a pole when he tugged down his underwear.

“My, oh my, aren’t we excited?” I teased.

He stood there, seemingly confused about what to do. A suddenly stupid sub, it seemed.

I sternly ordered,

“Get on your hands and knees and back yourself into the cage, Danny.”

He got down and backed in until his toes touched the rear wall of the cage, just enough room on all fours to close the wire door about six inches from his face. From the shopping bag, I fished out two mini padlocks and locked them around the vertical wires that formed the edges of the gate.

Danny’s hard-on was magnificent, the tip of his dick touching the metal floor pan, dripping cum.

![InhaleMyWorkoutStinkPart14.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/InhaleMyWorkoutStinkPart14.jpg?lossy=1&amp;strip=1&amp;webp=0)

#### Confined inside his cage as punishment for his bad behavior towards his Mistress, me!

“Can you tell me why you have been confined inside your cage, pet?”

“Because I came yesterday, Mistress?” Danny postulated. Brilliant boy.

“That’s right. And you failed to open my door like a gentleman. And you failed to call me Mistress. You’ll be confined to the cage for an hour as punishment.”

“Yes, Mistress.”

I rattled the padlocks, making sure there was no way he could open the gate. I examined each of the corners and the seams where the wire panels joined. Danny had wired them thoroughly; no way it was ever coming apart; he was locked in for the duration. I had read that subs can’t help themselves when they are locked up; they are compelled to try to escape. If they can’t, into subspace they go.

“You’re not getting out, pet, I had someone very handy make certain of that, so you might as well relax and spend your time in there thinking about how you can be a better submissive.”

“Yes, Mistress,” Danny replied, sounding grateful, which I’ve read is good for the soul.

#### Last instructions before I left him alone!

God, he looked cute in there. I wanted to climb inside and jerk him off. But no room for that, barely room for him.

“I’ll check on you from time to time, pet,” I said, “You’ll hear me coming up the stairs.”

“Yes, Mistress.”

I had told him that I would be checking on him to reassure him, but now a sexy idea occurred to me.

“Each time I come upstairs, I expect your cock to be hard for me. You may stroke yourself if you need to, but I always want to see you hard in my presence. Is that understood, pet?”

From the look on Danny’s face, I knew I had hit a button: subspace.

“Yes, Mistress.”

Another idea. I bent over, slipping my halter off the shoulders so that the areolas and my nipples became exposed. Then, I taunted,

“I want you to think about these, .”

Calling Danny “slave” seemed remarkably natural, a term of endearment, a variation on “pet.” When you have your man in a cage, I guess you can call him what you like.

#### Being a dominatrix was more fun than I ever imagined

I could see the longing in his eyes; he was imagining sucking on my nipples. Danny once confessed that he preferred small breasts. Mine was a 32C. I had always been a bit self-conscious about their size until Danny. He liked me to wear bras, not because I needed them but for effect.

“If you are a good boy and behave yourself, perhaps I’ll let you suck on them when you get out,” I teased.

He licked his lips hungrily. God, he was incredibly horny.

“I . . . I’d love to, Mistress.”

“Good.”

Then I marched down the stairs, making sure my footfall sounded on each step. Slamming the door at the bottom for good measure, so Danny would know he was alone, locked upstairs in his cage, completely dependent on me to return. I paused, my hand lingering on the old crystal doorknob, and I realized I was smiling. I had just learned how to put lightning in a bottle—you stuffed it in a wire cage. Being a dominatrix was more fun than I ever imagined. What would I think of next?

### Part 3

Needless to say, an hour later when I let my dear submissive boy out of his cage, he was as hard as a man can get. But we didn’t have sex. Nope, not this mistress. I had prepared supper (I love cooking and sitting with Danny at the dining room table, chatting about the events of the day). Our meal was ready and I didn’t want it to go cold. Besides, if this submission thing was going to be on my terms, then it was my terms, right?

![thumb_YouWereMeantToCleanFeet6_640x360.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/thumb_YouWereMeantToCleanFeet6_640x360.jpg?lossy=1&strip=1&webp=0)

I sent Danny to the shower. I told him to take a cold shower and I’d be in to check. And make it quick. As I set the table I heard the water running, so I ran into the bathroom, pulled back the shower curtain (my shower is in an old-fashioned tub), and there he was, taking huge gulps of air under a spray of cold running water, his dick still hard as a rock. I rubbed my hand on his ass.

“Good boy.”

#### Diner time

Then I gave his left cheek a stinging slap that made him wince.

“Supper is ready, pet. Don’t be long.”

At the table, I had set two bowls of soup. Danny’s place was always immediately to my right. I have always sat at the head of the table; it was nothing conscious, it simply seemed the logical place to sit when eating alone.

When Danny came into my life, I kept the habit and he sat next to me where we could brush knees. I liked being close to him. When Danny came into the dining room, his hair was tousled and still wet. He wore blue jeans and a hooded sweatshirt and was a little out of breath. I stood at the head of the table, waiting behind my chair. He’s a quick learner. He came around to move my chair back and seat me. As I sat I noticed him looking down, eyeing my breasts.

“Thank you, Danny.”

“Yes, Mistress.”

Good boy. I hadn’t thought out the naming protocol entirely yet, but I was thinking that in public, around other ears, I would have him call me Vanessa, otherwise, when at home or in private, ‘Mistress.”

#### “Time Out” arrangement in our new female lead relationship!

I was also thinking about a ‘time out’ arrangement, where if I called time out, he would revert to Danny and I would just be regular old me, Vanilla Vanessa. But not just yet.

“Danny, I want you to be my pet just a little longer, okay?”

“Yes, Mistress.”

“Move back your chair and put your bowl on the floor.”

That put a quizzical expression on his face, but he did as he was told.

“Now get on your hands and knees, pet. I want you to eat at my feet.”

He got down on his hands and knees. I noticed he wore loafers, no socks. In a rush, poor thing. Well, I looked down on him, his head poised over the bowl, and it was another Deja-vu moment where I knew exactly what to say, it all seemed so familiar.

#### Mistress’ Pet on the floor during the diner

![SlopForThePig1.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/SlopForThePig1.jpg?lossy=1&strip=1&webp=0)

“Danny, my pet, unless I instruct you otherwise, you will always kneel at your place on the floor, at the table to my right. You will eat like a dog, lapping up or eating your food without using utensils, is that understood?”

“Yes, Mistress.”

“Now, while you lap up your soup, I have some things to say to you.”

“Yes, Mistress.”

“Go ahead, start eating.”

Danny began slurping up his soup (quite messily, I might add). I spread my napkin on my lap and spooned the vegetable-rice broth into my mouth. Delicious. I put the spoon down.

“My pet, I have been thinking about our new arrangement and I have an idea—no, more like a procedure—that we are going to put into practice.”

He looked up, soup dripping down his face, looking just adorable.

#### The practice of the new procedure

“Yes, Mistress?”

“It’s called a ‘time out’. When I call time out—and only I can call it—we will revert to the regular you and me, Danny and Vanessa. That way we can enjoy each other’s company in a normal fashion, and be a regular couple. Understood?”

I saw a flash of disappointment on his face, but he said nothing.

“Then, when I want you to revert back to being my slave, I will tell you ‘time in,’ or I’ll instruct you to get on your knees, just as you are now. Is that understood?”

He looked a little confused, poor thing. I gave him some help.

“The correct response at all times when you are on your knees or otherwise in your submissive role is ‘Yes, Mistress.’”

“Yes, Mistress.”

“Good. Now let’s practice. , get up.”

#### You are Released! That’s the same as ‘time out!

He stood up. His crotch was almost at eye level. I couldn’t help but notice there was a huge bulge behind the buttoned fly.

“My sweet pet . . .”

I reached out to caress his hard crotch, and I meant to say “time out” but the words that came out of my mouth were different:

“Danny, you are released.”

He looked confused.

“That’s the same as ‘time out,’ sweetie,” I explained hastily. “If I say to you, ‘you are released,’ it’s the same as ‘time out’. Actually, I think it’s better because it might be that I am releasing you from slave status for longer than a few moments, maybe for the day, or longer.”

Now he looked disappointed. Almost a pout.

“Danny, you are released. Pick up your bowl and have a seat next to me.”

“Yes . . . sure, Vanessa.”

He picked up his bowl, wiped his face with his napkin, and sat at his place next to me. I took his hands. I could tell he had this fantasy of a 24/7 slave status, constantly in a fevered state of arousal. I liked him being super-aroused, but I also liked him just being my guy. How else could I bitch to him about the stuff that happened at work? For that, we had to be equals. Besides, when he was a sub, he was sort of stupid. I like having a bright, handy guy around.

#### I like you being my submissive slave but it has to be on my terms, not yours!

“Okay, so I’ve been thinking about this a lot,” I began. “Danny, I like you being my submissive slave. It’s more fun than I imagined, but if we’re going to do it, it has to be on my terms, not yours.” (I hadn’t yet learned the expression ‘topping from the bottom,’ but this was the subject at hand.) “Danny, I know you want me to be your mistress, and the truth is, I kind of like it.” I smiled. “I definitely like how turned on it makes you, and that turns me on, too.”

He nodded.

“But if you really want a Mistress, then you have to accept that it has to be on my terms when it works for me. Otherwise, it’s just your fantasy. You want it to be real, right?”

He cleared his throat.

“Yes, I do, Vanessa. I get it . . .”

He looked down at his lap, lost in thought, and it appeared to me the bulge began to deflate, a little, anyway. Then he lifted his eyes to mine and said :

“Vanessa, I love you. No, I adore you. I can’t tell you how grateful I am you’ve gone along with this. You are really good at it, better than my wildest imagination . . . I understand that it can’t be all the time, or we would both probably burn out. So sure, you decide how and when I accept that.”

I leaned over and kissed him.

“I love you, Danny.”

“I love you too.”

I brought his hands to my lips, kissed his strong fingers.

“Thank you for understanding.”

I looked at him playfully.

“There’s another thing I want to share, something I’ve learned since we started this.”

#### Always subject to my will!

He looked at me inquisitively, eyebrows lifted, inviting me to continue.

“You are right; I am a little bossy, I like being in charge. I like that I have this power over you. I like that if I get mad, I can just order you over my knees and spank you, tell it like it is. That part I want to be 24/7 . . . the ability to order you to shut up and kneel at my feet at any time. Are you willing to accept that, that you are always subject to my will?”

His face lit up with gratitude.

“I would love that, Vanessa.”

I felt a little mischievous.

“Good. Then let’s practice. “Danny, get on your knees.”

He looked startled, but pushed back his chair and got on his knees, facing me expectantly.

#### Your first lesson is complete!

“No, what I mean when I say that is that I want you on your hands and knees.”

He dropped down, hands on the hardwood floor. Better, but that wasn’t quite it.

“No, what I want is you on your elbows and knees, arms out, head down and butt up, facing my feet.”

He turned to face me, prostrating himself, his forearms splayed on the hardwood floor. Better.

“You may kiss my feet, slave.”

I wore sandals, pink socks. He kissed my cotton-clad feet tenderly.

“Good boy, now gets up . . . you are released.”

He got up and took his seat with a wry grin.

“So you understand how this works,” I said with a happy laugh.

“Yes, I do,” he said, smiling. “And I think you’re a sexy goddess.”

“Then your first lesson is complete. Now let’s enjoy our supper.”

### Part 4

Normally I do the dishes while Danny watches TV in the living room. All that was about to change. After a delightful supper, we cleared the table, stacking the dishes in the sink. Mistress’s sink is stainless-steel, a double-bowl; dirty dishes on the left, drying rack on the left. I put my hands around Danny’s waist, drawing him close.

“Danny dear, get on your hands and knees, please,” I said.

He hesitated for an instant, then got down on the tile floor. I looked at him kneeling there prostrate on the floor, thinking that I had to come up with a less wordy command. ‘Get in position’ came to mind. Only three words; I liked it. For now, I had him where I wanted him.

“You may kiss my feet.”

#### Sweet Tenderness

![GuardTheDoorDoggie1.jpeg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/GuardTheDoorDoggie1.jpeg?lossy=1&strip=1&webp=0)

He kissed them just as he had before, tenderly. I could tell he liked doing it. So, a foot fetish, too.

“Danny, my love . . . from now on you will be doing the dishes, understood?”

He looked up, not exactly happy.

“Yes, Mistress.”

“Don’t give me that look.”

“Sorry, Mistress.”

That pout he gave when he was bothered had always bothered me. So now, born of a hint of anger, I had an inspiration.

“, go upstairs and get the shopping bag from Tractor Supply.”

Danny rose to his feet. Upstairs he went, a short pause, then his footfall sounded down the stairs. He reported back to me with the bulging plastic bag in hand.

“Okay, now strip down naked.”

#### Strip!

He dutifully peeled off his clothes, seeming a little putout, but his cock grew hard before my eyes. There is a white painted chair under the triangle shelf in the corner that I use as a mini-office, stacking incoming mail. A junk drawer underneath. Above it, I have a wall phone (yes, I still have a landline) and a corkboard for post-it notes. I motioned to the chair.

“Bring the chair over here, please.”

He collected the chair and placed it in front of me. I sat primly on the wicker seat and motioned to him with my forefinger. “Now lie face-down on my lap.”

He looked at me, positively aghast. This was in the kitchen, not the bedroom, and it wasn’t sex play, it was clearly punishment. But he did as he was ordered, bent over my lap and waited, his pert white ass pointing up at the ceiling.

#### Spankings

![OverVanessasKnee3.jpeg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/OverVanessasKnee3.jpeg?lossy=1&strip=1&webp=0)

I lifted my right hand and gave him a firm swat.

“What do you say?”

“Thank you, Mistress.”

I swatted him again.

“Why am I spanking you, Danny?”

“Because I hesitated, Mistress.”

Another swat.

“Not just that. Because you pouted. No more pouting—ever—understood? If I tell you to do something, I want you to do it with a happy face. Got it?”

I sounded mad, and I was a little.

Contritely,

“Yes, Mistress.”

“Now I’m going to give you seven more spanks, and I want you to count them and thank me.”

I cupped my hand and smacked his right cheek.

He flinched.

“One . . . thank you, Mistress.”

Dumb slave.

“No, that’s the fourth spank, start from four, or we’ll start over.”

I spanked him on the left cheek, hard. He winced.

“Four, thank you, Mistress.”

#### How red can it get?

Better. I spanked him to the count of ten, his ass growing red. When we had finished, I ordered him to get back down on the tile on all fours.

“Danny, from now on, when I want you to get on your hands and knees, I’m going to tell you ‘Get in Position,’ understood?”

“Yes, Mistress.”

“And if I call you ‘slave’ at any time, I expect instant obedience.” I fished into the plastic bag for the dog collar. It was black leather, with a steel buckle and D-ring for the leash. “Sit up, slave.”

Danny sat up, his cock at rigid attention. Clearly, this wasn’t sub abuse; he loved it. And the truth is, I was getting off on it as well. Better yet, I had a dishwasher, something I had dreamed about for years.

#### Endless Possibilities since I am his Mistress!

Endless possibilities of forced labor ran through my head; weeding the overgrown back fence and raking up leaves, for starters. But for now, I had an idea to seal the arrangement.

“Danny, this is your slave collar. You’ll wear it whenever you’re inside this house unless I specifically tell you you’re released.”

I pointed to the junk drawer.

“That’s where we’ll keep it. I want you to put it on when you come in the door.” I smiled. “Now, kiss your collar, slave.”

He puckered his lips and smooched the collar. I looped it around his neck. I’ve walked enough dogs to know to leave two fingers’ space; I found the right hole in the leather, inserted the pin, then slid the free end through the buckle. I have to say, it looked nice on him. Went well with his hard-on.

#### My pet

“Okay, my pet, now back to the subject at hand. From now on, you’ll be doing the dishes. I expect you to clean both sides, dry them thoroughly so there is not even a hint of water spots, and put them carefully away. Do you think you can do that?”

“Yes, Mistress.”

“Good. I’ll be in the bedroom when you’re done.”

His face brightened.

“Knock on the door, then crawl in on your hands and knees, understood?”

His eyes flared. I think he was just now realizing just how all-encompassing this might be.

“Now get to work.”

I scooped his clothes off the floor, collected my laptop, and headed for the bedroom, filled with erotic ideas. I had some BDSM research to do, and maybe a little internet shopping, too. While Danny did the dishes, I was going to get another three steps ahead of him. And then I was going to get laid.

### Part 5

Thirty minutes was not nearly enough time to absorb even the faintest outline of the world of dominance and submission, consensual power exchange, kinky sex, et. all. And when the dishes were done and Danny came knocking on my door, I had better things to do than read The Mistress Manual, so permit me to summarize a year’s subsequent research in three small words: Looking for Love.

Yes—looking for love. Behind the chrome and leather, beneath the sweet surrender, is what seems to me to be the age-old search for love. “Love, love, love, all you need is love,”—as in the Beatle’s song. Yes, I know all you cynics out there are singing the chorus, “lust, lust, lust, all you need is lust.”

A pity you’ve become so jaded.

![thumb_CheckYourPrivilegePart15_640x360.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/thumb_CheckYourPrivilegePart15_640x360.jpg?lossy=1&strip=1&webp=0)

#### So jaded

Well, to each his (or her) own. This is a love story, a true story, and I must warn you that it doesn’t end happily ever after. So if you are just looking for something to help you jerk off, look elsewhere. Video porn, for example, god knows there’s plenty of that available. On the other hand, if your beloved loves you as I loved Danny, then our journey down this kinky road will be an enlightenment.

Not a technical enlightenment—there a vast BDSM resources available on the internet and in print. Google away. I’m talking about emotional enlightenment, the realm of the spirit. So along with that copy of The Mistress Manual, consider giving your beloved a copy of The Reluctant Mistress.

#### Why?

Besides the lucrative (ha) royalties, why am I doing this? Rejecting the notion of meaninglessness (Nihilists, I commend your courage), my purpose is to find meaning in loss. Heartbreaking loss.

If you and yours benefit in the slightest from reading this, if my brilliant insights, clever ideas and our moronic missteps help you to better navigate this dark terrain, if I hold before you a candle and it leads you to safety, then it will be worth all the heartache and keystrokes. Hold hands, please.

#### Introduction’s

Permit me to briefly introduce myself, your guide. My name is Vanessa Prudence Whetstone. About my middle name, alas, Mom had such high hopes. While that was a fail, I love my first name, which goes with my red hair and green eyes. And isn’t the last name just perfect? (Boys, think about it.) I went to UVA, got a degree in accounting, work in a bank, do yoga, and eat my vegetables. I’m pretty and petite. My three bratty younger brothers turned out well (contractor, lawyer, physician). Dad, a retired Army colonel, passed away last year. Mom went to Oregon to live with wild mustangs. So that’s all about me.

Moving on, the kinky toys arrived on Friday, in a discreet box from JT’s Stockroom. It might as well have been organic almonds from California. I placed the box on my bed and slit it open.

![2301_19_BU.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/2301_19_BU.jpg?lossy=1&strip=1&webp=0)

#### Kinky

Inside were locking leather wrist and ankle cuffs, a wide-end riding crop, a luxurious leather flogger, an adjustable spreader bar, Japanese clover nipple clamps (elegant looking), and my favorite, a stainless steel and leather cock cage. Why did I select these particular items? Because they looked so sexy on the models in the pictures, I suppose. And because they aroused a premonition of utility. Both reasons equally true.

I caressed my purchases, thinking about how I might use them. I loved the leather smell of the (very) expensive buffalo hide flogger. The nipple clamps looked a little vicious (and convinced me to start out with wooden clothes pins; I had plenty of those around). My favorite item was the cock cage, which seemed to me a perfect, portable version of the slave cage upstairs. Lightning in a bottle, remember?

#### Deja vu

When I looked at the image of a model’s handsome dick swelled inside the cage, that was another “aha!” Déjà vu moment—I knew I would use this device—but I have to say that the accompanying text, so matter-of-fact, is what sold me: “This cock cage is a very confining male chastity device. It will tend to limit erections and prevent orgasms. There is a locking leather strap that goes around the cock-and-balls . . . riveted to the strap are three more straps that attach to a cylindrical stainless-steel enclosure, known as a “stallion guard” (I swooned at that). The small cock cage had one ring in between the locking strap and the cage, the large cage had two. Which will it be, Madam?

#### Large

Of course, I went large. Now, sitting on the bed, I toyed with the narrow leather strap that would go around Danny’s scrotum. I brought the ends together, inserting the mini D-ring through a slot on the free end and locking the resulting leather loop with one of the mini padlocks we purchased on Sunday. Fingering the cock cage, it occurred to me that the biggest challenge would be how to put it on before Danny’s cock swelled up to its usual considerable proportions, making it impossible for him to put it on.

I realized I would have to surprise him with it. But how? If I tried to put it on him here at home, he would be impossibly erect before we even got his pants down.

#### Lovemaking sessions

![KL-208-highres.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/KL-208-highres.jpg?lossy=1&strip=1&webp=0)

After our lovemaking session on Monday night (yes, incredibly hot, especially with his collar on), I had given him strict instructions to not play with himself during the week—no masturbation. I wanted him hot and bothered for the weekend.

Home assembly wouldn’t do; I’d have to find another venue. Then it came to me: I had a spare key to his truck in the junk drawer. If I could get to his truck, I could put the cage under the driver’s seat and send him a text. That way he would be in a non-sexual setting (well, non-sexual other than that time we had sex in the cab). It would work, especially if he was just getting off work or leaving his apartment.

#### Early release

I checked my watch. Lucky me, I had gotten off work today at four; it was just now 4:30 p.m. The strip mall where we both worked was only ten minutes away. I wrapped the cock cage inside a plastic bag, grabbed my phone, purse and the spare key from the junk drawer and flew out the door. I made it to the auto repair shop and spotted Danny’s truck with fifteen minutes to spare, an hour to spare if he got bogged down with last-minute Friday oil changes and inspections, as was often the case.

Danny always went home to shower and change before coming over for our Friday night dates, so I had to think this through. OK, I’d put the bag under his seat, text him to call me before he came over.

#### Cock Cage

If he called leaving work, I’d have him put on the cock cage right there in the cab of his truck, before going home. If he called leaving the apartment, I’d have him put it on in the cab outside my place.

I parked in front of the floral shop two stores down so he wouldn’t see my car if he happened to be parking a car outside the garage for pickup. Feeling like I was in a spy movie, I made my way to his truck, staying behind shrubbery and parked cars so he wouldn’t by chance see me approaching. Jason Bourne would be proud. Fortunately, Danny’s truck is ancient, so I could open the passenger-side door without fear of triggering an alarm.

#### Key holder

I inserted the key, unlocked the door, and leaned across the filthy footwell (note to self: yuk, he needed to clean his floor mats), staying low and out of sight. The plastic-wrapped cock cage went under the driver’s seat. I high-tailed it out of there, mistress mission accomplished.

Then I texted Danny’s phone from the Honda before I drove off: “pet, call before you cum over.” That would get his attention. I had sent flirty texts and sexted him before, but this was the first time I had texted him as my submissive. My hands trembled; at the time I didn’t know why, I assumed it was the cloak-n-dagger element, but I was wrong. It was because of what the cock cage represented.

#### Chastity

Chastity. There are entire websites devoted to the topic. And some porny fiction. But in this context, the Mistress-submissive relation, it means the control of the man through his willing submission to your control over his orgasms. The power of this first became apparent to me when I threatened Danny with not just a spanking but no orgasms for a month, and then I blurted out that if he failed me I would lock him in a cage and throw away the key. That sent his eyes rolling back in his head, deep into subspace. There’s the big red master power button. A man wants you to own that power. Why?

#### Theory

Here’s my theory: men know they are dogs, and they hate themselves for it. But they just can’t help themselves, they need you to help them. Take charge of a man’s sexual desire and you help him to be a better man. His eyes may still wander, but that invisible bond of your power to grant his orgasms, that is ten times more powerful than the attraction of casual sex. Really. So when you relieve your man of this burden, he loves you for it. He wants you to have this power. I think this may be true of all men.

#### Awareness

![sub.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/10/sub.jpg?lossy=1&strip=1&webp=0)

The catch is that the bond must be absolute and real and must be actively attended, just as a man must attend to your emotional needs. For a submissive, a chastity device heightens the awareness of this bond; it adds intensity in what might otherwise be a boring day. Danny’s boyhood fantasies didn’t include chastity, they were about being tied up by a girl, used and abused, he would soon learn that enforced chastity is both erotic and brings masculine peace in a world of sexual temptation.

All this was just a woman’s intuition on that fateful Friday afternoon when I sent Danny that text. Somehow, I intuitively suspected this dynamic, but I didn’t know for sure.

#### Fateful Friday

When he called me as I drove home, it was obvious the ‘pet text’ had piqued his curiosity. I toyed with him, “There’s something I want you to put on before you come over tonight.” That evening, we were going out to dinner and then to a club to listen to a musician we both liked, in town for a one-night stand.

“Okay, I give, what. You want me to wear a tie?”

The club was casual, so I found it amusing Danny’s thoughts went to a tie.

“Well, not exactly a tie, but it will sort of have you tied up.”

A long pause. “You don’t expect me to wear that collar in public, do you?”

“No, silly boy. Collars are cute on Goth girls with tattoos, not guys. Yuk.”

“I’m just leaving work, heading home. Is it something I need to pick up?”

#### That’s an Order!

“It’s under your seat, Danny.” As I said this, I had a vision that the discovery of the cock cage under his crotch might totally distract him and he’d crash into oncoming traffic. “Danny, don’t reach for it now! Wait until you get back to your apartment. That’s an order!”

“Okay, I’ll wait.” He didn’t sound convincing. I could see him reaching under the seat.

“I mean it, slave!”

“Yes . . . Mistress.”

This Mistress/slave thing can save lives in a crisis. Especially since men never listen.

“It’s a gift, and I know you’ll like it, but I don’t want you crashing. Just wait, okay?”

“Sure.”

“Wrong answer, slave.”

“Yes, Mistress.”

#### Take off your clothes!

That was better. I continued, “When you get over here, I want you to take off your clothes, put them in a neat pile, put on your collar, and present yourself to me. I’ll be waiting in the living room.”

“Yes, Mistress.”

Now I had his attention. “Be at my house by six-thirty, understood?”

“Yes, Mistress!”

Ladies, we have the power to make the male world punctual.

I pulled into my driveway. “Ok, I’ll see you soon, pet. Be sure you’re wearing my gift.”

“Yes, Mistress.”

Forty minutes later I heard his wheels crunching on the gravel. I had put on my skimpiest black dress and those black leather field boots, which I thought might be clunky but actually looked amazing with the short dress. I ran over to the living room couch with my laptop, the browser opened to an online leather-and-lace store. Very sexy stuff.

#### Leather-less

I didn’t own a single leather garment and my lingerie collection was fairly tame. I was pretty sure Danny would happily buy me anything in the catalog.

I heard him open and close the kitchen door. Next the sound of him putting his keys on the shelf. Then muffled sounds; he was stripping down. A pause. Folding his clothes neatly. Another pause. The sound of the junk drawer opening. Putting on his collar. Good boy. He’d make his way into the hallway, turn the corner and see me waiting for him on the couch in my sexy short dress and tall boots. I heard the sound of his bare feet padding down the hallway . . . then it stopped. What was he up to?

#### Surprise

To my surprise, when he entered the room he was on hands and knees. He crawled forward until he was in front of me. He lowered himself, eyes cast down, forearms outstretched, a few inches from my booted feet. All I could think of was, ‘good slave.’ He had his collar on. I was dying to see the cock cage. “You may kiss my feet, slave,” I said. I put my feet together, so that the tips of the boots were under his lips. He kissed them passionately. Clearly his foot fetish included boots, too.

“Rise, slave,” I said, sounding a little like Cleopatra. What the heck.

He lifted up on his knees. Wow. He looked better than the Stockroom male models, a smoldering, horny expression on his face, lean physique, muscled chest, taut abs . . . his cock was bulging; it looked like it was going to explode through the steel wire cage.

#### Loved It

Poor thing. I loved it. Loved it.

“My, don’t you look handsome, slave . . . I see you found your gift.” I patted the couch next to me. “Come, have a seat. I want you to pick out some nice things for me.”

### Part 6

Ironically, the singer mistress and I went to hear that Friday night was named Danny. Unlike my Danny, this one’s last name is Schmidt, a former local singer/songwriter who found fame in Austin and gained a national following. I first heard him on Pandora. His voice is okay, nothing special, but his songwriting is exceptional, accompanied by melodic guitar, a modern-day Bob Dylan. Danny got hooked on him, too. We sat on folding chairs in the low-ceilinged basement hall of the Southern Café and listened to songs of love, lost and found, of money earned and stolen, of sickness and health, of days perfect and hellacious, of marriage and miscarriage, angels and devils, secrets and truth . . . the bittersweet songs of life.

#### Songs of Life

One row over, next to a pillar, sat a Goth girl, her head nodding to the melodies. She was striking—purple-streaked, coal-black hair cut at her jawline, silver rings in her nose and up her ears, a skimpy peasant-girl dress that revealed tattoos everywhere. The eagle on her left arm soared over her shoulder and a wingtip stretched across her throat. Better than a collar. I caught her stealing glances at Danny, not so surprising, girls do that all the time—his bad boy James Dean look is irresistible. I was used to it. What was hot about the situation was I knew that stuffed in the crotch of my bad boy’s tight blue jeans was a caged cock. Danny was mine, all mine; that steel cage around his manhood was a constant reminder to him. That Goth girl could have stripped naked and thrown herself at him, no dice.

#### Caged

He had worn his cage through dinner, he would suffer it through three sets at the Southern Café, and he would endure my merciless petting on the drive home. When I finally took the cage off his throbbing cock in my bedroom later that night, let me tell you, the sex was hot, over the top.

I even let him cum. Twice.

But not without consequences. I had been doing my reading, and I knew that when you let a sub finally cum, he goes into a sinker. There’s probably a better name for it, but ‘sub blues’ will do.

#### Sub Blues

I knew that when my Danny woke in the morning, he’d not only be spent from the night before, he would be feeling the blues. When you aren’t horny, what’s the motive for obeying your girlfriend? The antidote to this surly state of mine, I had read, was to take his mind off it with punishment and hard work. So while Danny snored away, I started the coffee, then padded down to the basement in my pajamas.

#### The Basement

My basement has a mostly concrete floor, dirt under the front porch, with a sump pump that goes into overdrive whenever it rains. At the top of the foundation walls, opaque glass bricks set at intervals provide a hint of light on sunny days. There’s a furnace, hot water tank, washer, dryer, copper plumbing, electrical wires, joists, and in the center, a 4-inch diameter vertical steel post that holds up the main floor beam. On this post I had noticed an oval-shaped eyelet welded just below the U-bracket that held up the laminated wood beam. I stood on my toes and to this eyelet I snapped a carabiner link.

Then back upstairs. In the kitchen I split and toasted an everything bagel and smeared it with butter and orange marmalade, poured two cups of coffee, put them on a tray, and carried it into the bedroom, flooding our bed with a delicious aroma that woke Danny.

#### Good Morning!

He sat up and rubbed his eyes. He was still naked. I snuggled next to him with the tray on my lap. I noticed the cock cage on the carpet in a corner where Danny had flung it as a prelude to our fevered lovemaking. We drank our coffee and shared the bagel, laughing about the sexy Goth girl and the sexy secret she didn’t know. When the bagel was eaten and our cups empty, I sprung my trap. “Danny, I’m still horny . . . do you have to go pee?”

This was our code for “I want to have sex.” From the look on my beloved’s face, I could tell he was not yet recovered from last night’s sexual marathon, for which I must give him all credit. Poor thing, he wasn’t going to be able to get it up for a while.

#### Sex Drive

Danny may have an exceptional sex drive, but he’s not superman. But there was a devious plan in my heart, born of love for my sub. I looked at him longingly, and with feigned disappointment said, “Well, why don’t you go pee anyway, we can still have fun.”

He got up, I think glad to escape my insatiable desire, if only for a moment. I heard his stream pouring into the porcelain toilet. No doubt spattering urine everywhere—another failing I planned to correct. For now, I ignored the sound and the visual of pee spots in my head.

#### Toy Chest

In the top drawer of my chest, I had stashed the leather wrist cuffs. While Danny peed, I hopped up and retrieved the cuffs, slipping them under the sheets. When Danny returned and crawled back in bed, I took hold of his limp member and stroked it while planting a passionate kiss on his lips. His dick stayed limp, as expected.

I lifted an eyebrow. “What’s wrong, pet, can’t get it up this morning for your mistress?”

He looked mortified. Better mortified than the blues, I thought. I let go of his penis.

“Danny, put your arms out, love . . . that’s an order.”

#### That’s an ORDER

He complied without resistance, sticking his arms out in my direction. I pulled the leather cuffs out and laid them on the sheets so he could see them. Surprise. I fumbled with the first cuff—it was more complicated that I thought; you’d think it would be simple but no—eventually got it properly wrapped around his wrist and the buckle fastened. Danny remained silent, somewhat bemused. The other cuff went much quicker. Then I snapped the little D-rings on each cuff together with the padlock that I had pre-positioned on one buckle. I removed the key and admired my handiwork. The leather cuffs were as effective as metal police handcuffs, and more comfortable for what I had in mind.

Taking Danny’s flaccid dick in my hand, I said, “Let’s go, slave. Follow me.”

I noticed an instant perking of the flesh between my fingers. Fancy that.

#### Basement Play

I led Danny by his dick out the bedroom, down the hall, through the living room and to the foyer inside the front door. The stairs leading down to the basement were opposite the front door, which I almost never used. I realized I had forgotten his collar and the flogger, both of which I intended to use. (Note to self: plan a scene like you plan a meal.) “Wait here,” I ordered, leaving him standing in foyer. When I returned, his cock was at half-mast and rising fast. I put the collar on him, snugging it down a little tighter this time, not choking him, but tight enough that he felt the leather against his throat.

#### Flogging

I kissed him on the cheek. “Better.” I took his dick—now it was firm—in one hand, the flogger in the other, and carefully led him down the stairs to the basement. When we got to the bottom, I flipped the light switch, which turned on a single 100-watt bulb which hung in the basement center. Looking at Danny’s face by the light, I could tell he didn’t have a clue of what was coming next. I led him to the steel post, sticking the flogger in the waistband of my pajama bottoms like a pirate would her sword.

“Lift your arms, high as you can, against the post.” He did as I told. I left him and retrieved the step ladder I kept by the washer and dryer.

#### Step ladder

Then brought it over and stood on it, lifting his cuffed arms another six inches, bringing him up on his toes. I snapped the carabiner around the linked cuffs. I hopped down. Danny was on his toes, his arms outstretched above him. He looked very sexy, that beautiful lean body arched and straining. Amazingly, his cock was fully erect, sticking out to one side of the pole.

I took his cock in my hand and stoked it, making him moan. I said in a matter-of-fact voice, “So slave, now that I have your full attention, let’s review the plan for the day.”

“Yes, Mistress,” Danny panted.

“After you’ve completed your time down here, you have chores to do.” I kept stroking his cock and it kept getting harder. “First, you’ll clean the toilet bowl of all your pee stains.”

“Yes, Mistress.”

#### Chores

“Then I want you to rake up the leaves in the backyard.”

“Yes, Mistress.”

“Then you’re going to clean out the cab of your truck. The floor is filthy.”

“Yes, Mistress.”

“Then we’re going over to your place. It’s a pigsty, and you’re going to clean it up while I supervise. You’ll be naked and wearing your collar and cock cage while you do.”

His cock was like carved wood in my hand. Oh, the power of chores to arouse the submissive beast.

I extracted the flogger from my waistband and lifted it to his nose. I wanted him to absorb the rich leather fragrance and remember it. “Smells nice, doesn’t it?”

He inhaled. “Yes, Mistress, it does.”

#### Teasing

I got behind him and caressed his beautifully muscled back with the strands of the flogger, ran the handle down the deep furrow of his spine. “I’m going to give you a flogging now, slave . . . not because you’ve done anything wrong, simply because you are my possession and I can do what I want with you.” I added an afterthought, “And because you weren’t hard for me this morning.”

Ouch. That wasn’t quite fair. But he offered no retort.

“What do you say?” I demanded, wanting him to bring up last night in his defense.

“Thank you, Mistress?” he ventured, saying it with a sarcastic tone in his voice.

#### Humor, Yes. Sarcasm, NO

Humor, yes. Sincerity, yes. Sarcasm, no. Without warning, I gave him a swift, stinging swat that made him flinch. Then another. And another. And another. In a few swings I found out just how close to stand and how hard to swing. Something came over me, I found a rhythm, I went into the zone where my shadow dominatrix dwells. She was merciless. I landed blow after blow and didn’t stop until Danny’s poor back and ass were inflamed, one big red blotch. He took it like a man, gritting through his teeth.

It was like an aerobic workout. When I finished I was gasping for air. Sweat beaded on my forehead. My pajamas clung to my body, damp with perspiration. I needed a shower. Without saying a word, I left Danny hanging there, cuffed to the pole. On my way out, I flipped the switch, leaving him in the dark.

Lord have mercy on my savage mistress ways.

### Part 7

#### Her Unexpected Transition

I took a long bath instead of a shower, sitting there in the steaming tub, feeling guilty. What had come over me? My first bout of mistress remorse. I reassured myself that I had done my homework well; I had read up on the differences between spanking, paddling, flogging, and the harsher options, caning and whipping, which were not for me. Spanking I found was intimate and sexy; you got direct feedback from your beloved’s ass. Paddling seemed like spanking only it spared your hand; that might be a future option. Flogging seemed relatively safe. The strands of the buffalo hide flogger were broad and soft. For all my blows, I hadn’t come close to breaking Danny’s skin.

I sunk back in the tub, looking up at the ceiling. I noticed it had a faint sheen of what looked to be mold. Damn. Then it occurred to me that I now owned a slave who would scrub and paint my ceiling on command. Being a mistress had its benefits. I thought about Danny, strung up down in the basement, straining in the shadows, literally on his toes. My, it gave the cliché new meaning.

![HesVeryObedient3.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/HesVeryObedient3.jpg?lossy=1&strip=1&webp=0)

#### Time for Chores

After my bath I dressed in work clothes: paint-smattered jeans with tears at the knees, white tank top layered with a blue work shirt I had borrowed permanently from Danny. I wore sneakers, my hair in a ponytail. Not exactly dominatrix wear, but we had chores. I checked my watch and realized it had been 45 minutes since I left Danny. A spurt of panic shot through me that he might have been there too long, but I reminded myself that had he been in real trouble, he would’ve yelled for help. With the cock cage in hand and the slave collar stuffed in a rear pocket, I went down the creaking wooden steps.

Sunlight filtered through the glass block windows and cast a shadowed half-light into the basement. I left the light bulb off and paused and let my eyes adjust admiring Danny’s masculine profile, hanging motionless from his wrists. I listened to his steady breathing. I watched him for a minute, seeing him shift his weight from one foot to the other. I walked up softly and placed a hand on the cheeks of his behind. It was cool to the touch. He didn’t flinch. His back and ass were no longer red; only a pink blush remained. I spoke in a tender voice, “I see you’ve been a good boy down here, my pet.” “Yes, Mistress,” he replied in a hoarse whisper.

#### Caged

Thankfully, his cock was flaccid. I slipped the cage over his penis and slid it up against the base of the shaft. The leather straps that went around the base of his scrotum were another matter. Danny had put on the cage himself last night; now I had to learn to do it myself. Well, they were my balls now so I might as well get used to ‘mistress-handling’ them. With some trepidation I squeezed his scrotum and drew his balls away from his body, holding them at bay with my left index finger and thumb while with my right hand I maneuvered first one leather strap, then the other, bringing them together so they joined at the base of the scrotum. I pushed the D-ring through the slot and snapped the mini padlock in place.

Throughout this process, Danny stood silently, motionless, like a horse letting you brush it down. Not a quiver from his cock; it remained flaccid in the cage. I retrieved the foot ladder and placed it next to the pole. Standing on it, I reached up and swung in the gate of the carabiner to free the cuffs, letting Danny’s forearms come down. He groaned with relief. I’m sure the muscles of his arms and shoulders were screaming. “Get down on your knees, slave, hold your arms out in front of you,” I ordered.

#### On you Knees!

He dropped to his knees. I used the universal key that locked the padlock on his cock cage to open the padlock holding his cuffs. I undid the locking buckles and let the cuffs fall to the floor. There were compression marks on the flesh of his wrists, but no real damage. I gently caressed his wrists, cooing, “There, there, such a good boy to suffer for his Mistress. Now on your hands and knees.”

Danny dropped down on all fours, like a dog. It occurred to me that I needed a leash. I cast my eyes around, looking for a substitute. Nothing. Well, I’d just have to swing by the pet store this week. I could imagine the conversation: “I need a leash for my boyfriend” . . . “Honey, don’t we all.”  For now, I’d have to make do with verbal commands.

![thumb_Torture2_640x360.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/04/thumb_Torture2_640x360.jpg?lossy=1&strip=1&webp=0)

“Pet, I want you crawl your way over to the steps, then upstairs. Crawl through the living room and to the bathroom. I’ll be following close behind. Consider yourself on a short leash. You will crawl in my presence like a dog unless I give you permission to stand, is that understood?” “Yes, Mistress.” “Now move.”

#### My Handsome Pet

My handsome pet made his way on hands and knees across the concrete floor to the wooden steps. I picked up the leather cuffs and followed behind. He went up the stairs bent over, sort of a faux crawl, his gorgeous, pert ass inches from my nose. His leashed balls swung like a pendulum between his thighs. I noticed his cock was quickly swelling inside the cage, the glans of his circumcised penis now pressing against the head ring of the cage. Good doggy. On the main floor, he crawled through the living room and down the hallway to the bathroom, with me right behind.

At the threshold of the bathroom I ordered, “Heel.” Danny froze. “Good boy.” I caressed his back, then gave his butt a light slap. He flinched. “Now slave, as you can see, I’ve placed cleaning tools in the bathroom.” I pointed past his nose with my forefinger. On the floor at the base of the vanity was a plastic tub with a scrub brush, sponge, cloth rag towels, scouring powder and a spray bottle filled with a 50-50 water/vinegar mix I make myself. “I want you to scrub the bathtub, pet, then the toilet and floor. Use the scouring powder and the scrub brush first, then use the sponge and spray cleaner, then wipe everything down with those towels. I want it all squeaky clean.”

#### Lick it all Clean

“Yes, Mistress.” “Clean enough to lick, understood?” “Yes, Mistress.” “Now get to work.”While Danny worked in the bathroom, I prepared breakfast. I knew he must be starving; half a bagel was an appetizer after last night’s marathon, not to mention the calories he had just burned straining on his toes, not to mention the long day of work we had ahead. I was hungry, too. We would have our usual Sunday breakfast: a big bowl of oatmeal, laden with bananas, blueberries, raisins, walnuts and flax seed. I prepared the fruit and nuts while the water heated to a boil, then poured in the oatmeal letting it cook for few minutes and then turned the burner down to simmer. I returned to the bathroom.

Danny was facing away from the door, drying the floor, the muscles in his shoulders rippling as he made circular motions with the towels. I saw that his cock was flaccid in its cage. “Have you done a good job, pet?” My voice startled him. He rose up on his knees, turned to face me. “I think so, Mistress.”

#### Know your Place

“Slave, never rise up higher than your Mistress’s cunt unless she tells you to.” I had been doing my reading. “Keep your head low, butt up, eyes cast down, hands on the floor.” Danny quickly dropped down, the towels still clutched in his hands. I let that slide. The bathroom is narrow, so his prostrate body filled the space between vanity and tub. I cast my eyes on the interior of my vintage four-legged tub. The ceramic needed resurfacing, but it was as white as I had ever seen it, buffed to a soft glow. I edged past Danny to inspect the toilet. It shined, even down in the nooks and crannies at the base. Amazing that he could do such a fastidious job as a slave but as a grown man, the bathroom of his apartment was disgusting. Well, he would be retrained.

![IMG_0122.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0122.jpg?lossy=1&strip=1&webp=0)

#### Inspirations

With sudden inspiration, I kicked off my shoes, stripped off my jeans and panties and folded them on the top of the tank. I put down the seat and sat, legs bracketing the bowl. Danny faced away from me, his head pointed toward the open bathroom door, the soles of his feet by my right foot. “Slave, turn around and crawl over here to the toilet. Keep your eyes cast down at all times.”

Danny turned around, keeping his eyes lowered. He crawled on his hands and knees to me, turning himself in the narrow confines to face me straight on, his head lowered between my feet. Now he could see that my legs were bare. I felt like I was sitting at a throne . . . a very kinky white throne. “Lick the bowl, Danny.” I don’t know why I called him ‘Danny,’ and not ‘slave,’ but that’s what came out.

#### Danny

He tilted his head to one side and tentatively reached out with his tongue, pressing it against the bowl and giving a modest lick. That wouldn’t do. “No, lick the bowl like you would lick my cunt, Danny. Show me how well you cleaned the toilet, show me how much you want to lick my cunt.”

That set him off; he began licking the white ceramic bowl with fevered enthusiasm. I hoped he had followed my directions and used the vinegar spray after the cleanser; I didn’t want him ingesting any toxic chemical residue. “How does my toilet bowl taste, slave?” He paused, drew his tongue back into his mouth, swallowed. “It tastes wonderful, Mistress.”

#### Good, You may Continue

“Good. You may continue. Lick the entire bowl. Start from the bottom and work your way up, right up to the rim. Take your time, show me how much you adore your mistress.”I watched as Danny set to work, his tongue flared out against the white ceramic, making big slurpy stokes. The sight of it—I think it was the passion he put into the task—made me wet. I let him work his way up until he had elevated his chin to the level of the seat. I took his head in my hands. “Enough.”

#### He obediently retracted his tongue.

“Look in front of you, slave.” Danny raised his eyes, getting a good look at my exposed cunt. “I want you to work up some saliva and spit into to bowl,” I instructed. I watched as he worked his tongue around the inside of his mouth, building a wad of spit. “Now spit it out.” He spat out a wad of whitish saliva into the bowl.

“Good, now you may lick your Mistress’s cunt.” I pulled him closer, so that his chin came over the rim of the bowl. I guided his mouth against my wet cunt. His tongue pressed up against the lips of my labia and reached deep inside the vagina. “Softer,” I commanded. “Move your tongue up higher, where my clit is.”

#### Softer Danny, Softer

As enthusiastically as Danny went down on me, he never quite got this right. Now I felt empowered to guide him directly to the spot and make him do it right. Over the next several minutes, I gave him explicit instructions, how hard, how fast, up, down, lighter strokes, circular strokes, now cup your lips around the clit and gently suck while tapping the tip of your tongue against the engorged head, it’s just like your penis, don’t you know (of course, he didn’t).

With him on his knees and the cheeks of his face firmly in my hands, these instructions came naturally, without any of the inhibition that had always constrained me in our vanilla lovemaking. Do it this way, just so. Before I knew it, that tingly, welling feeling rose up, filled my loins and exploded deep inside my vagina. I came so hard my legs went weak. Oh, my god it felt so good. And shocker—I felt myself squirting. Never have I done that before.

I tried to push Danny’s face away, but bad slave, he resisted and drank it all up, just like I swallowed his cum when he came in my mouth. Relaxing, I let him ‘worship at my temple’ (I read that phrase in a dominatrix tome and kind of liked it). I tilted my head back in bliss and gave thanks to heaven that I had finally cum with Danny’s mouth on my cunt. No more faking it. Hallelujah. I looked down and saw that his cock was engorged, the flesh bulging out between the cage rings, his glans poked completely through the head ring. Wow. I wondered if that hurt. Well, if it did, he was oblivious to the pain.

#### Your Pain, Her Gain

Just at that moment the smell of oatmeal wafted into the bathroom. Yeah, I know, it should’ve been roses, but it was oatmeal, and hopefully it wasn’t burning. This time I pushed Danny’s face away more firmly. I put a hand over his mouth and looked him deeply in the eyes. “Thank you, slave, that was really nice. Now finish up in here and then crawl to your place in the dining room.”

In the dining room, I made Danny eat from his bowl set on the floor by my feet. His cock still bulged in the cage, so clearly he got off on being treated as a dog. I would definitely have to get a leash. I was learning to appreciate that cage more and more. Not only was it a chastity device, it made erections look super sexy while giving me a visual barometer of my slave’s arousal. Not that a slave always had to be aroused, but when he was, the cage was an immediate, unrelenting reminder of who owned his cock.

After he had done the breakfast dishes, I had Danny stuff himself into his blue jeans, put on socks and shoes and a sweatshirt to go outside. I made him keep on his slave collar. With a five-foot wooden slat fence (that badly needed whitewashing, another project for my slave), my backyard is reasonably private; no one would see him wearing the collar unless they came up to the gate and looked in. I was willing to take that chance for the sake of Danny working with the threat of humiliation hovering in the air, a bit of erotica to heat up the cool autumn air. As we raked and collected leaves, we looked like an ordinary vanilla couple, except that my man wore a collar and had a caged cock bulging in his pants.

### Part 8, The Rewards of Cleanliness

Mistress: As I may have mentioned in passing, the cab of Danny’s truck was filthy. After we raked and bagged the leaves, that was next on my list. I sent him out to the driveway with a soap-filled bucket, sponges, scrub brushes, wash rags, cleanser, Formula 409, cloth towels, roll of paper towel and his cell phone.

“Text me when you’re ready for an inspection,” I told him.

He fingered his collar. “May I take this off, Mistress?”

I told him no, his cab was so filthy he deserved to be humiliated. He’d just have to deal with it if one of the neighbors came over to say hello. Danny wasn’t happy about that, but he didn’t argue.

#### Collared

![IMG_0125.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0125.jpg?lossy=1&strip=1&webp=0)

While he cleaned the cab of his truck, I went inside, opened the laptop, caught up on email and did some more BDSM reading. It’s amazing the resources available now. And the communities that have sprung up in every city, it’s almost mainstream. But for me, this was a private affair, between me and Danny. I suppose if he took up skydiving, I’d probably give it a try. Who knows, maybe I’d love that too.

I knew I had a lot to learn. One question I really wanted answered was how could I get feedback from my submissive without relinquishing control. If you asked your slave how it was going, it seemed to me you were at risk of undermining your own authority, letting him “top from the bottom.” What were the right questions to ask your slave and how did you ask them and not show weakness? I knew I wasn’t the first to be asking these questions, so I went ahead and ordered three of the best-reviewed books on the subject: The Mistress Manual, The Hesitant Mistress, and The Sexually Dominant Woman.

In the meantime, I decided to make my own BDSM survey, and hand it to Danny to fill out, as if it were something I had found on the internet. It would have “tell your Mistress” and “list your fantasies” sections and a “kink interest” checklist. That way he would not be put on the spot, he could answer my questions as if someone else were asking them. The survey would serve as a kind of intermediary, giving us a degree of separation. Danny would be free to write whatever he pleased; I would read and know his deepest desires and be free to act on them, or not, without obligation. I stayed in control.

#### Control

If you are wondering, yes, I got Straight-A’s in high school and college. Smart, sexy and nerdy.

I was 15 minutes into the “Mistress/slave Survey” project and getting into it when the phone dinged with an incoming text. It read, “Ready for inspection, Mistress. Hope I don’t have to lick.”

Ha, that was better. Humor, yes.

I found Danny standing next to the open passenger door of his truck, the soap bucket, bottles and cleaning implements arrayed at his feet. He wasn’t kneeling, but then I didn’t expect him to be. At least he had kept his collar on. The alley was empty though, so what the hell. “Get in position, slave.”

Danny looked at me with a stunned expression, but he did as he was ordered. He got down on his knees in front of me, head down, butt up, hands flat on the gravel. To be honest, his instant obedience sent an erotic thrill through me. I liked giving orders and seeing them obeyed without hesitation. It was intoxicating. Even so, I didn’t want the neighbors coming around the corner and seeing him that way. “Good slave, now you can get back up. Stand by the door while I inspect.”

#### Good Slave, Now wait while I inspect.

![IMG_0126.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0126.jpg?lossy=1&strip=1&webp=0)

Danny hastily rose to his feet, brushed his hands on his pants legs and stood to the side. I looked inside the cab. It was as clean as you could reasonably expect, the rubber floor mats gunk-free and all the spilled soda and coffee scrubbed off the upholstery. The stains would be forever, but at least no more sticky places. And no more burger wrappers and cups under the seats. Even the leather boot around the stick was clean. For the cab of an old truck, it was as spic-n-span as you could ask for.

I backed out of the cab and stood up, turning to face Danny. “Come here, slave.”

Danny took a step closer, not knowing what to expect. I embraced him with my arms wrapped around his neck and kissed him on the lips. “Nice job, slave, you don’t have to lick,” I said with a smirk. Then I unbuckled the collar and removed it from his neck. “We’ll put this back on at your place, slave. Now put all this cleaning stuff in the bed of the truck and let’s head over.”

#### Danny’s Apartment

Inside Danny’s apartment, I locked the deadbolt and ordered him to his knees. I had him kiss his collar and put it around his neck, made him strip down and get in position. I left him waiting by the door while I surveyed the apartment. It was a mess: dust bunnies on the floors, dirty carpets, filthy couch, the kitchen a disaster, the bedroom a war zone, the bathroom post-apocalypse; no wonder I hadn’t gone over to his place in months, not since our initial dating honeymoon when I was willing to overlook such domestic squalor for the sake of a good fuck. A bad boy who’s a good man is irresistible.

Semi-furious (but not totally outraged; I expected this) I strode up to Danny, looking down at his naked, prostrated body, his raised ass ripe for a spanking. “Danny, your place is disgusting.”

“Yes, Mistress.”

“You deserve to be punished.”

“Yes, Mistress.”

“Crawl over to the dining room.”

#### Crawl Slave!

![IMG_0127.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0127.jpg?lossy=1&strip=1&webp=0)

The small dining room adjoined the kitchen, barely big enough for a cheap rectangular IKEA table with four chairs. I pulled out one and sat down. “You know what to do, slave.”

Danny climbed onto my lap and bent over, his hands and feet supporting his weight. I felt the rings of the cock cage against my thighs. “We’re going to do this a dozen times, slave.”

“Yes, Mistress.”

I gave him a firm swat on the right cheek. He flinched.

“One. Thank you, Mistress.”

“No, let’s try it again.” I swatted his left cheek.

“Two, thank you, Mistress.”

“No, wrong answer.” I swatted his right cheek.

Silence. I really do think subs get stupid. “Slave, why am I spanking you?”

“Because my apartment is dirty, Mistress.”

I rubbed his ass, making circular motions with my hand, loving caresses. “That’s an understatement, my sweet pet. Your apartment is unfucking, unbelievably filthy. So what should you say?”

“I promise to keep my apartment clean, Mistress.”

#### Accepted

It was a sincere reply, no sarcasm. That I would accept. “That’s right, slave, so what should you say each time I have to make that point on your ass to get it through to your thick sub head?”

“I promise to keep my apartment clean, Mistress.”

“That’s right. Now shall we start again?”

“Yes, Mistress.”

I spanked him hard, aiming the blow on the crack. It made a sharp sound.

Flinch. “One. Thank you, Mistress. I promise to keep my apartment clean.”

“Very good, slave.” I spanked him again.

“Two, thank you, Mistress. I promise to keep my apartment clean.”

Another spank, this time on the side of his right cheek. He winced.

“Three, thank you, Mistress. I promise to keep my apartment clean.”

#### Punishments

![IMG_0128.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0128.jpg?lossy=1&strip=1&webp=0)

And so it went, up to twelve. As I rained blows down on Danny’s ass, I wondered how well the walls and floor were insulated, if his neighbors could hear the ruckus. Oh well, let them wonder. I gave Danny a thirteenth spank for good measure, and then set him to work—picking up months of accumulated trash (filling two 25-gallon garbage bags), sweeping, vacuuming, scrubbing, washing, polishing, doing the laundry, on and on and on . . . even with me working by his side, it took the better part of the afternoon. By the time we had finished, Danny’s cock was limp in his cage and we were both tired.

This left me with a Mistress’s dilemma, how to finish the scene? It wasn’t really even a scene; it was more like four hours of hard vanilla work. Did I simply release Danny and we call it a day, order pizza?

It was six p.m. and I was hungry. If I was hungry, Danny was starved. What to do? When in doubt, drink wine, that’s my motto. Seriously, while cleaning out the pantry I had found an unopened bottle of Charles Shaw Cabernet Sauvignon, 2013. I had probably purchased it myself and brought it over on one of our early date nights and we never drank it, Danny being more of a beer guy. Anyhow, it was a decent wine and I had an idea. We would order a pizza delivered, and while we were waiting, I would drink wine while Danny . . . hmmm, what would I have Danny do?

#### Wine Time

![IMG_0129.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0129.jpg?lossy=1&strip=1&webp=0)

I called the neighborhood pizza shop and ordered my favorite vegetarian pizza, large. From now on, Danny would be eating better, too. I found him in the bedroom, folding laundry. “Slave, when you finish, I want you to come to the dining room.” He looked up from his pile of socks, T-shirts and underwear. He was still nude, his cock limp in the cage. “Yes, Mistress.”

“Don’t be long, I’ll be waiting. You have done such a good job, I have a reward for you, slave. And I’m having pizza delivered.”

His face brightened. “Yes, Mistress.”

I wondered if he thought pizza was his reward. Silly sub.

#### Silly Sub

On the way to the kitchen, I stepped into the bathroom and took a small tub of Vaseline from the medicine cabinet. In the kitchen, I uncorked the Charles Shaw Cab and poured myself a glass. I took the lubricant and wine to the dining room, setting the Vaseline on the table. I sat on the chair I had used earlier to bend Danny over my knee. I stripped down to bra and panties, a leather strand around my neck with the little key that went with the padlocks. Chic Mistress jewelry. I was halfway through the first glass of wine when Danny walked in. His eyes popped at the sight of me.

#### Chastised

“Shouldn’t you have crawled into my presence, slave?” I chastised him. “Try it again.”

He backed out of the room. A few moments later he entered on hands and knees, crawled up to my bare feet at waited. I took a sip of wine. “You may kiss my feet, slave.”

Adoring kisses on each foot. I have pretty feet, so they deserve it.

“I bet you would like to suck my toes, wouldn’t you, pet?”

“Yes, Mistress.”

“Go ahead.”

He started in with his tongue, sensuously licking between each toe, sucking the toes, then moving on to the big toes, sucking them like he was sucking cock. It felt amazing. I had never enjoyed a foot rub more. Which gave me an idea. “Lick the soles of my feet, slave,” I commanded.

He lifted my feet and angled his lips under them, making deep, probing thrusts of his tongue against my insteps, massaging them. Heavenly. I could have let him go on like that for an hour, but the pizza guy would be at the door before long. “Okay, slave, enough. Now rise up on your knees.”

Danny sat up on his haunches. I saw that his cock had come roaring back, completely filling the cage. Damn . . . I had planned to have him take it off and let him masturbate. I wondered if he could jack off while wearing the cage. Only one way to find out. I reached behind me for the Vaseline, opened the lid and scooped a large, walnut-sized dollop from the container.

#### “Put your right hand out, slave.”

![IMG_0130.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0130.jpg?lossy=1&strip=1&webp=0)

Danny extended his hand. I smeared the Vaseline into his palm. “I want you to masturbate while I watch, slave. I don’t know if you even can while wearing that cage, but under no circumstances are you to cum without my permission, understood?”

“Yes, Mistress.”

“You may begin.” I poured myself a little more wine and watched as Danny wrapped his right hand around the cock cage and stroked his caged shaft, smearing it with the Vaseline.

“You can use both hands if you like, slave.”

Danny didn’t reply, he had closed his eyes and was going into subspace, but I knew he heard me because his left hand joined the right and together they wrapped around the cage. His pelvis made deep thrusts, drawing his caged cock in and out through the tunnel formed by his hands. Like a vagina.

Watching, I felt myself getting wet. I took a gulp of wine and put down the glass. “Slave, stop.”

#### Slave, Stop!

Danny reacted slowly, reluctantly, but he obediently came to a halt. Then he watched as I pulled my panties down my legs and wrapped them into a ball. I stood up and stepped forward, spreading my legs so that they bracketed Danny’s engorged cock. I pushed the wadded panties into his mouth, filling it entirely, forcing him to breathe though his nose. I lifted the leather necklace from around my neck, grasped the key and reached under his balls to unlock the padlock, then pulled the well-lubricated cage with a swift, hard tug, freeing his cock. “Fuck my hands, Danny. Fuck the hands that spank you.”

That set him over the top. He began pumping his cock through my hands, moaning, his eyes rolled back. His chest heaved; the air sang through his flared nostrils. He was losing it. I knew it was only a matter of seconds before he came. “Look at me, slave, look into my eyes!” I commanded.

Danny’s eyes flew open. The thrusting of his cock in my cupped hands continued unabated.

“Keep your eyes fixed on mine. Don’t you dare cum without my permission. I want you to beg.”

Danny moaned a muffled reply through the panties, his eyes locked on mine, accelerating his thrusts. It didn’t take long, maybe thirty seconds. I looked into his bottomless brown eyes and watched his soul swim up from the depths of subspace and break the surface. I yanked my panties out of his mouth just in time for him to cry out, “Please, Mistress, I beg of you, let me cum.”

#### “You may cum, slave.”

His cock exploded in my hands; I felt him violently pulsing, ejaculating cum in hard spurts that hit my belly. The cum dripped down to the floor. I let go of his cock and took his head in my hands, pulled his face against my breast and let him heave. When his breathing finally calmed, I gently pushed him away. “You’ve made a mess, slave.”

“Yes, Mistress.”

“Now lick your cum off me.” I pulled his face down to my pubic region and made him lick it clean. I released his head. “Now the floor.” Danny looked down at the small puddle of cum on the linoleum below us and hesitated. I pushed the top of his head down, forcing him to bend over and put his hands out to support himself. “Get in position and luck your cum off the floor, slave!” I insisted.

Danny got down in position and started tentatively licking his cum off the floor. I stepped back and sat down on the chair, picked my wine glass off the table and sipped. “Lick it clean, every drop,” I said.

He eventually lapped it all up, but I could tell he found the act of consuming his cum disconcerting. That, or the taste of it. “You’ll learn to get used to it, slave,” I said. “Now crawl to me.”

Danny crawled over, putting his lowered head between my feet. “Sit up.” I unlocked the collar and removed it from his neck. “Slave, the pizza will be here in a few minutes. Go take a shower. When you come back, you are released. No need to crawl, you can stand up and walk to the bathroom.”

Danny rose to his feet. He bent over and kissed me on the forehead. “I adore you.”

I looked up at him. “I know you do.” I gave him a playful slap on the rear. “Now get.”

So ended our first week as Mistress and slave . . . I wondered where it would all lead.

### Part 9, Survey Says

A dozen red roses showed up at the bank Monday afternoon. The card read, “Thank you from the bottom of my heart. Will you marry me?”

![IMG_0253.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0253.jpg?lossy=1&strip=1&webp=0)

I chalked it up to youthful exuberance, but still, it was a nice sentiment, especially since after pizza on Sunday night at Danny’s apartment, before leaving I had given him instructions that he was released for the week, we would be a vanilla couple until further notice. He gave me a crushing embrace at the door and showered me with kisses, not a word of complaint, not the slightest hint of a pout.

I work in an open cubicle, not much privacy; with roses adorning my desk, I knew my co-workers wanted to know who my secret admirer was, so I went outside to call Danny. It was such a nice day, mild and sunny, I considered just walking to the other end of the plaza where Danny worked, but something told me to keep my distance until I sorted out the feelings his proposal triggered.

He didn’t answer my call, so I left a message, teasing him about a spring wedding date. April would be perfect. He called a minute later, out of breath. “So, you got the roses?”

“That was very sweet of you, honey, thank you.”

“Uh, about the wedding date, April seems a little soon, don’t you think?”

I laughed. “I was just teasing, sweetheart.” I was, wasn’t I? “I know you weren’t serious.”

“I’ll come over after work and get on my knees and ask properly. Just give me a little time to swing by a jewelry store and pick up the ring.”

“You know you can still buy Crackerjacks at Kroger’s, sweetie.”

“I’m serious.”

“Danny dear, you forget I know what you earn. You can’t afford an engagement ring.”

“I can’t afford to let you get away, you’re the best thing that’s ever happened to me.”

“Because I’m a good Mistress, right?”

The pause on the other end told me I wasn’t far off the mark. His inner sub proposing.

“No, because I adore you.”

“You adore your mistress.”

“Well, that, too, but seriously, Vanessa, I’ve adored you since the moment we met.”

“When was that?” I hated testing, but I wondered.

#### Memories

This time there was no pause. “The day you brought your car in . . . it was just before Halloween, last year. You were wearing an orange blazer, a white blouse, a matching skirt with a slit up one side, strappy shoes, looking totally hot. You said your tire low pressure light was on.”

I looked at the phone, hardly believing my ears. Other than the fact that the jacket and skirt were tangerine, not orange, he had gotten the occasion and the outfit right. Wow, maybe Danny did adore me for me. But still, I doubted the sincerity of his proposal, especially coming on the heels of our first week and Mistress and slave. Now, if it had come after a visit from my mom—

“But you know what really did it for me?” Danny interrupted.

I couldn’t resist. “What?”

“You knew which tire it was.”

Well, of course I knew which tire it was, it was the left rear. Dad raised me right. I had a tire gauge and knew how to use it. I also regularly checked the oil, added wiper fluid, and could take off a flat tire and put on the spare. And I knew how to drive a stick (which may have cost Dad a few years).

“You know what else did it for me?”

“My tiny boobs?”

“Your voice. The sound of your laughter . . .”

This boy could charm the pants off a girl. Which is exactly what he had done the evening of our first Halloween party. We had done a role reversal, I had dressed up as Captain Hook and he as Peter Pan. There’s a sign, though it didn’t occur to me until this very moment. I laughed.

“See, there you go again, laughing. I love your laugh. And your perfect, perky breasts.”

“Danny, I have to get back to work. Thank you for the flowers.”

“So you’ll marry me?”

“Sweetheart, no . . . but keep asking. I might eventually change my mind.”

“Can I come over tonight and ask again?”

“No! I’m working on a project. You watch Monday Night Football with your buddies.”

“No way . . . if they see how clean my apartment is now, they’ll think I’ve turned gay.”

“Goodbye, Danny. Enjoy the game.” I went back inside, grinning.

#### Sub Survey

![IMG_0172.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0172.jpg?lossy=1&strip=1&webp=0)

That night I curled up on the couch and worked on my sub survey, presumably while Danny watched football. I pulled a lot of the material off the internet, various BDSM surveys and kink preference lists. I even did a BDSM aptitude test, which reported that I was 80% dominant and 20% submissive. There it was, scientific evidence that Danny was right about me.

OK, so I might be a little bossy, but I’ve always been the smartest person in the room, and I always do my research. When you are right, it doesn’t do any good pretending the dumb bunnies know better. These qualities made me a good loan officer, and apparently, a good Mistress.

Tuesday I called Danny and asked him to take me out to lunch. We went to Chipotle, and while Danny ate his chicken burrito and I my salad, I handed him a manila envelope with the survey inside.

“What’s this?” he said, eyeing the envelope suspiciously. “The guest list for our wedding?”

Such a witty boy. I smiled innocently. “No, silly, we’re not having a wedding. These are just some questions I want you to answer.”

A lifted eyebrow. “About . . .?”

I put a hand on his forearm. “About your special interests.”

He swallowed down a sudden lump in his throat and his face went pale, as if I had shouted out to the entire restaurant, “Hey, everyone, my boyfriend is a closet sub!”

“Danny, it’s just some questions to help me understand you better, okay? I found them on the internet. Just take it home and fill it out when you have time. It’s no big deal. Now eat.”

He put the envelope to the side and we had a normal vanilla lunch.

My survey was extensive, ten pages, 30 multiple-choice and fill-in-the- blank questions, a long list of kinky toys and preferences, concluding with an essay section where he was to write out his fantasies and expound on why he thought he had submissive leanings.

Knowing Danny’s profound dislike for paperwork (I had helped him with his taxes in April), I honestly didn’t expect him to finish the survey until Friday, if not Sunday, after he had the weekend to work on it. But the next morning at nine fifteen, barely in the office and still on my first cup of coffee, I received the text message that I had instructed him to send when he had completed the survey:

#### “Assignment complete, Mistress.”

Amazing what sexual motivation can do for a man. He must’ve stayed up all night working on it.

I waited until three p.m. to text a reply: “Slave, be at my house at 7 sharp. Bring the survey. Wear your cock cage. Don’t be late.”

At seven p.m. I heard tires crunching on the gravel. I sat on the couch, wearing my first dominatrix outfit: a black, faux leather corset and mini skirt multi dress with leather lacing up the front. No bra, no underwear. On my feet, four-inch black T-strap high heels. My hair down. I looked sexy as hell.

There was a note for Danny on the kitchen counter, his slave collar resting on top. The note read, “Welcome, slave. Leave your survey on the counter. Take your clothes off, fold them neatly and leave on the floor. Put your collar on. Go upstairs and lock yourself in your cage. Throw the key towards the door, out of reach. I’ll be up in to check on you.”

I heard Danny come in. The antique wall clock (inherited from my dad) showed three minutes past seven. Danny would pay for that. It occurred to me I needed a Mistress’s demerit book. And a system of punishment. My thoughts were interrupted by the sound of something clattering to the tile floor in the kitchen. Probably the collar, dropped. Muffled sounds, then after a few minutes, the sound of Danny’s footfall going up the stairs. Then a faint rattle of metal on metal, the door of the cage closing. I waited a few minutes and then went into the kitchen. Danny’s clothes were in a neat pile on the floor, the manila envelope resting on top. My, another demerit; I’d teach my slave to follow orders explicitly yet.

I was tempted to open the envelope and start reading, but my plan was to go first go upstairs and tease Danny with the outfit, leave him horny while Mistress read all his dirty secrets. Stick to the plan, Vanessa, I told myself. Just like preparing a meal, stick to the recipe. I carried the envelope up with me and mounted the stairs, a little unsteady on the high heels but making sure they clicked on every step. At the top, I could see into the lit spare bedroom. The key was at the doorway, on the carpet.

In the room, I found Danny inside the cage, naked and collared, crouched on his hands and knees. He lowered his head and torso as I approached. I rattled the padlock, making sure the door latch was secure. I slapped the manila envelope against the side of the cage. “Good evening, slave.”

“Good evening, Mistress.”

“I see you were eager to have Mistress know all about your kinky submissive self.”

“Yes, Mistress.”

#### Kinky Submissive

![IMG_0254.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0254.jpg?lossy=1&strip=1&webp=0)

I stepped to the side of the cage and knelt down, the leather skirt hiking up to just below my crotch. I caught Danny peeking out one eye. His cock was swelled inside the cock cage, the glans of his engorged penis pushed through the head ring. “I see you are properly aroused by the presence of your Mistress.”

“Yes, Mistress.”

I rose, walked around to the cage and stepped back into the doorway. “You may look up, slave.”

Danny lifted his head and took me in greedily. The lust radiating out of his eyes was worth the price of the outfit and then some. Money well spent, especially since it was on his MasterCard.

“You remember we picked this out last Friday night?”

“Yes, Mistress, I do.”

“I think it flatters my figure, don’t you?” I did a flirty twirl in the doorway.

“That’s an understatement,” Danny responded.

Sexual energy poured out of the cage. If Danny hadn’t been locked up inside it, the fierce expression on his face told me he’d leap out, throw me down on the carpet, jerk the skirt up and ram his cock inside me hard. Of course, all wishful thinking.

“That’s an understatement, Mistress,” I corrected him.

“Yes, Mistress.”

Dumb sub, he just didn’t quite get the proper response etiquette. I let it pass and let him drink me in for a moment, an erotic vision to keep him preoccupied for the next hour.

After a few seconds passed, I slapped the envelope against my bare right thigh. “Well, slave, I’m going downstairs to read your survey. I’ll be back up to discuss it with you when I’m done. I expect to find you hard, so keep your mind on how just badly you wish you could fuck your mistress.”

“Yes, Mistress, it won’t be hard.”

I tilted my head. “It won’t be hard?” I teased, feigning disappointment.

“No, no, what I meant is it won’t be hard to stay hard for you, Mistress.”

“Aha,” I chuckled. “Good boy.” I flicked off the light and headed down the stairs, my heels echoing in the staircase, no doubt floating up to Danny’s lustful ears.

The wine was chilled and ready, the bottle already opened and waiting in the fridge, a nice Italian Pinot Grigio, 2013 Santa Margherita (yes, I like my wines). I poured a glass and took it to the living room, where I kicked off the high heels and curled up under a comforter on the couch. I opened the manila envelope and pulled out the survey, ten stapled pages. The instructions said to use a pen, not a pencil, as I wanted Danny’s first thoughts, no erasures. I was pleased to see he had gotten that right. He wrote in block letters. I wondered if he even knew how to write in script. Perhaps Mistress would teach him.

The survey was intentionally designed to make it look as if I had taken it from the internet, so that the questions seemed impersonal, not mine. I wanted Danny to answer questions as if responding to a disinterested party, so that he would be more truthful. The survey began with multiple choice questions that covered basics we were already way past, considering he was locked in a cage upstairs, but they were leaders for the subtler questions to follow:

1. If your girlfriend told you she wanted to tie you up and have sex, would you:

  - Run for your life.

  - Ask her to explain why she felt the need to control you.

  - Tell her you wanted to tie HER up.

  - Negotiate limits to the scene, including the use of safe words.

  - Tell her that’s the sexiest thing a girl has ever suggested to you.

2. You’ve come upon an article about dominant females and female-led relationships. You:

  - Ignore the article and continue your search on dominant football teams of the 90s.

  - Browse the article, but are put off when it describes women as the superior sex.

  - Read the article with interest, but dismiss it as not relevant to you.

  - Read the article with disgust, wondering what kind of pervert wrote it.

  - Devour every word, your soul shouting out this is true and right for you.

3. When you meet a strong, alpha female at work or a social setting, your response to her is:

  - She’s a bitch, she needs to learn her place.

  - Pity the poor guy who ends up with her.

  - You admire confidence in a woman and find it attractive, even sexy.

  - You flirt with her, hoping to get her in bed and show her who’s boss.

  - You introduce yourself to her, dreaming of her dominating you in bed.

And so on. Not unexpectedly, Danny answered “E” to the first two questions, C and E on the third. The questions got subtler, not so leading, but after 30 questions a clear picture emerged: Danny was a man who wanted to live a lifestyle where the woman in his life was in charge of his life. And it wasn’t just about sex—he wanted to be submissive to his woman in every way—chores, finances, travel, work, vacations. I wondered why he had this longing. In regular life, he seemed a normal guy, a man’s man.

#### Prefereneces

![IMG_0134.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0134.jpg?lossy=1&strip=1&webp=0)

Interesting. I sipped my wine and moved into the section on kinky preferences. Turned out my guy liked a lot of kinky things, but disliked an equal number, which pleased me, because most of his dislikes were strong dislikes for me. As for his likes, well, I had no idea just how kinky Danny was, deep down.

Systematic me, I had made the list alphabetical. Danny’s kinky interests included 24/7 total power exchange, anal sex (really!?), breath play (?), blindfolds, bondage, breast/nipple torture, candle wax, chains, chastity devices, cock & ball torture, confinement/caging (had those two covered), defilement (huh?), domination (yup), ears (how?), handcuff/shackles, high heels (worn on my feet, I presumed), humiliation, lace/lingerie, latex, leather, massage (oh, he’d be giving me a lot of this), masturbation, nipples, oral sex (no surprise), pantyhose/stockings, power exchange, shoes/boots, role playing, scent, sensory deprivation, spanking/paddling, talking dirty, toys, vibrators and whips.

The list of dislikes was equally long: age play, biting, blood, body hair, branding, bukkake (the name alone a yuk), Chinese balls, cling film, clown (wtf?), coprophillia (scat play, another yuk), cross dressing, cuckold (I had to look it up; not happening), dacryphillia, denim, depilation/shaving, doctor/nurse, electrotorture, exhibitionism, feathers, fire play, fisting (ow!), food play (not even grapes?), gang bangs, hair pulling, infantilism, knife/needle play, lactation, making home movies, masks, piercings, pinching, pony play, religious play, sadism, tickling, tongue fetish, transvestism and watersports.

Whew, I was relieved he wasn’t into those. I mean, to each his own, but I didn’t know half those things existed, that people really got off on them. I was a vanilla girl in a kinky world. Going back to Danny’s likes, I was both disturbed and intrigued. Anal sex . . . I presumed that he meant he would like me to use a strap-on on him, because my asshole was sure as hell off limits to him. The survey raised as many questions as it answered. Having him in the cage when I asked them would help; we wouldn’t be negotiating, I would be interrogating.

Moving on to my favorite part, the essay section. I had Danny list six locations that appealed to him for enacting a BDSM fantasy. He listed bar/nightclub, bed, desk at work, dungeon, dressing room and a vehicle. All doable, except maybe my desk at work. Next I had him write out five fantasies. Turns out we had already acted out three of them—confinement in a cage, flogging in a dungeon, treated like a dog. The next two fantasies caught me a little off guard.

#### Fantasies

In one, he was blindfolded, hogtied in the trunk of a car, left in there while I did suburban shopping at a mall, then driven to an unknown location where me and my kinky girlfriends would use him as a sex slave all weekend. My! Well, maybe the first part of that might be possible, but I didn’t have any kinky girlfriends (that I knew of) and I wasn’t sharing Danny with them. The fifth fantasy was about coming home to discover his Mistress wearing a strap-on cock, being forced to suck it, then her taking him in the ass. Bend over, boyfriend. I wondered if I could pull that off. Was it just a fantasy, like the girl’s sex slave weekend, or did he really want to be fucked by his Mistress?

The last essay was probably the hardest. Why did he want to be dominated by a woman? Danny’s answer was short but thoughtful. He wrote, “I’ve had these desires since adolescence, when I grew balls. My first masturbation fantasies were about being dominated by a girl, forced to go down on her, tied up and teased, etc. It’s almost impossible to bring up these sort of desires in a regular relationship, and the BDSM dating sites are filled with posers, mostly sick guys screwing around with fake profiles. I’ve been to a few professional dominatrixes but those experiences were disappointing. Yes, they went through the motions, but the motions were not connected to emotions. For me, this has to be connected to someone I love, someone I adore, someone I respect. That’s the cake—the relationship. The kinky part is like the icing on the cake. It wouldn’t be the same without icing, but you have to have the cake. For me, submitting to a good woman is my way of making myself a better man. It fills me with joy and gives me power in my daily life.”

I have to admit, that essay melted my heart. Danny might be a bad boy, but he was a good man, seeking to be an even better man. I loved that he wanted it connected to his woman, the woman of his life (not withstanding his fantasy about my girlfriends and me having our way with him, the dog). The thing that really caught my eye was his concluding sentence, about being a submissive filling him with joy and power. I could imagine him working under a filthy car, wearing his cock cage under his jeans, thinking about what the cage represents, giving him a secret thrill in an otherwise grungy, mundane moment at work. Not to mention, knowing he was going home to an adorable, sexy woman who held the keys to his cock. I got it. Yes, it was a little crazy, but then everyone is crazy in their own way. It comes down to what makes you happy. Some people like to skydive, right?

I made a decision. I finished my wine and threw off the comforter, slipped the high heels back on. The wall clock read a quarter past eight. Up the stairs I went, heels clicking. I paused at the top, just a moment to let my slave know I was there. Then to the doorway, flicked the light on. Danny was on all fours, his cock still swelled large in his cock cage. Good boy. I walked up to the dog cage and caressed Danny’s hair through the wires. “Have you been a good boy, slave?”

“Yes, Mistress.”

“Well, I’ve read your survey . . . very interesting. Thank you for your honesty, slave.”

“Yes, Mistress.”

“We have a lot to talk about. Or rather, I have a lot to say to you, about what we will and won’t be doing, specific things, but for now I have a bigger, more important question to ask you.”

“Yes, Mistress?”

“You say that your soul comes alive, that you find joy in life when you are my submissive slave, not just the kinky sex, but the act of submission to my feminine will. Is that right, Danny?”

“Yes, Vanessa, I mean, yes, Mistress.”

I smiled. The slip of his tongue was revealing. “So you want me to be in charge, is that right?”

“Yes, Mistress.”

“In charge of your real, vanilla life . . . chores, finances, where we go on vacation, everything?”

“Yes, Mistress.”

“What if I want to have children?”

A long pause. “That would be for you to decide, Mistress.”

“You mean that?”

“Yes, Mistress. I love you and trust your judgement. If you wanted a child . . . that too.”

I stared at Danny in the cage, wondering, thinking. How could he know his own heart so well? What if this was just his submissive side talking? It was a fun fantasy, but making it real, making submission to another—to me—the center of his life? How could I know his resolve?

#### An idea came to me.

“Slave, I’ll be right back.”

I went downstairs, opened the fridge, took out the milk. It was skim in a quart plastic container, about half full. I poured the milk into an empty water bottle, rinsed the jug out and screwed the lid back on. Back upstairs, Danny was in the same position, the same hard cock. I wondered if he was filled with ideas about what erotic scene I had in store for him next. Little did he know, none. I picked up the key and went to the dog cage, unlocked the padlock, swung open the door and threw in the empty milk jug. I quickly swung the cage closed, reset the latch and locked it. Danny didn’t have a clue.

“I’ve made a decision about you, slave. I have an offer to make, but I want you to really think it over, I want you to sleep on it tonight, here, inside your cage. It won’t be very comfortable, but then being my submissive slave won’t be comfortable . . . you’ll have to do things you don’t like, things you don’t want to do. You’ll have to do them because it pleases me, period.”

“Yes, Mistress.”

“So here’s my offer. I’ll become your full-time Mistress, everything you dreamed of and wrote about in the survey, the woman who makes you a better man, but if we are going to do this, you have to be utterly sure. I’ll write up a contract, and you’ll sign it in the morning. If after spending the night in this cage you decide it really isn’t such a great idea, I’ll understand completely. But if you call it off, then we go back to being a vanilla couple, no kinky games. It’s all or nothing, understood, Danny?”

“Yes, Mistress.”

“So that’s a bottle for you to pee in. It’s plenty warm up here, so you won’t freeze. And now you have all night to meditate on your heart’s desire, and decide if it is for real or a foolish fantasy. I want you to decide if you really, really want to submit your mind and body to a woman—to me.”

“Yes, Mistress.”

The ‘Yes, Mistress’ response had serious limitations, I realized. Gazing at Danny, I took a closer look at the space in the cage. Could he lie down? Maybe he could, if he was on his side, lying in a semi-fetal position. I wondered about the sanity of what I was proposing. Then another idea came to me, a sound idea, some common sense, something I could live with. An end game to this, a future.

“Slave, I’m calling a time out. You are to respond to me as Danny.”

“Yes, Mistress—I mean, yes, Vanessa.”

“Danny, in that note with the roses you asked me to marry you. We joked about it on the phone. Were you serious, or was that just your submissive self gone crazy?”

Danny lifted his eyes. “Vanessa, I meant it with all my heart. I can’t afford an engagement ring, sure, but I meant it—I mean it.” His expression couldn’t have been more sincere. And his cock was still hard.

#### So, I’ll Take that as a Yes

“Okay, I take that as a yes. So here’s the deal. If you still want to go through with this in the morning, I’ll have you sign a contract that goes through April, six months from today. It’ll be our own private engagement, with the key to your collar my engagement ring. Then in April, when the contract expires, if you still want to be my slave, in a female-led marriage, and I agree to it, then we’ll set our wedding date in July. If you decide that you don’t want to be married—”

I choked up, almost started to cry, but caught myself. I was made of sterner stuff. I was a Mistress. “—If you decide you’ve had enough, you’ve got it out of your system, and you don’t want to be married, I’ll release you from any obligation. That’s my proposal.”

Danny heard the catch in my voice, and I think he knew I was dead serious. He studied me, his eyes raking in my leather-clad body, but I could see that his focus was deeper, on what I had proposed, what it really meant for him. He cleared his throat. “I accept.”

I couldn’t help but notice the absurdity of the situation, a couple discussing their future, the man naked in a cage, the woman in dominatrix garb. As the French say, C’est la vie.

“Okay then, the time out is over, slave.”

“Yes, Mistress.”

I rattled the padlock one more time, just making sure the latch was secure. I turned for the door. Just as I put my hand on the light switch, Danny spoke, “Mistress, may I say something?”

Of course he could. Why hadn’t I thought of it before, giving my slave permission to speak, to ask a question or make a statement, and not just say “Yes, Mistress’? He needed to be able to communicate. We both had plenty to learn if we were going through with this.

I turned to face Danny, folding my arms across my chest. “Go ahead, slave.”

“Vanessa, that’s the sexiest, coolest thing a girl has ever suggested to me.”

“Mistress, that’s the sexiest, coolest thing a girl has ever suggested to me,” I corrected him.

“Yes Mistress, if it pleases you.”

I flicked off the light so he couldn’t see me smile.

### Part 10, Simon Says

I woke up early. The red digits of my alarm clock glowed 6:46. I got up, took a shower, made coffee, got dressed. I picked the tangerine outfit, the one Danny remembered from our first encounter. Pale cream blouse, white lacy bra underneath. The strappy shoes, open-toed with two-inch heels. While the coffee brewed, I opened my laptop and read the contract. I had rewritten it a dozen times last night, mostly condensing it to the bare essentials, down to a page and a half. I found a few grammatical errors and some typos, made the changes and hit ‘print.’ I heard the printer come to life in the hall closet.

![IMG_0131.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0131.jpg?lossy=1&strip=1&webp=0)

Upstairs, silence. I figured with all the noise I made, Danny would be surely awake by now. That is, if he hadn’t been up all night, unable to sleep from either mental or physical discomfort. Probably both. The printer stopped. I opened the closet door, pulled the sheets, took them to the kitchen and stapled them together, then carried the papers to the dining room, setting it at Danny’s place. He’d need a pen. Back to the kitchen, grab a pen, carry it to the dining room. Back to the kitchen, pour a cup of coffee, stir in some skim milk from the water bottle, sip my coffee, wonder how it went last night for Danny.

I felt nervous, like I was about to buy a house. Hell, I was about to take possession of a man.

Unless he had changed his mind.

The kitchen clock read half past seven. Time to go upstairs and see how Danny fared through the night. I put down the coffee and went to the staircase, making plenty of noise as I climbed. I wanted him in position when I walked in. He was, looking the same as last night, apparently none the worse for the confinement. He even had a hard-on, which surprised me. Either it was a morning hard-on or he had stroked himself into an erection. Either way, I was flattered. As I circled the cage, I observed that Danny kept his head still and eyes cast down. Good slave. The milk jug was in the back of the cage.

#### “Did you sleep well, slave?”

“No, Mistress.”

“But you slept?”

“Yes, Mistress.”

“Look up, slave.”

Danny looked up with a shock of recognition when he realized what I was wearing.

“I’m going to let you out of your cage now, slave. I want you to go downstairs and take a shower. You’ll find a key by the washbasin. Take off your collar and cock cage while you shower, then put them back on. Crawl out to the dining room when you are done. We’ll look at the contract together.”

“Yes, Mistress.”

I unlocked the padlock, slid the latch, opened the door, and took a step back. Danny started to crawl forward but saw that I was blocking his exit.

“What are you supposed to do, slave?”

Danny bent lower and kissed my shoes.

“Did you forget something, slave?” He froze, thought for a moment. Then he backed into the cage, reaching with his right arm for the milk jug. He crawled forward, back into position at my feet.

“Dump that out in the toilet, slave, then rinse it out while you shower. You can leave it under the vanity. You’ll probably be using it again.”

#### “Yes, Mistress.”

![IMG_0132.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0132.jpg?lossy=1&strip=1&webp=0)

“Go.” I stepped aside and Danny crawled forward to the door, then rose up stiffly. He disappeared down the stairs. I looked around the room. Light flooded through the dormer. Outside, I heard a school bus rumble down the street. I went to the window. Another beautiful fall day. We had enjoyed a string of them. I could smell coffee from the kitchen. Gazing at the brilliant yellow Gingko tree in front of the house across the street, I wondered what Danny had decided. For some odd reason, I wondered what we would wear at the Halloween party this year. That is, if we were still a couple. Assuming we were, it would be fun to make Danny cross-dress. I’d be an airline captain and he’d be a flight attendant.

I closed the door to the cage, hung the lock on the latch, then headed downstairs. Danny was still in the shower. I poured myself a fresh cup of coffee and took it into the dining room. I took my seat at the head of the table and picked up the contract. I read it again.

#### The Contract

“I, Daniel Simon Barton, born March 21, 1992, being of sound mind, enter this agreement freely, without any mental reservation or purpose of evasion, in order to develop my character and to obtain the benefits of intellectual, moral, physical and spiritual growth.”

I made that up, but I thought it read pretty good. The second paragraph got specific:

“I, Daniel Simon Barton, hereby enter bondage as the slave of Ms. Vanessa Prudence Whetstone. This agreement shall be binding on both parties, and last six months, from the date of this agreement, today, October 15, 2014 until April 15, 2015. Under the terms of this agreement, Ms. Whetstone shall have full authority over me in all matters, private and public, encompassing all actions and intentions, without restriction. Such authority shall extend to my body, diet, exercise, sexual activities, intellectual and physical pursuits, recreation, work, management of time and finances. Nothing absent from this list shall be excluded from the scope of Ms. Whetstone’s authority, which is absolute.”

![IMG_0133.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0133.jpg?lossy=1&strip=1&webp=0)

The third paragraph was specifically about physical domination and sexual practices:

“I, Daniel Simon Barton, agree that Ms. Whetstone has total power over my sexual activities, any form of sexual release, specifically orgasms, and may engage in anal play using dildoes and butt plugs, may fuck me using a strap-on cock, may restrict my breathing with her hand, ball gags or tape, may blind me with blindfolds, may exercise any form of bondage, may torture my nipples, may pour hot candle wax on my flesh, may bind me with chains, may restrict my cock with a chastity device for any period of time, may torture my cock and balls with any instrument, may confine me in a cage for any period of time, may defile me by any means, may practice domination in all its forms, may bind me with handcuffs and shackles, may dress me in any clothing, including ladies high heels, lace, lingerie, latex and other feminine clothing, may force me to masturbate, may demand oral sex at any time, may engage in any form of role playing, may impose sensory deprivation, may spank and paddle me at will, may speak to me in a derogatory and humiliating fashion, and may flog or whip me with and without cause.”

The fourth and final paragraph was on the top of the second page. It was the one that I thought might scare Danny to his senses, make him realize that this was not just sexual domination, this was total domination over every aspect of his life:

“I agree to Ms. Whetstone’s complete authority over my daily activities, the use of my time and scheduling of appointments and future events. I agree to her authority over my short- and long-term finances. I agree that all my earnings will go into an account managed by Ms. Whetstone, that she will issue me a weekly allowance, and that purchases of twenty dollars or more must be cleared in advance. I further agree that Ms. Whetstone may allocate any portion of my income against expenses, make distributions and/or investments with my savings, and may dispose of any and all property at will. Finally, I agree that subject to any legal encumbrances, Mr. Whetstone may terminate credit cards or real estate agreements, including the rental agreement at my current place of residence. Any funds or financial instruments not addressed above fall under Ms. Whetstone’s authority, without exception.”

——————————————   Daniel Simon Barton

Signed this ___ day of October, 2014

——————————————

Vanessa Prudence Whetstone

Signed this ___ day of October, 2014.

That about covered it. Putting all those erotic activities in dry, contractual language was seriously weird, but it made it real. I have to say, it was the last paragraph that excited me. Not as in make my panties wet, but as in, the idea of absolute, 24/7 power over a man, an honest man with a good mind and a great body, a man who adored me and worshiped the ground I walked on. I would have carte blanche over all his affairs, and the means to make him a better man, which frankly, he needed.

What’s not to love about that?

I put the contract down and waited. It was eight o’clock when Danny came into the room and crawled around the table to his place to my right. I had removed his chair, so the hardwood floor was open. I stayed in my seat at the head of the table, but extended my right shoe so that he could kiss it.

“I presume you had plenty of time last night to think over our proposed agreement, slave,” I said.

“Yes, Mistress.”

“And you’ve decided you wish to proceed?”

“Yes . . .  I think so, Mistress.”

“You think so or you know so?” I looked down at the back of his head, wondering what was going on inside. Was he going through with this? His hair was getting long; it was still damp. He needed a haircut. “Well, let’s see what you think once you’ve read the contract. It’s on the table. Sit up and take it.”

Danny rose to an upright position, balanced on his knees and bare feet. For the first time, I saw that his penis was flaccid in the cock cage. That was interesting: perhaps it meant the seriousness of what he was contemplating was finally settling in.

![IMG_0134.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0134.jpg?lossy=1&strip=1&webp=0)

Well, I wasn’t going to make it any easier on him. If he wanted a Mistress, then he would have one, but only if he fully committed to being my slave. Otherwise, this was just a fantasy—Danny’s wet dream. “Slave, take the contract and then get back down,” I instructed. “Put it on the floor and read.”

Danny took the papers off the table. He dropped back down to the floor with the contract between his hands. It didn’t take him long, maybe three minutes for the first page. He made no comment, folded the first page over, and began reading the second page. I couldn’t see his face, but I had been watching his chest expand and contract with each breath. As he read the second page, his breathing slowed, then stopped for what seemed like thirty seconds. Then he resumed breathing.

I took that as my cue. “Are you done reading, slave?”

“Yes, Mistress.”

“Do you have anything to say?”

“May I speak freely, Mistress?”

“You may, but before you do, you should know that contract is non-negotiable. Take it or leave it.”

“Yes, Mistress, I understand. I only wanted to clarify that when I circled those clothing items in the survey, the lingerie and lace and high heels, I meant that I liked seeing you wear them—not me.”

I was glad Danny’s eyes were cast down, so he couldn’t see my smile. “I thought that might be the case, slave,” I said. “But perhaps I would enjoy dressing you like a sissy girl.”

There was a long, pregnant pause. Then he replied, “Yes, Mistress, if it pleases you.”

I almost laughed. Here he was, about to enter an agreement that gave me total authority over his mind and body, an agreement with a financial clause that reduced him to taking an allowance, and his biggest concern was not wearing women’s underwear?

“So are you prepared to sign the contract, slave?”

“Yes, Mistress.”

“Sit up, hand me the contract.”

He sat up. Wonder of wonders, his cock was now fully erect, swelled in the cage. I wondered if it was the thought of an allowance or having to wear women’s lingerie that turned him on. I took the contract and signed and dated it, then handed him the pen. “Are you sure, Danny?”

He looked me in the eye. “Yes, Mistress, I’m sure. I’ve never been so sure about anything in my life.” He signed on the dotted line and filled in the blank: 15th of October 2014. It was done.

I took the contract from his hand. “One more thing.”

Danny looked at me inquisitively. “Yes, Mistress?”

“You’re calling off work today. Tell them you’re not coming in.”

### Part 11, Danny Begins His Submissive Education.

Danny called work and took a personal day off, and so did I. It was a spur-of-the-moment decision that came to me the instant I held the signed contract. I realized we had many steps and a lot of ground to cover in order to put the agreement in force. I wanted to get started on our new living arrangement. It needed to be real, and my slave needed to know it.

![IMG_0135.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0135.jpg?lossy=1&strip=1&webp=0)

The first thing I had Danny do was to give me his wallet. I had him kneel before me in the kitchen, not prostrated but upright, so that he could see what I was doing. His erection was magnificent, almost perpendicular to his body in spite of the weight of the cock cage.

I ignored his hard-on and pulled out the contents of his wallet: faded slips of paper with phone numbers (resisting the temptation to ask about old girlfriends), four ancient Powerball lottery tickets (that was his retirement plan?), his driver’s license, a car rental membership club card, health insurance and dental cards, two credit cards, a bank debit card, and 68 dollars in cash—three twenties, a five and three singles. I found an old parking ticket folded in with a nest of faded credit card receipts.

I put the driver’s license, medical and dental cards back in the wallet, cut up the credit and debit cards under his nose, and threw everything in the trash can except the cash and the parking ticket.

I dangled two twenty dollar bills in front of him, then tucked them into the pocket of the wallet. “This will be your weekly allowance, slave . . . spend it wisely.”

#### Allowance

He had a deer-in-headlights look. “Yes, Mistress.”

I held out the parking ticket with the remaining 28-dollar cash. “You’re going to pay this parking ticket today out of this money and your allowance. You’ll do it in person, at city hall, while I watch.”

“Yes, Mistress.”

“Now before I have you get dressed and we start running errands, I want to clarify a few rules about misconduct and punishment for infractions. Get in position while I explain, slave.”

Danny dropped down, prostrating himself at my feet.

“Okay, I have decided upon a system of demerits ... for every demerit you accumulate, you will receive a spank in punishment, is that understood?”

“Yes, Mistress.”

“So let’s run through the list of infractions.”

I pulled out an unused 4 x 6” spiral-bound notebook I had found in the junk drawer. My first official Mistress Demerit Book. I continued, “Yesterday you walked in the door three minutes late. That’s three demerits. Slave, you would be wise to set your watch to the clocks in this house. I expect you to be prompt, understood?”

“Yes, Mistress.”

“Next infraction. Yesterday I left a note on the kitchen counter, telling you to leave the survey on the counter, and your clothes folded neatly on the floor. You put the envelope on top of your clothing, not on the counter. Not a major crime in itself, but it’s the principle that’s important here. When I give you instructions, I expect you to follow them explicitly, is that understood?”

“Yes, Mistress.”

#### “That’s three more demerits . . . you’re up to six.”

![IMG_0136.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0136.jpg?lossy=1&strip=1&webp=0)

I moved to the third item I had jotted down. “Next infraction. When I correct something you’ve said, like yesterday, when I corrected you, ‘That’s an understatement, Mistress,’ then the proper response is not, ‘Yes, Mistress’ . . . the proper response is—” I looked down and noticed drops of pre-cum on the tile under Danny’s cock. “Back up and lick that cum off the floor, slave.”

Danny shifted back a few feet until his lips were over the drops. He reached down with his tongue and lapped up the offending puddles.

“Okay, back to correct verbal responses. When I correct you, I expect you to repeat back, in full, the correction exactly as I have spoken it, is that clear?”

“Yes, Mistress.”

“Three more demerits.”

“Yes, Mistress.”

“And if you have any doubts about my instructions, or they seem illogical to you, I don’t want you to be a robot, I don’t want you to say ‘Yes, Mistress’ when you are confused. You have my permission to ask if you may speak, in order to get a clarification or to make a statement. You will do this by saying, ‘Mistress, may I speak?’ Now let’s practice, go ahead and ask.”

“Mistress, may I speak?”

“No, you may not, slave.”

There was a long silence as my dumb bunny slave processed this response. I gave him some help. “Slave, you have my permission to ask to speak, but that doesn’t mean I will let you. If I don’t want to hear what you have to say, then that ends the conversation as far as your concerned. Got it?”

“Yes, Mistress.”

“Back to demerits. How many are we up to?”

He had to think a moment. “Six?”

I sighed. I realized I was going to have to make an allowance for Danny being a little slow when he was in sub space. The only problem was he was going to be in sub space 24/7 for the next six months. “No, slave, we were up to nine, three for being late, three for not following instructions, and three for not properly correcting a verbal infraction. And now, three more. Do you know why, slave?”

![0WBB2B.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/08/0WBB2B.jpg?lossy=1&strip=1&webp=0)

#### Silence.

“No, Mistress.”

Poor thing, this must be hard. I gave Danny a light slap on the cheek, barely more than a tap of my fingertips on the side of his oh-so-handsome face. “Pay attention, slave. Focus. You can do better than this. When you replied to me, you said, ‘Six,’ not ‘Six, Mistress.’ You will always reply to me completely and respectfully, is that understood?”

“Yes, Mistress.”

“Now pay attention, think carefully before you answer. How many demerits are we up to now?”

It took him a second, but he got it right. “Twelve, Mistress.”

“Very good.” I caressed the nape of his neck and down his spine, rewarding him. His back arched in response to my touch, like a dog being petted. It occurred to me that neither of us had eaten anything yet. I continued, “Okay, so we are up to twelve, and then there’s the parking ticket, but that’s a more serious infraction and we’ll deal with it later. So that’s a total of twelve demerits for now, and I will always administer punishments at the first opportunity. You know what to do, right?”

“Yes, Mistress.” Danny crawled over and retrieved the spanking chair, then drug it to my feet.

I took off my jacket, hung it over the seatback, and sat down, smoothing my skirt. Then thought of Danny dripping cum on my beautiful tangerine skirt occurred to me. I reached over and snagged a dry towel from the cabinet next to the sink. I spread it over my skirt. “Take your position, slave.”

We went through the spanking routine, in groups of three. Before each set, I announced the reason for the punishment. “These are for being late,” “These are for not following instructions,” “These are for not repeating verbal corrections,” “These are for not addressing Mistress properly.” To his credit, Danny focused and got the responses correctly. When we were done, his ass was red and my hand stung.

“Okay, slave, get back in position on the floor.”

![thumb_PleasureThroughPain1_640x360.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/08/thumb_PleasureThroughPain1_640x360.jpg?lossy=1&strip=1&webp=0)

Danny slid off my lap and assumed the position. I tore out the first sheet in my demerit book and let him see me toss it into the garbage can. “These are forgotten, and you are forgiven, slave. You now have a clean slate . . . that is, other than the parking ticket. Now we’re going to have a quick bowl of cereal for breakfast and then you’ll get dressed.”

I poured raisin bran into two bowls, added sliced banana and milk, reminding myself I needed to go to the store and get another carton of milk. I set Danny’s bowl on the floor and watched him slurp up his breakfast like a dog while I stood over him, spooning cereal into my mouth. As we ate, a half-dozen new rules of the house occurred to me, but one in particular demanded attention.

When we he had finished his cereal, I took Danny’s bowl and put it in the sink with mine. “Now sit up, slave.” His face was covered with milk. My dear, adorable pet. “Wipe your mouth.”

He wiped his mouth with the back of his hand. I lifted his folded clothes off the kitchen counter. “You may stand now, slave. Carry these into the bathroom and get dressed.”

Danny rose to his feet. He extended his arms to take the bundle of clothes. I handed it to him, but didn’t let go. “There’s a new rule for the bathroom, slave, in case you have to go pee. From this moment forward, you will always sit down on the toilet and pee like a girl, is that understood?”

#### “Yes, Mistress.”

“I don’t think you understand, slave. This is not just a house rule; this is a slave rule. In the future, whether you are here, or at your apartment, or at work, or anywhere else—even a ball game or a truck stop on the side of the road, you will not stand up to pee. You will go into a stall and sit down on the toilet, and pee like girl. Am I perfectly clear about this?”

The look in Danny’s eyes was priceless. He stood there, frozen in place, his male brain calculating the implications of what I had just said. He nodded once. “Yes, Mistress.”

“Welcome to my world, slave. Now go, get dressed. We have a long list of errands to do today.”

I watched Danny retreat into the bathroom. He had no idea what was ahead of him. Having to sit down to go pee was the least of his worries.

### Part 12, The Price of Submission

Our first stop was the local pet store, where I picked out a black leather leash while Danny watched. I had him pay for it out of his allowance, 12 dollars plus tax, leaving him with 27 dollars and change for the week. Next stop, Danny’s apartment, where I ordered him to strip and get in position. I collared him and let him kiss my feet. Then I snapped on the leash. “Okay, pet, be a good boy and lead me to your files.”

![thumb_GoPatchesGo6_640x360.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/08/thumb_GoPatchesGo6_640x360.jpg?lossy=1&strip=1&webp=0)

On hands and knees, Danny crawled to the hall closet, me following behind, keeping the leash short so he could feel it tug on his collar. At the closet door, he reached up and turned the knob and opened the door. He reached inside and pulled out a cardboard file carton. “My files are in here, Mistress.”

“Good boy, now push the box to the dining room table.”

Danny grasped the carton and crawled, pushing the box ahead of him into the dining room.

“Put it on the table, pet.” Danny lifted up on his knees and put the box on the table. “Now get in position at my feet while I go through your files, pet.”

Danny took his place to my right. I pulled out a stack of manila folders labeled “apartment,” “bank,” “cable TV,” “cell phone,” “insurance,” “mastercard,” “tax,” “truck,” “visa,” “work,” and so forth. I started with the apartment file. Danny was paying 850 dollars per month in rent for his apartment, plus approximately 150 dollars per month for electric, gas and water. Unless there was a missing document, it appeared his lease had expired. “This expired in August. Do you have a new lease, pet?”

“No, Mistress.”

#### Bill’s

“So you’re renting month-to-month?”

“Rick said he’d come by with the new lease, but he never did, Mistress.” Rick was the apartment manager and played on Danny’s men’s league softball team. He was one of the Monday Night Football regulars. He was a slacker, and now I was glad for it.

I moved on to the bank folder. Danny banked with a competitor. Looking at his statements, I saw his paycheck was deposited directly into his account, 1320 dollars every two weeks, 2640 dollars a month after taxes. Knowing how hard he worked, I was bothered by his underwhelming income.

Next folder. Danny paid 95 dollars per month for cable internet/TV and 125 dollars per month for his cell phone. On to insurance. Danny had no life insurance policy; the folder was for his auto insurance. The F-100 was insured for 8000 dollars. Collision, liability and medical coverage cost him a hefty 1300 dollars per year.

“Why is your car insurance so high, pet?”

“I totaled my previous truck the summer before I met you, Mistress.”

This was news to me. “And you were at fault?”

“I slid into a tree at night after veering from a deer.”

“Well that’s not your fault. Why did they raise your insurance rates?”

“The police report said I was speeding. I got ticketed for failure to maintain control.”

“And were you speeding?”

“I guess so, Mistress.”

I jerked Danny’s leash. “No ‘guess so,’ slave. “Were you speeding or not?”

Danny lowered his head. “Yes, I was speeding, Mistress.”

I went through the credit card folders. Danny was carrying a balance of 1252 dollars on the MasterCard and 4650 dollars on the Visa. He was making the minimum, interest-only payments on both, about 500 dollars per month combined. The interest rates were 17.5% on both cards. I was aghast. “Slave, I’m looking at your credit card statements . . . is this correct, you owe nearly six thousand dollars on your cards?”

Reluctantly, “Yes, Mistress.”

“And you’re making minimum payments each month on both?”

“Yes, Mistress.”

“I am not pleased, slave,” I fumed. “This is disappointing . . . you’ve been irresponsible.”

“Yes, Mistress,” Danny abjectly acknowledged.

I opened the folder labeled ‘truck,’ dreading I’d find a loan with more monthly payments. Inside was the title, no loan papers. “How is it you have clear title to your truck, slave?”

Danny explained, “I got six thousand dollars from the insurance company for my truck, and I had three thousand in savings. I paid cash for the F-100.”

“I see. How did you rack up so much credit card debt, then?”

“Originally it was for furniture and the TV. Then the trip we took last February to Cancun, Mistress. And for dinners out, and the concerts we went to last summer. Things like that.”

“Things like that,” I repeated, feeling a little guilty for letting Danny pay for our first big trip together to Mexico, and the concerts. He had insisted on treating, and being the ever-frugal girl, I had let him. Had I known he was putting the trip and concerts on high-rate credit cards, I would have balked.

The work folder was filled with Danny’s pay stubs. He earned 21.50 dollars an hour, 40 hours per week. “You don’t get paid overtime when you work late, slave?”

“No, Mistress.”

#### Taking Advantage

![thumb_HeWantsToBeMySlave7_640x360.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/04/thumb_HeWantsToBeMySlave7_640x360.jpg?lossy=1&strip=1&webp=0)

This news upset me even more. The shop was taking advantage of Danny. He should get overtime when he worked past five, which I knew was frequently. I pushed back my chair. “Stay, pet.”

I went into the kitchen and found a pencil and calculator in the junk drawer. I poured myself a glass of water. I returned to the dining room table. I was about to sit, but instead returned to the kitchen, filled a bowl with water and brought it back, setting it under Danny’s nose. “We’re going to be here a while, pet. Drink if you get thirsty.”

“Thank you, Mistress.”

I did my calculations on the back of a file folder. It didn’t take that long, about twenty minutes to devise a short-term financial plan. Danny wasn’t going to like it, but too bad, he brought it on himself. “Slave, you have been quite irresponsible with your finances,” I admonished him.

“Yes, Mistress.”

“I’ve devised a plan to get you out of debt. It will be quite painful.”

Silence. I think Danny knew his life was about to change. Radically.

“I want you to listen carefully, slave. You’re moving out of your apartment immediately. Give Rick notice today. You’ll move in with me. You’ll pay five hundred dollars a month in rent and three hundred dollars for your share of food and utilities. Your paycheck will go directly to an account I’ll set up at my bank. From your income I’ll take out your room and board and pay down the credit card balances. Once you are out of debt, I’ll put any extra in a savings account. Understood?”

Long pause. “Yes, Mistress.”

Without thinking, I reared my right hand back and swung it down on the left cheek of Danny’s ass. His body jerked in surprise and he let out a guttural yelp. The blow left a red, hand-sized mark. I swung again, this time on the right check, even harder. Another jerk of the body and this time a choked, primal grunt. Another hand-shaped red mark. No words were exchanged; I was beyond words. I didn’t have to explain my anger. After all, his ass was mine now. My hand stung, but I felt better. I stood up and put the file folders back in the box. I jerked on my slave’s leash. “Come, pet. We have errands to do.”

The girls at the bank fawned over Danny and teased him into admitting he had sent the roses. I took pleasure in the thought of the cage locked around my slave’s manhood. Look all you want, girls, I own this hunk: cock, stock and barrel. When I set up the joint bank account in Danny’s name and mine, my boss and best friend Rebecca, the bank manager, assumed that meant we had become a couple and were joining finances. I made no effort to correct her, because after all, that was pretty much the case. Only in this case, I was taking over Danny’s finances entirely. His ass and his money were mine.

#### City Hall

![morecleaningless.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2015/09/morecleaningless.jpg?lossy=1&strip=1&webp=0)

Next to city hall. The parking fine was 30 dollars, so that left my slave with 25 dollars of his 40-dollar allowance to live on for the rest of the week. He’d be eating bag lunches from here on out.

Our last stop was the DMV office, where I took possession of Danny’s truck for 1 dollar and other valuable considerations. With clear title in hand, I filed for a vanity plate: “HER-F100.”

Starting tomorrow, Danny would be riding a bicycle to work. Back home, while he went to the tool shed to pump up the tires of my bike and lubricate the chain, I called my insurance company and put the truck on my policy. It was 595 dollars per year, but totally worth it—I had always wanted a truck of my own.

That night Danny slept on the floor of the basement, collared and short-leashed to the pole. It’s cold and damp in the basement and I kept my slave naked, so I gave him a one-inch thick yoga mat to lay on the concrete and a comforter to wrap around his body. Before I went upstairs I ordered Danny into position. I knelt down on one knee next to him and stroked his back, then slipped my hand down the crack of his ass to his balls, circling my thumb and forefinger around his scrotum and gradually tugging his balls away from the collar of his cock cage until I was able to collect them in my fist.

“Who owns your balls, slave?” I murmured into Danny’s ear, stroking his neck with my left hand while I tugged downward with my right.

“You do, Mistress,” he gasped.

“And who owns your money now, slave?”

“You do, Mistress,” Danny acknowledged.

“And who owns your truck, slave?”

I think this was the hardest one of all. “You do, Mistress.”

“What do you own now, slave?”

Silence as Danny pondered the reality of his new existence.

I tugged down harder, making his lower back sag. “What do you own now, slave?” I demanded.

“Nothing, Mistress.”

I squeezed his balls together like big marbles, making him squirm. “That’s right, slave” I hissed softly into his ear. “I own your balls, I own your body, I own your money, and I own your truck. You own nothing; you are mine to do with as I please. Isn’t that right?” I gave a sharp downward jerk.

Danny writhed in pain. “Yes, Mistress,” he gasped.

I released his balls and rose to my feet. I wore pink slippers. “You may kiss my feet, slave.”

Danny dropped his head and began kissing my pretty toes. “You’ll be sleeping here until further notice, slave,” I informed him. “If you perform well, in time I’ll consider moving you upstairs and let you have the privilege of sleeping at the foot of my bed. You’d like that, wouldn’t you, slave?”

“Very much, Mistress,” Danny agreed, pausing between kisses.

I looked down at him, thinking how much I would miss having him to spoon in bed. This would be hard on me, too. I looked wistfully at his sculpted body. What strange madness that the man I loved would sleep on the cold, hard basement floor tonight while I slept directly above him in my soft, warm bed. But this was what he needed most. I knew it in my mind, but my heart cried out.

### Part 13, His Mistress’s Sex Life

Friday morning, I got up early and made coffee. I hadn’t slept well, tossing and turning all night, troubled by thoughts of Danny freezing on the basement floor and having second thoughts about completely ruling the life of another human being. But I loved Danny and had committed to this; there was no backing out now. I felt sort of like I do before a long trip, when I have second thoughts about leaving home and going to the airport, enduring the noise and chaos, the crowds, the long security lines, sitting next to strangers in a confined cabin, all the unpleasantness of modern travel. Then you finally get to your destination, meet new people and have adventures you’ll remember for a lifetime.

I had purchased a large leather valise in which I kept my mistress books, print-outs from internet BDSM resources, Danny’s survey, the contract and of course, the kinky toys and tools. When the coffee was brewed, I poured a cup and took the valise into the dining room. I thumbed the combination locks and flipped the latches. The leather scent of the flogger filled my nose. Setting the flogger and riding crop to the side, I went through the papers, reviewing Danny’s survey and the contract.

![IMG_0172.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0172.jpg?lossy=1&strip=1&webp=0)

#### Daniel Simon Barton

Daniel Simon Barton, age 24, of sound mind and body. Looking at Danny’s signature reassured me. He wanted this. And the truth was it was fun, as long as I wasn’t racked by doubts and puritanical guilt. Why not just take Danny’s word that he wanted this experience, he dreamed of this lifestyle? Why not just go with it and enjoy the benefits? After all, the hardest part was behind us, we had arrived at our ‘sexcation’ destination—a suddenly erotic home of kink that housed a slave and a slave-owner who could snap her fingers and have her every whim and desire met. Like breakfast in bed . . .

That gave me an idea. I snatched up the riding crop—might as well give it a try—and padded down the stairs to the basement. I was relieved to feel the air was cold but not frigid. I flipped on the light to see a turtle-shaped mound under the comforter, at the base of the center pole. For some odd reason, I was reminded of the snake that eats an elephant and looks like a hat in The Little Prince.

I strode over to the mound, took a handful of the comforter and lifted it into the air, exposing the naked man underneath, curled into a fetal ball. Sleepy boy’s eyes fluttered open. I slapped him with on the thigh with the tip of the riding crop—a doubled flap of leather about the length of a thumb and two fingers wide. It made a satisfying snapping sound. Danny’s leg jerked.

#### Rise and Shine!

“Rise and shine, sweetheart!” I commanded, snapping the crop against his thigh again. His whole body jerked this time and stiff muscles came to life. He rose unsteadily on his hands and knees. Aiming the tip of the crop at Danny’s ass, I rained down a shower of quick, light blows that raised red blotches on the white skin and sent blood coursing through his body. “Listen up, pet. I want you to go upstairs to the bathroom and take a hot shower, then dry off and crawl to my bedroom.

“Yes, Mistress,” Danny replied groggily.

I reached down and unwound the leash from the steel pole. “Move, pet, we have to be out the door in an hour, and I have plans for you.” I slapped the crop against Danny’s ribs and he scurried forward, crawling on hands and knees toward the stairs. I kept up the rain of blows landing on his sweet ass as he climbed the stairs, crawled through the dining room and into the bathroom. By the time he got through the doorway and scrambled onto the tile floor next to the tub, he was breathing hard.

“Five minutes, slave. I expect you to knock on my bedroom door in five minutes.”

I retired to my bedroom, stripped off my pajamas and crawled back into bed. I fluffed the pillows and settled into a semi-upright position against the headboard. I took the small, bullet-shaped vibrator from the nightstand drawer and turned it onto my favorite setting. I put the vibrator against my clit and felt it work its magic while I leaned my head against the pillow and closed my eyes. I heard knocking on the door just as I was starting to get aroused.

#### “Come in, slave.”

![IMG_0173-1.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0173-1.jpg?lossy=1&strip=1&webp=0)

The door swung open. With my free left hand, I patted the mattress between my legs, keeping the vibrator against my clit with my right. “Climb up here, slave, put your face between my legs.”

Danny climbed up on the bed. I saw his cock was hard, filling the cage. He crawled forward to my crotch. I could tell he was surprised to see me using the vibrator. He knew I owned one, but I had never used it in his presence, unconsciously thinking that it would be an affront to his manly prowess. I mean, after all, I had his lips, tongue and cock to arouse me, right? Well, all that had changed. He was my slave now, and I would arouse myself any old (or new) way I pleased. And it pleased me to use both the vibrator and his tongue at the same time. “Put your tongue out, pet,” I commanded.

Danny extended his tongue, which was nicely shaped, pink, clean and all mine. “Now press your tongue softly against my cunt, slave, very softly, like you are licking the petal of a flower.”

Danny inched forward and put his tongue against my labia, ever so lightly. I pressed down harder on my clit with the vibrator. “Okay, now a little more firmly, make slow circles with your tongue.”

#### Following Directions

Danny did as he was instructed, and it was heavenly. I took the back of his head with my left hand and drew him in. “Put your tongue inside me. Move it in and out—gently—like it’s your cock.”

Danny began probing my vagina with his tongue. I circled my clit with the vibrator, and swiftly felt the welling of an orgasm in my thighs. My spine tingled, starting from the nape of my neck and working down. It intercepted the current rising up my thighs, the two currents intersecting at my clitoris. Then came the explosion, radiating out in every direction, making my toes curl and my scalp tingle.

God, I love my orgasms. I held Danny’s face hard against my cunt and let him feel the muscles pulsating in my vagina, waves of pleasure flooding through my body, then receding like a tide going back out to sea. I realized I was holding Danny’s head with both hands; I had dropped the vibrator. I heard him choking a little, struggling for his breath. I relaxed my grip and let his face withdraw a few inches.

“Mmmmmm, that was nice, slave. Now let’s do it again. Lay on your back, and put your head here.” I patted the wet spot on the sheets between my legs. As Danny rolled over, I slipped around to the side and then straddled the top of his thighs, so that his caged cock was directly in front of me. There was no way I could possibly take it inside me, but it was erotic just the same, looking like some caged rocket, Danny looking back at me and longing for me to uncage him and mount his throbbing cock.

I took the cage in my hands and bent over, swept my tongue across the swollen flesh between the stainless-steel wires. I opened my mouth wide and took it deeply, sliding it in and out between my lips. Danny’s breathing accelerated. I didn’t want him to cum, so after a teasing minute or so of deep throat fellato, I stopped. I lifted up and over the cock cage and settled on Danny’s heaving chest.

![IMG_0174.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0174.jpg?lossy=1&strip=1&webp=0)

From his chest, I slid forward, straddling his neck. I spotted the vibrator in a fold of the sheet, near the edge of the bed. I reached over and took it between my thumb and forefinger, then placed it against Danny’s lips. “Suck my little friend, slave. Suck it, just like it’s my dick.”

Danny puckered his lips and I inserted the lipstick-sized red bullet. He began eagerly sucking on it.

“I bet you wish it was bigger, don’t you, slave? I bet you wish it was as big as your dick, and you were sucking my hard cock, don’t you?”

Of course, Danny couldn’t speak, but I saw it in his eyes, yes, yes, he wanted that, yes, yes. I pulled the vibrator from his mouth and pressed it against my clit. I moved forward, laying my cunt on Danny’s mouth, settling down so that my crotch completely enclosed his face, so that my sex suffocated him, so that he could hardly breath. His arms were stretched out perpendicular to the bed, fingers clawing at the sheet. I rose up an inch, letting him breath. He took in a long, sucking breath. “You like me sitting on your face, don’t you slave, my cunt against your mouth. You can imagine dying that way, can’t you?”

#### Imagine dying like that!?

I knew he did. Once, riding Danny’s cock, I leaned over and buried his face with my breasts, feeling him greedily suck my nipples. He pushed me away for a second and blurted that he could die this way. La petite mort. I looked down into Danny’s eyes. “Well?”

His liquid eyes were deep, a bottomless well. He blinked. “Yes, Mistress.”

“Well, I won’t let you die just yet, but you may think you are, slave.” I eased back down and began rotating my hips, grinding my cunt against his face. I held the vibrator to my clit, felt its vibrations drill down to my core, feeding into the sexual current that flowed through my body. Harder now against Danny’s face, totally enveloping him. His arms reached in, hands grasping my waist, pulling me down against him. He loved it. I was suffocating him and he loved it. Why that hit the orgasm button I don’t know, but I just suddenly exploded, harder than I ever have in my life. I felt myself squirting into my slave’s mouth. Drink or drown. I felt Danny gulping down my release.

I wondered what this discharge was made of. Was there such a thing a female ejaculate? I didn’t know. But this was the second time this flood coursed out of me. Danny’s grip on my waist slackened and his chest heaved. I lifted up and he sucked in air like a man breaking the surface after a long dive.

Hot tears leaked from the corners of my eyes. I lifted off Danny’s face and shifted down to his chest. His face glistened with my juices, brown eyes wide and blinking, looking up at me with awe, adoration, lust and desire. I shifted down until his caged cock pressed against my derriere. I began to rise and fall, lifting with my thigh muscles, rubbing the cage in the crack between the cheeks of my ass.

“Oh, please, Mistress,” Danny moaned.

“Please what, slave?” I mocked him.

“Please let me come.”

“Please let me come, Mistress,” I corrected, my ass rising and falling against the cage.

“Please let me come, Mistress,” he begged.

#### Not today Pet

I looked at him thoughtfully. My gaze went to the alarm clock. We had thirty minutes to get out the door. “Not this morning, pet. But thank you for servicing me so well, you’ve been a good boy.”

The expression of lust and frustration radiating from Danny’s face was intense, almost frightening. He was desperate. “Please, Mistress,” he demanded, “just let me cum!”

![IMG_0175.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0175.jpg?lossy=1&strip=1&webp=0)

My reaction was instantaneous and unplanned. I slapped him across the face, hard enough to make his face jerk and sting my hand. I was shocked by what I had done. Filled with remorse, I reached out to caress his cheek. Blood rose to the surface where I had struck him.

Should I apologize? My instinct told me no, that would be a mistake—even if it had been a mistake to slap him. I was learning as I went; mistakes were going to happen. Reading mistress manuals didn’t prepare you for moments like this. Tenderly, I said to Danny, “When I say no, pet, I mean no. Never beg unless I order you to beg, or you’ll be punished immediately. Is that understood?”

Danny nodded. “Yes, Mistress.” I searched his eyes, looking for a flash of anger, or even worse, disappointment. But that’s not what I saw. I saw something else—respect.

“You need another shower, slave, and so do I. Go to the bathroom and turn the water on, wait for me by the side of the tub,” I ordered. “I’ll be right behind you.”

“Yes, Mistress.” I swung myself to the side of the bed so Danny could get up. He slid off the bed and stood up. I let him take one step. “Slave, you will get down on the floor in my presence unless I tell you otherwise. Crawl to the bathroom.”

Danny dropped on all fours and crawled out the door. I flopped down on the bed, rolled over on my back and stared up at the ceiling. Oh. My. God.

I had become a Mistress.

And I liked it.

### Part 14, Halloween Humiliation

Rebecca’s annual Halloween party was legendary, an event not to be missed. Things happened at her party (things besides costumed adults acting like happy children, eating candy and getting drunk). The universe shifted, couples fell in love. I got laid by Danny for the first time on the night of her party.

![IMG_0126.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0126.jpg?lossy=1&strip=1&webp=0)

My boss lived in the perfect house for a large party, a big, three-story, pink brick mansion in one of the old-money neighborhoods of town, with a tall ornate wrought iron fence circling the property and a red brick walkway leading up to the front door. Between Rebecca’s salary as a bank manager and the retirement income of her (second) husband Hamilton, they owned one of the nicest homes in town.

A bit about Rebecca and her husband. He is a distinguished professor emeritus of Eastern Studies from UVA. Hamilton is his last name; I don’t know his first name, because that’s what Rebecca calls him. Hamilton is bright, handsome, professorial, worldly, an engaging conversationalist and a wonderful host. He does all the party decorations, bartends and manages a small army of college kids who swarm the house passing out Halloween-themed hors d’oeuvres and liquor shots. Rebecca, 20 years younger, is tall, model-thin, statuesque, a platinum blonde and always drop-dead gorgeous in her costume. She is the perfect hostess, gliding from guest to guest, guessing identities and making everyone feel special.

Rebecca and I have a mentor-mentee relationship going back to the day she hired me out of college. She saw something in me, and I have worked hard to meet her expectations. Outside of work, she has been a friend beyond measure. She helped me buy my house with a personal, interest-free loan to swing the down payment. When my dad died, she was there for me, especially after the funeral when mom took off and I was left executor of the estate. At one point during this difficult period, I was such a wreck that Rebecca had me stay with her at her home for a weekend. It was during this stay that I saw how lovingly attentive Hamilton is to her, the kind of husband every woman deserves.

#### The Event

The event that bears mention from this weekend, because of the events that follow, is that on the second night of my stay, a little after midnight, Rebecca knocked on my door and came into the room. She climbed into bed with me and we talked into the wee hours, and then we slept together, spooning like two sisters. I must admit that clinging to her exquisite body, I was more than a little aroused. I had played around a little with my first roommate in college, but that was simply two girls exploring their sexuality, while with Rebecca, I felt sexual energy flowing between us. Nothing happened; we got up in the morning as if we had slept in separate beds. I knew nothing about her relationship with Hamilton, but apparently, it allowed for cuddling with female guests.

Ever since, when Rebecca greets me in private, she embraces me and kisses on the lips, and I can’t help but respond in kind. I know, I know, not the proper way to relate with your boss, but Rebecca is so much more than a boss. I am deeply grateful for her counsel and friendship, and yes, when it comes to her (and her alone) I guess I’m a little bi-curious.

Back to the Halloween party. In the week that preceded it, while I worked on our costumes upstairs in my sewing room, my slave slaved away in the basement. Danny is a capable carpenter, so I had him construct a small storage room where I planned to put his things (once we cleaned out his apartment). In another corner of the basement, I had him construct a “dungeon.” I put that in quotations because at this point, it was simply a framed-in room with plywood walls and modest potential.

I wanted my dungeon to be subtle and easily disguised (in case mom ever visited). So instead of a gaudy St. Andrew’s Cross, I had Danny spread-eagle against one wall and marked the appropriate spots to install screw-eyes for the restraints. On the opposite side, I took an idea from a neighbor’s basement gym and had Danny hang a pull-up bar from wood extrusions bolted to the rafters, the bar a wooden dowel with screw-eyes in both ends, so I could suspend him up on his toes, with a matching dowel with screw-eyes serving as a spreader bar between his ankles, leaving him spread-eagled and allowing me to get behind him with the flogger. Finally, I had him construct a whipping table. I upholstered it with padded leather, with ankle and wrist restraints on the legs near the floor. It looked deliciously evil.

Of course, Danny loved making all this. It was fun to come down to the basement and find him all sweaty, covered with sawdust, his cock hard in its cage as he sawed and hammered wood, constructing the dungeon where his sadomasochistic fantasies would play out, where he would endure his harshest trials and build a love-hate relationship with his Mistress’s instruments of punishment.

The final touch was a coat of paint. I rejected “dungeon black” (boring and too obvious) and instead went with a feminine shade of lavender. If mom ever visited, I’d throw a tarp over the whipping table, fill the room with boxes from the attic, hang garden tools on the wall and call it my home and garden storage room. Decorating genius.

#### Halloween

Halloween came on a Saturday that year, which made it perfect for the day I had planned in detail for my submissive slut. We started with a haircut. I think it hurt me more than it hurt Danny to see those long locks of gorgeous black hair fall away, his James Dean identity falling away with them to the floor. He looked like a military man when we were done, reminding me a little of my father, which I found somewhat unsettling but also a revelation.

I found cutting off his hair immensely erotic, like Delilah cutting off Samson’s hair, only with nicer intentions (which were to emasculate my slave, not betray him to the Philistines). Also, unbeknownst to Danny, I was preparing him for a job interview later next week, one that Rebecca had helped line up, a positon as assistant manager at University Auto and Tire. It meant a substantial increase in income.

Next came a manicure and pedicure. That totally freaked Danny out. He would’ve balked at the door if I hadn’t given him the Mistress eye. The girls in the chairs around him loved it, and complimented him on having the masculine confidence to have his nails done. Ha, if they only knew what I had in store for him. I may have gone overboard when I had his nails painted cherry red, but it went with his shoes.

Things got serious that afternoon in the bathroom, when I cuffed Danny’s wrists to the shower nozzle, lathered him up and then shaved all his body hair off, including the public hair around his cock and balls. He was as smooth as a newborn when I was done. It was at this point I informed him he was going to be a girl tonight, and not just any girl, but a girl in a sexy nurse outfit. (I had abandoned plans for the flight attendant uniform; it seems the days of sexy stewardess outfits has passed, alas.)

#### Submitting to his Mistress

![thumb_ServingMissKatrinaPart21_640x360.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/06/thumb_ServingMissKatrinaPart21_640x360.jpg?lossy=1&strip=1&webp=0)

I think it was at this point that Danny realized my powers extended to scenarios he never fantasized about, for example, submitting to Mistress playing dress-up with her new doll. Halloween afforded the perfect opportunity for exposing his total submission in a humiliating and yet socially acceptable venue. He may not have liked it, but he had a big hard-on, so clearly something about the dynamic aroused him. I don’t think it was so much being dressed as a girl, it was having his manhood taken from him.

After the shaving, I kept on stripping his masculinity, sitting him in front of a mirror and trimming his eyebrows and then applying foundation, mascara, false eye lashes, eye liner and lipstick. Next came the wig, which I had spent considerable effort obtaining, a luxurious wig made of real, wavy blonde hair, just over shoulder length. The effect was mind-boggling. Danny made a very attractive woman. Who knew?

Hair and makeup out of the way, next came the dilemma of what to do with his man parts. It would be unseemly to have them bulging in the wrong places. I wrapped his balls tightly with two-inch medical tape, and then pulled them back between his legs, so they were tucked up and out of the way in crack of his butt. I ran the tape up and around his waist to keep it all in place. It was uncomfortable, and Danny wouldn’t be able to go #2 for the evening, but we discussed that before I taped him. And I planned on carrying a small pair of scissors for emergencies.

Next, what to do with his dick. He had a hard-on that I hoped would eventually subside, so for now I taped it down and stuffed it in his tight lycra panties. Next came the lace-up corset, which gave him a remarkably tapered waist (and made it difficult to breath, part of the plan). The corset came with a padded pushup bra. With extra padding, I made him a voluptuous 36R. Frankly, I was a little jealous.

#### The Costume

![IMG_0228.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0228.jpg?lossy=1&strip=1&webp=0)

Finally, the costume itself, my own creation, a sexy heartbreaker in hospital white with red pipping around the bodice, a three-inch red belt with a white cross at the center of the waist, with a short, flirty skirt at mid-thigh, flirty white ruffles underneath. To finish it off, white fence-net pantyhose, cherry-red pumps with two-inch heels, a stethoscope, white gloves and a cute nurse’s cap.

Holy shit, Danny looked sexy. He even had a great pair of legs, especially when I had him coyly lift a pump to the toe and turn one knee in front of the other. I had him strike various seductive poses while I snapped photographs with my iPhone. On the spot, I decided these were going in a new Mistress photo album. And not just these photos, but photos and videos of Danny in all the compromising erotic scenes that were sure to follow. I’d let my sub know I was taking the photos and videos, and if he ever acted up, it would be perfect material to blackmail him into obedience.

Or maybe I’d play slideshows and videos on a laptop placed outside his cage during long periods in confinement, just to help remind him of who he had become—my submissive, sissy slut. The possibilities were endless, but for now, I simply had to capture the images of him as a nurse because he was just so damn sexy. Too bad I couldn’t post them on Facebook.

I went to the party as an airline captain, wearing a navy-blue coat with four stripes sewn on the bottom of the sleeves, silver plastic wings on the chest, man pants, black patent leather shoes, my hair pinned in a bun underneath a pilot’s wheel cap. I glued on a mustache and penciled eyebrows to match. I looked the part, but the truth is, Danny stole the show. Rebecca (stunning as Cleopatra) knew instantly it was Danny and me, but many guests had no idea of sexy nurse’s true gender. I instructed Danny to speak in a breathy whisper to disguise his voice. Most women eventually figured out he was a guy, but the men were blinded, drooling at the hot nurse. Dim lighting, I guess. Dim men, too.

Throughout the night, every chance I got, I whispered into Danny’s ear about how hot he looked, how I couldn’t wait to get him home and screw him (not that I had a strap-on at this point). As the party wore on, I decided it would be good for Danny to circulate on his own and get the experience of being a hot chick at a party full of semi-inebriated men and see how that felt. Each time he came back to me, he would shake his head and beg me to take him home, and when I refused, he’d toss down another drink.

Before we left the party, at the stroke of midnight, I pulled Danny into one of Rebecca’s many spare bedrooms. I locked the door and had him kneel in front of me. I pulled down my pants, grasped the back of his head and pulled his red painted lips against my cunt. I forgot the vibrator, but just the sight of a hot blonde nurse with her face buried in my sex was such a turn-on that I came in a few minutes—hard. Afterwards, wiping Danny’s smeared face with tissue and reapplying lipstick, the episode with Rebecca came to mind—I realized it had been in this very bedroom. Funny how things played out that way.

#### The Local Pub

The last event of the night was a visit to the local pub. It was not the rowdy bar scene I had hoped for, just two guys drinking beer at one end and a few patrons in the adjoining restaurant having a late meal, but the purpose was accomplished, dragging my sissy slave to a public venue and subjecting him to the scrutiny of strangers’ eyes. The two guys kept looking our way and the bartender, a grad student by the looks of it, was smitten. He looked my sexy companion up and down before asking her what she wanted to drink—on the house. Squirming like a virgin, Danny answered in a soft falsetto that he would have a vodka tonic. I had my first drink of the night, a beer, something a pilot might drink (although one would hope not while in uniform).

Well, it was Halloween, so anything goes. We enjoyed a quiet drink together and left. When we got home, I finally rewarded my slave. I stripped off his girly nurse outfit, pulled the wig, wiped off lipstick and mascara, unwound the tape from his tormented balls, and then released him.

Unlike Samson, Daniel Simon Barton’s strength had not been lost with his hair. He picked me up, threw me down on my bed and fucked me like a whore.

### Part 15, The Virtues of Silence

My father often extolled the virtues of silence when we were growing up. He said it brought peace at home, peace inside your head. He said the strongest leaders listen and learn. He said you should let the other guy do most of the talking, you’ll learn more. He said, when you have something to say, say it, but otherwise, be silent and listen. He said only a fool argues with an idiot. And so on, platitudes about silence with a grain of truth to them. Not that his silence helped his relationship with mom. But he and I shared a wonderful affinity for the unspoken word. One look and I knew exactly what was on his mind.

![IMG_0231.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0231.jpg?lossy=1&strip=1&webp=0)

With my slave, I decided to see what good might come from the rule of silence, so I bought some ball gags from Stockroom, my favorite being a head harness with a ball gag/dildo combo, the idea being that the slave is gagged with a six-inch dick sticking out of his face, staring into his Mistress’ cunt when she mounted the dildo. She can put him on his knees, hold him by the ears and pump away, or lie him on his back and ride him. Poor slave, so close yet so far away. Seriously kinky. I couldn’t wait to try it.

The other half of my silence strategy was a series of short verbal commands accompanied by hand signals (or just the hand signal) to indicate the position I wanted my slave to assume.

One finger meant “Position One,” with slave standing in front of Mistress, his right hand holding his dick in a vertical position while the left hand wrapped tightly around the scrotum so that his balls were squeezed into a tight package, presented to Mistress. I had learned that when Danny’s balls were tightly collected in this fashion, they were exquisitely vulnerable; all it took was a flick of the wrist, a finger slap, and I had his full attention. A snap with my hair brush made his knees buckle and put fear in his eyes.

Two fingers for “Position Two,” slave standing with legs spread and hands behind head. And so on, Position #3 was the same as two only slave on his knees. Position #4 meant (fittingly) to go down on all fours. Position #5 (or simply “get in position”) meant to get into a position of supplication at Mistress’s feet. In all positions, the slave was to be silent, eyes cast down, focused on the floor at her feet.

This way, I could say a few words or make a simple gesture and my slave knew which position to assume. I loved being able to look at him and lift a single finger and have him snap into a submissive position. If he had clothes on, the poses were all the same except for Position One, where he was to drop his pants and underwear around his ankles and present his Mistress’s cock for her attention.

#### Listening to Learn

The ball gags introduced a whole new dynamic, because now Danny couldn’t cry out his safe words “red” and “yellow” (which we had yet to use). In lieu of a safe word, I instructed Danny to alert me of trouble with three short grunts or three stamps of his leg or three repetitive motions of any extremity or body part he could move. Otherwise, it was up to me to assess the degree of stress or the severity of the punishment and decide if I could risk using a gag, the tradeoff being the slave’s increased helplessness. How sweet the muffled sound of a slave crying out in agony and moaning behind his gag.

![IMG_0232.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0232.jpg?lossy=1&strip=1&webp=0)

The downside of silence was that it prevented verbal communication, which left a girl wondering how her beloved was faring. And then there was the broader issue of communication between Mistress and her slave; which forms of corporal punishment were pushing my slave’s buttons best? To solve this issue, nerdy me, I wrote another questionnaire.

The questionnaire listed every form of physical punishment, each with three scales, the scales rated from 1 to 10. The first scale was for how much Danny liked the form of punishment (example, flogging), the second scale was for how hard the blows had been experienced to date, and the third scale was for how hard he wanted to experience it. I left room for written comments.

I’m glad I did the questionnaire, because Danny’s responses were a revelation. For example, to my amazement, he liked being slapped, rating it a seven. And that slap that came out of nowhere that I felt so guilty about? He rated it a five. A five! He wrote that he liked the spontaneity of a slap, that it gave him instant, intimate feedback from his Mistress. He claimed he would be fine with an eight or nine.

And so it went. Without exception, Danny’s numerical responses indicated that I had been going easy on him, that he would like it harder. Wow. Even with verbal communication, that hadn’t come across, probably because a slave isn’t going to ask his Mistress to hit him harder; that would be topping from the bottom. So now I knew. Not that I would necessarily be able to punish Danny as hard as he seemed to want; after all, I had my own limits, but at least now I knew how far I could go.

I realized I could use the same scale real-time while administering blows. I’d just whack at him and demand, “Rate that, slave.” He’d say “six” or whatever and then I’d have direct feedback on how hard he had been struck. After I got calibrated, I could safely gag him, knowing how hard I was hitting him without further verbal feedback. I wasn’t too worried about him needing to use a safe word, because of my own personal limits; I would have a hard time striking harder than a seven. I could never strike him so hard as to break flesh. I might cut off his hair, but there would be no blood on my hands.

#### Thanksgiving

The weeks of November fled by. We moved into a comfortable Mistress/slave routine, with Danny bicycling home from his new job around seven (later than I liked, but the price of being in management). He’d come in the kitchen door, strip, put on his collar, then crawl to wherever I happened to be in the house, usually on the living room couch. He would kiss my feet and I’d snap his leash to the collar with an affectionate but formal greeting, “Welcome home, slave,” then silently continue what I was doing, Facebook or reading or watching TV, while he waited patiently. Sometimes I’d have him massage my feet while he waited, sometimes I made him my footstool, sometimes I’d order him to the dungeon. When we eventually got around to dinner, he was happy to eat whatever I fed him from his bowl.

The Wednesday before Thanksgiving that year stands out in my mind as the day when my role as Mistress truly solidified—and for a completely unexpected reason. We were going over to Rebecca’s house for dinner the next day. Before I left work, she called me into her office. Looking up from her uncluttered desk, she said, “We’ll see you at three tomorrow afternoon, then?” An errant strand of platinum blonde hair fell across her forehead. As always, she was radiantly beautiful, dressed in a demure white silk blouse with a beautiful gold necklace and matching earrings.

“Yes, at three,” I replied. “I’m still planning on brining my green bean casserole.”

“Vegetable in name only,” she laughed, alluding to the stick of butter and crumbled cracker crust that made my casserole a heart attack in a dish. “And you’ll be bringing young Mr. Barton with you?”

“Of course.”

“Good. We look forward to seeing him. I presume he won’t be wearing a dress this time.”

I laughed. “No, regular guy clothes this time.”

Rebecca raised an eyebrow and said mischievously, “And how is his training going?”

“You mean at work?” I replied, assuming she was referring to his new job.

“No, I mean at home, darling,” Rebecca responded, her eyes smiling but boring a hole through me. My jaw dropped; my face must have turned sheet-white. I felt stricken. What did she know?

“Why don’t you have a seat, Vanessa. Let’s have a little girl talk.”

I collapsed into one of the seats across from her desk. How could she know?

Reading my mind, Rebecca said, “Honey, I know exactly what you’re doing with Mr. Barton.”

“Wha . . . what do you mean?” I stammered. Had she been peeking through my windows?

Rebecca looked at me kindly. “Vanessa, you know the saying, where there’s smoke, there’s fire? Sweetie, I’ve been smelling smoke for a month now. First the new joint account, then you start driving his truck—I love the vanity plates by the way—then him wearing a nurse outfit at my party, then the short haircut, the new job, his paycheck deposited into an account that only you control—”

“His finances were a mess; I’m just helping him get out of debt,” I protested.

“Oh, I know you are.” Rebecca rose from her seat, walked around the desk, and plunked down in the seat next to me. She took my hands. “Vanessa, I approve. I approve of everything you’re doing.”

What did she approve? Me taking over Danny’s finances? “You do?” I stammered, still wondering just how much she knew. But how could she know? I felt hot, like I had been caught stealing money from the bank. I felt flushed. I must have been beet red.

“Yes, I do,” Rebecca insisted, leaning in close. She brushed my right cheek with her lips and put her mouth to my ear. “Vanessa, darling, welcome to the club,” she whispered throatily. “I knew from the day we met you had in it you . . . I’m so proud of you.” She sat back and beamed at me.

I looked at her in shocked silence. She couldn’t know. But somehow she did. If I was just silent I could bluff my way out of this. No, I couldn’t . . . not with Rebecca. “How do you . . . what do you—”

“Vanessa, it takes one to know one. Hamilton is my slave. We’ve been in a female led marriage for over a decade, now. I am his Mistress.”

She crossed her legs and sat primly in her seat, a Cheshire grin on her face. It seemed to me as if she had just informed me that she vacationed in Maui, same as me, so we could dispense with telling each other about our vacation adventures. Hamilton is my slave and I am his Mistress. Hearing her say those words sent me falling through the looking glass, Alice in Wonderland meeting the White Rabbit and the Queen of Hearts . . . I didn’t know what to say. I still felt like I had been caught in a crime.

Rebecca spoke, “Honey, you should be relieved to know there are other couples on the same path. It’s nothing to be ashamed of, it’s just who you are. It’s no different than being gay or lesbian. You are a strong woman and you found a good man to mold into a better man. And I approve . . . that’s it.”

“So you . . . you and Hamilton . . . he—”

“He does exactly what I tell him, Vanessa. He is my submissive slave. And he loves it.”

I just sat there, trying to comprehend her words, which seemed so foreign even though she was describing exactly the relationship Danny and I had embarked upon. Why was it so shocking to hear it spoken out loud? I guess because until that moment what Danny and I were doing was shrouded in my own mind with a cloak of secrecy, as if we were partaking in society’s greatest taboo. How silly.

![thumb_PamperingHer3_640x360.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/09/thumb_PamperingHer3_640x360.jpg?lossy=1&strip=1&webp=0)

#### How silly

Rebecca took my hands again. “I know this is a shock, but I suspected from the start that you had started down this path. After Halloween, I knew for certain. You are a dear friend; how could I remain silent when I know exactly what you are going through?  And that’s all you need to take away from this, Vanessa, if you need someone to talk to, if you need advice, I am here for you.”

Silly me, I began weeping. Rebecca stood, went to her desk and brought back a box of Kleenex. I went through a few tissues, collecting myself, finding my way back through the looking glass. Rebecca was too kind to be the Queen of Hearts, she was not a tyrant . . . she was more like the Red Queen, cool, calm, fair, the Governess offering Alice guidance. Offering me guidance. I realized that our friendship was about to go to a deeper level. She was a gift from heaven. I had a million questions for her.

I blew my nose and said, “If everything you’ve said is true, and I know it is, then I’d like to bring Danny over tomorrow wearing his collar.”

Rebecca smiled broadly. “Now you’re talking like a Mistress.” She reached out her hands, lifted me to my feet, pulled me into an embrace and said in a naughty voice, “But I have a better idea.”

### Part 16, The Mistress Sisterhood

Danny came home early that Wednesday, four thirty, an early start to the four-day Thanksgiving holiday. I heard him come in the kitchen door, I heard the junk drawer opening and closing; I knew he was stripping and putting his collar on, good boy, a pause while he got on his hands and knees and crawled through the hallway and into the living room. Imagine his shock when he spotted Rebecca’s elegant legs crossed next to mine. I could just imagine what was going through his submissive head.

To his credit and credit my good training, he paused only a fraction of a second, then continued forward until he was at my feet. I was wearing black pumps with 2” platform heels, a nice counterpoint to Rebecca’s strappy 4” white stilettos. “You may kiss my feet, slave,” I said in my best Mistress voice, hoping that Rebecca approved so far. I let Danny finish his worship and then said, “And now you may kiss Mistress Rebecca’s shoes . . . and thank her for helping you get your new job.”

Danny shifted two feet to his right, until he was directly in front of Rebecca’s expensive high heels. He dipped his head and politely kissed the top of the sharply pointed toe of each shoe. “Thank you for helping me get my new job, Mistress.”

![IMG_0233.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0233.jpg?lossy=1&strip=1&webp=0)

#### Thank You Mistres

“You are welcome, slave,” Rebecca responded formally. Danny had his eyes properly cast down on the floor, so there was no way he could see the knowing smiles exchanged above his head.

“Slave,” I said, “We would like some afternoon tea. Please prepare a tray and bring it here.”

“Yes, Mistress,” Danny said. He backed away from Rebecca’s feet, turned around and crawled out of the room. I was pleased to see he was huge in his cock cage.

Five minutes later the whistle of the tea kettle sounded, and a few minutes after that our girl chat was interrupted by the sight of my slave at the entrance to the living room. I had trained him to wear an apron when he served me. He looked magnificent standing there, eyes cast down, holding a silver tray with steaming teapot, cups, saucers, sliced fruit and cookies, using my best china. Good boy.

I let him stand there silently for a few moments and exchanged a look with Rebecca. I was pleased to see she approved. How could she not? Danny looked sexy as hell. My slave manservant. Of course, she had one of her own (only now it dawned on me why she called him ‘Hamilton’), but this one had to be cuter, after all he was thirty years younger than Hamilton (and now it became clear why their age gap worked so nicely). “Slave, you may enter. Put the tray on the coffee table and serve us.”

![IMG_0234.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0234.jpg?lossy=1&strip=1&webp=0)

#### Tea Time

Danny moved to the table and set the tray down. He filled two cups. “I have a selection of teas, Mistress Rebecca, which would you prefer, the Earl Grey or something milder?”

Well bless his soul, I thought, Danny does have the heart of a servant, and a perfect gentleman, too. I had never trained him to serve more than just me, but he was handling it nicely, thinking of more than one choice of tea and offering it to our guest first. And the sliced pear, that was inspired. I beamed.

“The Earl Grey is fine, slave,” Rebecca said, her tone not quite dismissive, but like she was speaking to the hired help. “I’ll skip the cookies and just have fruit.”

When Danny had finished serving us, I ordered him in position with a silent wave of my hand, four fingers. He took his place on hands and knees to my right, Rebecca on my left. We drank our tea and chatted about Thanksgiving plans. She and Hamilton were driving to Richmond to visit his family on Sunday. The traffic on I-95 would be awful. We went on this way for a few minutes until we had almost finished our tea, when Rebecca put her hand on mine, a signal we had agreed upon in advance.

“So Mistress Rebecca, would you like to play fetch with my pet?”

“Oh how sweet, of course.”

“Come here, pet, let me remove your apron,” I told Danny. “Then get in fetch position.”

Danny crawled to my feet and I pulled the strings and removed his apron. He crawled to his spot next to the couch, facing the dining room. I collected the hollow rubber ball with a jingle bell inside that I used for this purpose. It was a humiliation that I used to reinforce Danny’s status as my pet; I wondered if he would balk at fetching for someone else. He’d better not.

She turned to me inquisitively. “Does your slave have a name when you play fetch?”

“No, just ‘pet,’” I replied. It hadn’t occurred to me to give him a dog name. I handed her the ball.

“I used to call my Hamilton ‘Ajax’ when we played this game,” she remarked. “It’s become hard on his knees so we don’t play much anymore.” She reared her arm back and tossed the ball into the dining room, quite the throw. “Fetch, boy!”

#### Fetch boy!

Danny scurried off on his hands and knees into the dining room, crawled under the table, picked up the ball with his mouth, then scurried back on all fours to Rebecca’s feet. “Good boy,” she cooed, ruffling his hair, which had already grown an inch since the Halloween buzz-cut. “Again?”

Danny panted eagerly and wiggled his tush at the prospect, just as I had trained him. She threw the ball again. He retrieved it again. We repeated the game a few more times and then Rebecca handed me the ball. “Thank you, that was fun putting him through his paces. He’s really agile on all fours.”

![IMG_0136.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0136.jpg?lossy=1&strip=1&webp=0)

Now we had reached the moment for which I had some misgivings, but which Rebecca reassured me would be a turning point in Danny’s training and my progression as his Mistress. I put the ball and my doubts aside and said to her, “Would you like to take him downstairs and play a little?”

“I would love that, dear. He seems such a fine young specimen, you chose well.”

“Slave, heel,” I commanded to Danny, who was breathing hard from our game. He took his positon on my right and I snapped on his leash. I walked him to the basement stairs, Rebecca following. At the top of the stairs Danny turned around so he could safely crawl down the stairs backwards. It was at that moment that he got his first glimpse of Rebecca: legs a mile long on top of those high heels, wearing a slim, knee-high wool skirt, that exquisite silk blouse; she exuded a statuesque, commanding presence.

I knew he already knew it was her, but the shock of recognition on his face told me that suddenly it had become real, he was going downstairs and she was going with us, what had been our private game was enlarging into something much greater. He was not just my slave, he was a slave, subject to my will. He would do as I pleased, when I pleased, with whom I pleased.

#### A Slave

As we went down the steps that afternoon, I must admit I had some doubts, but they were balanced by an intense curiosity to see Rebecca, the ultimate Mistress, in action. And to be completely honest, the situation was triggering my bi-sexual longings, they were bubbling up once again. I found myself fantasizing her kissing me in front of Danny, the two of us teasing him . . . that and much more.

There was plenty of light spilling through the glass bricks so I left the light off and walked Danny to the dungeon door. At the doorway, I flicked the switch that turned on two amber-coated LED lights that gave the room a warm, soft glow. I led Danny to the underneath the pull-up bar, which had become my favorite spot to hang him in a stress position. “Stand, slave.”

Danny rose to his feet, wincing. His hard-on had to be excruciating in that cage. “Up on your toes, arms raised,” I commanded. Danny lifted his hands into position next to the leather cuffs that dangled from screw-eyes inserted at either end of the wooden dowel. I retrieved the footstool and stood on it, expertly wrapping the cuffs around his wrists. I retrieved the spreader bar and attached it to his ankles. Now he was spread-eagled, up on his toes, completely immobilized, swaying as he struggled to keep his balance. Next came the ball gag, hanging from a hook. I inserted the red ball in his mouth and wrapped the harness around the back of his head, buckling it in place. Poor slave, so vulnerable now.

Rebecca stood at the door, calmly observing my preparations. I knew Danny could see her out of the corner of his eye. I debated blindfolding him, but decided against it. Rebecca was a stunning woman and I wanted him to see her. I liked it when Danny’s roving eyes triggered his lust, and lust collided with the harsh reality of his cock cage. As long as I was the only woman he ever fucked again in his life, I was fine with him looking. Besides, I sympathized; I couldn’t keep my eyes off Rebecca, either.

![IMG_0236.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0236.jpg?lossy=1&strip=1&webp=0)

I went to the wall and selected the riding crop. I walked in front of Danny, slapping the tip of the crop against the palm of my hand. “We’re going to play a little game now, slave,” I said huskily. “As you can see, I’ve invited our guest to take part. You don’t mind, do you, letting our guest play with you?”

A muffled response through the gag, what sounded something like, “No, Mistress.”

“As if you had a choice . . . Rebecca, do you care to take the crop and step behind Danny?”

Rebecca stepped into the room and took the riding crop. Wielding it expertly, she traced a line down Danny’s flank as she stepped into position behind him. Meanwhile, I pulled down the shoulders of my long-sleeved black knit halter top, exposing my breasts to the nipples. Danny’s eyes widened with lust.  “Here’s how the game works, slave. You endure ten swats from Mistress Rebecca, and I’ll give you ten seconds to suck your Mistress’s nipples. Shake your head up and down if you agree.”

Danny’s head nodded vigorously. “Oh sweet thing, aren’t you eager to play,” I teased. “But before you agree, there’s more to the game.” I pulled out the Japanese clover nipple clamps from the pocket of my capris and dangled them in front of Danny’s nose. “You have to wear these while we play.”

Behind him, I saw Rebecca lazily running the tip of the crop from the nape of Danny’s neck down his spine to his ass. “Are you willing to suffer for Mistress while we play our game?”

#### Play Time

Danny nodded again, this time less vigorously. In the last few weeks, he had graduated from clothes pins and quickly established a love-hate relationship with the dreaded Japanese clover nipple clamps.

“And just to be fair, you need to know there’s a catch,” I said with an evil grin. “The clamps must come off each time before I’ll let you suck my nipples.”

I saw a flash of dread in Danny’s eyes. I had learned that his nipples numbed in a matter of minutes when squeezed by jaws of the clamps, and that he could endure them for at least thirty minutes, maybe longer. The torture came when removing them, an intense agony that lasted a minute or more. Taking them on and off was much worse than simply leaving them on. My final condition guaranteed he would suffer mightily for the privilege of sucking on his Mistress’s tits . . . and that was only after he endured whatever pain Rebecca administered as the pre-condition to his bittersweet reward.

“Tsk, tsk . . . such a cruel bargain,” I taunted, looking over Danny’s shoulder to see Rebecca’s face. She was enjoying my performance, her face shining with pleasure. She spun the tip of the riding crop in a circle, signaling she was ready to go. I pulled down the halter another inch, completely exposing my breasts. “So is the reward worth it to you, slave? Do you still want to play with us?”

Danny grudgingly nodded, steeling himself for the price he would have to pay to get his prize.

![IMG_0235.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0235.jpg?lossy=1&strip=1&webp=0)

“Very well.” I grasped his left nipple and squeezed hard, creating a fold of skin with the nipple at the center, and attached the first clamp. Danny winced. I repeated the process with the right side of his chest and he winced again. A silver chain dangled between the nipples. I tugged on it, knowing that the burn was intense, having tried the evil little devices on myself. A bit of drool leaked out one side of Danny’s mouth. I wiped it with a finger. “There, there, let’s not get all messy, slave. Are you ready?”

“Yes, Mistress,” Danny tried to say through his gag, but what came out was three muffled grunts.

“He’s all yours, Rebecca.”

#### Rebecca’s turn

The first blow was the hardest; it came with a stinging impact that made Danny howl against the gag and jerk violently against his restraints. I flinched and put my arm out to steady him. Rebecca’s next swat was not quite as hard, but Danny jerked again. I had briefed Rebecca that Danny’s limit was a seven or eight, but it hadn’t occurred to me that her seven was my eleven. Eight more blows followed, all to his rear from what I could tell from her stance. When Rebecca had finished, Danny’s chest was heaving.

“Poor thing,” I cooed, reaching behind Danny’s neck to unbuckle the gag. “I think that might’ve hurt a teeny-weeny bit, didn’t it?” The ball gag came out and Danny gasped and inhaled a huge lungful of air. “Now slave, I want you to rate those blows. Tell me how hard Mistress Rebecca hit you.”

Danny hesitated, reading an internal pain gauge known only to him. “Uh . . . an eight, Mistress.”

“I see. Well, you’ve earned your reward, slave. Are you ready?” I tugged down on his nipple chain.

“Yes, Mistress,” Danny said, wincing, girding himself for the flood of pain to follow. I unclipped the clamps from his nipples and he writhed in his restraints. I cupped under my breasts with my hands and lifted them up toward his mouth. He had to almost hyperextend his shoulders and bend his head down as far as he could to reach my nipples. His lips greedily found them and he sucked hard, first one and then the other, trying to erase the pain flooding his chest with the pleasure of my breasts. I counted to ten and pulled away. He groaned. “Poor thing, ten seconds goes by so quickly, doesn’t it?”

“Yes, Mistress,” Danny half-sighed, half-groaned.

#### “Are you ready to play the game again?”

Danny had that wild look in his eyes that told me he was going into subspace. “Yes, Mistress.”

I reached to his chest and squeezed the flesh around the nipples, attaching the tit clamps as I had before. This time, with the ball gag removed, Danny’s lips pursed and he grimaced with pain. With the clamps reinstalled, I put my hand against his chest and nodded to Rebecca. This time the blows came to Danny’s back. I could feel them through his chest, each one feeling like a ten. Danny gritted his teeth and grunted with each snap of the riding crop, taking it like a man. I was proud of him. When Rebecca was done, I removed the tit clamps and again Danny writhed in agony. This time I lifted up on my toes, bringing my nipples to his greedy lips. I might have let him suck for a little longer than ten seconds.

We played our game eight more times, until I could tell Danny was getting exhausted from being up on his toes, his nerves frayed from the whipping and the repeated administrations of the nipple clamps. I made eye contact with Rebecca and she nodded that she was finished. I lifted my halter back in place and was about to reach up to free Danny from his restraints when Rebecca said, “Vanessa, dear, do you think I could have a moment alone with your slave?”

The request caught me off guard. “Um . . . yes, of course, Mistress Rebecca.”

“And can you put the ball gag back in? I want your slave to listen, not speak.”

“Yes, of course.” Feeling a little discombobulated, I obediently picked up the gag from the floor and re-inserted it in Danny’s mouth. As I fastened the locking straps behind his neck, I saw a look of alarm on his face, “Are you going to leave me with this strange woman?”

I gave Danny the stern look in response, silently saying, “Yes I am, don’t be alarmed, I love you.”

Rebecca stepped around Danny to stand next to me. She took my hand in a sisterly fashion. Reading my mind, she said reassuringly, “Don’t worry, I just want to have a little chat with your young slave here. I promise I won’t hurt him a bit—” she caressed Danny’s cheek with a chuckle, “—I’ve already done all the damage I care to do today.” She removed her hand and placed it on mine, drawing me closer, kissing me on the lips. I felt sparks fly. As our lips separated, she said, “Your slave needs to hear from someone else just how lucky he is to have found a Mistress like you, darling.”

How could I argue with that? I gave Danny a parting silent glance, “Be good!” and walked out the door of the dungeon, leaving him in Rebecca’s capable hands.

### Part 17, The End of Innocence

Of all the toys on the list of kink, the strap-on cock was the one I most feared, because it veered into dark terrain of human anatomy which had been marked “off limits” for me since childhood. To put it bluntly, you pooped out that hole, so it was disgusting and filthy. Like a city sewer, you just stayed out. But I had seen videos and read Bend Over Boyfriend, so I knew at least in theory how it worked, and that plenty of men and women (and women and women) enjoyed strap-on sex/play. But how could I put this profoundly academic knowledge into practice? I was blocked, unable to take the first step.

![IMG_0237-1.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0237-1.jpg?lossy=1&strip=1&webp=0)

Into the void stepped Rebecca, my new subject-matter expert. She graciously received me at her house one evening a few weeks after Thanksgiving (observed, by the way, as completely vanilla couples, not a whiff of kink, at her insistence). This evening Hamilton met me at the door dressed in black slacks, a white shirt and collared. He invited me in, respectfully addressing me as Mistress Vanessa and kept his eyes cast on the floor in my presence. Rebecca explained that Thanksgiving had been a holiday, a break from Hamilton’s normal status as slave. I must say that it was a little disconcerting to see him so totally immersed in his submissive role after previously experiencing him as a sophisticated retired professor, but now I that I was a member of the Mistress Sisterhood, Rebecca wouldn’t have it any other way. Hovering discreetly in the background, Hamilton was clearly immersed in his role.

So with glasses of Merlot in hand and our legs tucked underneath us on a cushy couch in Rebecca’s sitting room, I broached the taboo subject. What could she tell me about strap-on play? Over the next hour I got an education, and not just academic, but practical. Rebecca ordered Hamilton to bring in her strap-on and the related paraphernalia, to wit, two strap-ons, each with different kinds of harnesses, an enema bag and an enema bulb, various lubricants and a selection of dildos and butt plugs. I was the only one in the room embarrassed.

#### Strap -on play time

“Vanessa, you need to get over your hang-up about the anus,” Rebecca began the lesson somewhat indelicately. “Spit comes out of your mouth, snot from your nose, wax from your ears, urine from your urethra, ejaculate from your vagina and poop from your asshole. It’s that simple. No orifice is inherently profane. As the good book says, ‘It’s the thought life that pollutes.’ So just as you wipe your nose or use a Q-tip to clean out ear wax or a toothbrush to clean your teeth, you clean your slave’s asshole with an enema. Once you’ve trained him, he can do it himself.”

![IMG_0238-1.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0238-1.jpg?lossy=1&strip=1&webp=0)

She went on to explain in candid detail how to use the enema bag, warning me that it would fill my slave’s lower intestines and he’d have to make a conscientious effort to completely void his bowels after taking in the contents of the bag, or else he’d leak water and bits of scat. (Oh my, we didn’t want that.) Alternately, she continued, prior to inserting a butt plug or if necessary for “a quickie,” the colon could be cleaned out in minutes using the enema bulb, which looked a bit like a turkey baster.

She put a hand on my arm and confided, “The way we do it is I tell Hamilton that ‘I want him clean, inside and out’ . . . that’s my code phrase to let him know I intend to play with his ass. I try to give him an evening’s advanced notice, at least a few hours. He is quite fastidious about his preparation. I think he would be horrified if he wasn’t squeaky clean when I penetrate him.”

I gulped down my wine. Well there it was, that the hardest part, wasn’t it? That wasn’t so awful.

Rebecca signaled Hamilton, who hovered in the hallway by the door, out of earshot. He trotted in with the wine, filling our glasses. “Thank you, Hamilton,” Rebecca said. “You are dismissed. Please be back here in an hour to draw my bath.”

“Yes Mistress.” Hamilton gave a polite bow and left the room.

Rebecca shot me a proprietary glance. “In time you’ll teach your slave to attend to you while you bathe and dress, do the laundry and ironing, take out your dry cleaning, clean the house, cook and serve meals, run errands, chauffeur and porter when you shop. After a while, you can’t imagine doing without a manservant . . . now, where were we?”

#### Squeaky Clean

“You expect your slave to be squeaky clean,” I replied absentmindedly, my mind stuck on thoughts of Danny as my manservant. We had only just started; it was barely a month since we had signed the agreement, and up until now our roles and play were always sexually charged and extreme, but I could see things evolving over time into a more refined form of submissive service with rituals that pervaded everyday life and infused it with meaning. A different way of loving. That’s where Rebecca and Hamilton seemed to have gone with this. Of course, they didn’t have children.

“Yes, squeaky clean. Want to try one on?” Rebecca said, holding out a very lifelike, flesh colored, seven-inch penis with balls mounted on a leather pad, the harness consisting of three separate loops attached to the central pad, presumably one for the waist and each thigh.

“Oh no, I’m fine, thank you,” I stammered. “I’ll just drink my wine.”

“Nonsense. Take off your skirt and try it on.” Rebecca leaned forward and thrust the strap-on into my lap. “You came here to learn; you might as well get some practical experience.”

How could I refuse? Blushing, I put the wine down and unzipped my skirt, stepping out and folding it neatly on the couch. I felt quite awkward, but also intensely aroused, standing in my pantyhose and bra in front of Rebecca. “Let me help you,” she said. She held out the leg straps. I stepped through them while she positioned the cock below my waist, above the crotch. “Now tighten the waist belt,” Rebecca instructed. I pulled the belt through the buckle and cinched it down on my waist. “Now the leg straps, snug them to your thighs.” I pulled the free ends through, cinched the straps and fastened the buckles. Rebecca stepped back, arms crossed, admiring. “There, you look sexy as hell, girl. Look in the mirror.”

![IMG_0239-1.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0239-1.jpg?lossy=1&strip=1&webp=0)

#### Get Ready!

I stepped in front of her dressing mirror and there she was, a petite redhead with a slender build and a big cock sticking out of her groin, who looked just like me. Frankly, I thought she looked rather silly. Rebecca stepped behind me, wrapping her arms around my waist and taking the cock in her hands. “I know you feel like an imposter, Vanessa, but that will pass. This dick—” she gave it a friendly shake, smiling at me in the mirror, “—it will change your life, and your slave’s, too. It’s the great equalizer. Once you learn to use it, you’ll wonder why you waited so long.”

I looked back at her in the mirror with a dubious expression. “This thing is huge, it’s almost as big as Danny’s cock, and it feels hard. I’m afraid I’d hurt him with this.”

Rebecca nodded. “You can hurt him if you don’t start gently. These are made of silicon so if it’s cold, you can soften it up under warm water. Before you start, use your finger first with plenty of lubricant to loosen him up. You can’t just shove it in him, any more than he can just shove his dick into you.”

I looked down at the dildo poking out from my body. So, this is what it was like to be a guy. It would be so easy to go pee. Poor Danny, now he had to sit down on a toilet seat like a girl.

Rebecca continued, bringing my mind back to the subject at hand, “When you first go in, it’s best if you press against him, but order him to push back and get the head of the penis through the sphincter on his own. Once you’re inside, go slow and go easy. Just being inside of him is going to rock his world, trust me. You can thrust more vigorously once you’ve warmed up his ass. It only takes a few minutes.”

All the while she was speaking, her hands were gently stroking the cock, pressing it against my crotch. My clit tingled. “There, do you feel that?” Rebecca said with a mischievous grin. I saw myself nodding in the mirror, blushing. “Feels nice, doesn’t it?” I nodded again, my knees going weak.

Rebecca released the cock. Her hands glided up my sides to my breasts, cupping them briefly, then they settled on my shoulders. She turned me around so that we faced each other with the cock between her legs, tucked under her crotch. “Vanessa,” she said in a low, sultry voice, “I’d hate for you go back to your slave without any real experience. Why don’t we try this out together?”

#### Double Trouble

I felt my mouth go dry. Rebecca didn’t give me a second to think up a reason to say no. She grasped my blouse and lifted it up and over my head. I didn’t resist as she tugged the sleeves off my arms. I was left naked except for my pantyhose, the strap-on and a lacy white bra. “You have the nicest boobs,” she said, looking down at my chest admiringly. “They are so perky. And my, what a hard-on!”

I looked down. Indeed, I did. Rebecca took my hand and led me to the bed. I sat on the edge and watched as she languorously stripped off her clothes. She had a magnificent body: gym-toned limbs, flat stomach, firm, full boobs that had to be enhanced. She pushed me back on the bed and straddled me. “First we need to lube this up,” she said, taking the penis in her mouth. I watched, transfixed as she deep-throated it, then slowly withdrew it, pausing, the head suckled in her lips, her eyes locked on mine. It was incredibly erotic. I could see why Danny loved to watch me suck his cock. Oh, my god.

She resumed sucking, sliding the penis in and out of her mouth, slathering it with her saliva. Then she climbed up and positioned herself above it, slipped it inside her vagina and gently settled down on me, a look of rapture on her face. I felt her gravity inside me. “Nice, isn’t it?” she murmured.

I looked up at Rebecca, her exquisite body impaled upon my cock, her weight pressing down on my clit, those beautiful breasts hanging like ripe peaches above my lips, begging to be devoured. The last shred of inhibition fled me, replaced by all-consuming lust. I reached up, took her sculpted face in my hands and pulled it down to mine. “Kiss me,” I said. “Kiss me, fuck me, teach me.”

And she did.

### Part 18, Taking Her Slave’s Virginity

A week later, I came home to find a discretely labeled box on my doorstep. Inside was my strap-on cock and two dildos (one six inches, the other eight inches) and a butt plug. I took out the strap-on and tried it on. It was flesh colored and lifelike, like Rebecca’s, only slightly smaller. It was my size, petite. Once I had the leather waist belt and legs straps adjusted, I trimmed the excess with scissors. I wanted my strap-on to be an extension of my body, the straps neat and tidy. When I had everything just the way I wanted, I put it in a drawer and waited for Danny to arrive home from work.

![IMG_0240.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0240.jpg?lossy=1&strip=1&webp=0)

He came home promptly at seven, stripped, put on his collar and crawled to the living room where I waited in my favorite dominatrix outfit: a sexy leather bra, garter belt and panties, fishnet hose snapped to the garter, 4” black patent leather high heels. Danny crawled up and I gave him permission to kiss my feet. As always, he had a rigid hard-on, but this time it wasn’t confined in his cock cage; it was Monday and I had established a routine of putting him in his cock cage on Wednesday through Saturday evening, when I usually let him have sex with me if he had been good during the week.

Monday and Tuesday, when he got home from work I would put Danny in the cage upstairs for an hour, a slave meditation that had to be good for the soul. More recently I had taken to setting a laptop in front of the cage and playing a looped slideshow of him in various submissive poses and sissy clothing (the nurse outfit was still my favorite), strung up or strapped down in the dungeon, plus a collection of sexy selfies of me, accompanied by an audio track of music laced with affirmations of his slave status.

#### Tranining

The subliminal training was Rebecca’s idea; she had used it with Hamilton early in their relationship. “I am Mistress’s slave, I was born to serve her,” “I am a submissive slut, I belong at my Mistress’s feet,” “I am lucky to be the slave of Mistress Vanessa,” “My body, mind and soul belong to Mistress Vanessa,” and so on. Sometimes I’d sit on the top step and listen to Danny repeat the affirmations. Woe to him if he didn’t, but at the same time, I knew I didn’t need to mind control him; his submission to me sprang from his heart. But the pictures were hot and having him repeat those affirmations had a certain kinky power, so what the hell, it gave him something to focus on while he crouched in his cage.

This Monday afternoon, however, Danny would not be going upstairs to the cage, he was going to my bedroom to lose his virginity. I snapped on his leash and walked him to the bathroom, where I had set up the enema bag, filling it with warm water and hanging it from the shower nozzle. A long hose dangled down from the bag. “Get in the tub, slave,” I ordered. “Face away from the drain.”

Danny climbed in the tub. “Head down, butt up,” I ordered. I put on a latex glove and let go of the wrist with a sexy sounding snap, then spread some Vaseline on my index finger. “Okay, slave, today you’re going to experience your first enema, and then I’m going to fuck you, understood?”

I think Danny’s heart skipped a beat, because it took several seconds before he finally stammered, “Yes, Mistress.” I knelt next to him and gently worked my finger into his ass. I felt some resistance, and then pushed past the sphincter. “Squeeze my finger, slave,” I ordered. I felt Danny squeeze down on my finger. I worked the finger in and out for a minute and then removed it. Next came the nipple at the end of the hose. It slid in easily. “Here we go, slave, just relax.”

#### Just relax Slave

![IMG_0238.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0238.jpg?lossy=1&strip=1&webp=0)

I released the clip that held the hose shut and the water flowed. Danny’s breathing became shallow and erratic. “Take deep breaths, slave, let the water in.” Danny’s breathing steadied. I watched the bag slowly deflate. When it was empty, I waited for a minute, stroking Danny’s gorgeous tush, then pulled the nipple and hose out. “Stay put, slave.” I rose to my feet and pulled off the glove.

My heels clicked on the tile as I walked out of the bathroom. In the bedroom, it only took me a few minutes to get the strap-on mounted properly. I returned to the bathroom. “Okay, slave, get up and sit on the toilet. Squeeze when you stand.”

Danny got on his feet, stepped out of the tub and took a seat on the toilet. For the first time, he lifted his eyes and saw the strap-on mounted to my crotch. His eyes grew wide. “Like it, slave?”

“Yes, Mistress.”

“I thought you would. I’ll let you suck on it in a moment. Now relax and push, let it out.”

Danny voided his bowels. I knew it was humiliating for him, having me stand there, watching him, but then he was my slave; nothing was off-limits. I leaned over him and pressed my right knee against his stomach, the way Rebecca had showed me. More fluid poured out. I took Danny’s chin in my hand and tilted his face up. “You feel that, slave? In the future, you’ll have to do this on your own. You want to be sure you get it all out before you present your ass to your Mistress, understood?”

He nodded. I could tell he was mortified. “It’s nothing to be ashamed of, slave. I expect you to be clean, inside and out. It’s for my pleasure. You want Mistress to fuck you, don’t you?”

#### “Yes, Mistress.”

“I’ll give you a few more minutes here alone. Stand up and sit down a few times; make sure you get it all out. Then crawl into the bedroom, I’ll be waiting for you.”

I took the Vaseline and left the bathroom. I had draped a towel on the foot of the bed. I sat on it, nervous as hell, waiting for Danny. I wanted to do this right. Candles set on the nightstand cast flickering light on the bedroom walls. When he finally crawled in, I ordered Danny to kneel with his hands at his sides. I took his head in my hands and drew his mouth close to my cock. “You want to suck it, don’t you, slave?” I asked him. His eyes shined yes. “Say it, slave, beg me to let you suck cock.”

He licked his lips. “Please, Mistress, please let me suck your cock.”

I pushed my hard dick into his mouth, pulling on the back of his head, thrusting my pelvis forward until he had taken the shaft all the way and his lips were at the base. Deep throat. Before he gagged, I pulled out halfway. Then again, pushing in as far as I could go. Back out. And again. We started to get the hang of it, reversing the sexual roles of our vanilla life. We soon found a rhythm, Danny squeezing hard on the shaft to control it, which I found increased the impact on my pubic area, which increased the stimulation on my clit. I looked down and saw Danny’s own shaft as rigid as a flagpole; no question this was turning him on. It was turning me on, and I realized I could cum this way; I would cum this way if we didn’t stop. But if I was going to cum, I wanted to do it with my cock up his ass.

“Stop,” I commanded, pulling my shaft free of Danny’s mouth. I tapped the side of his cheek with my hand. “Turn around, slave. Stand up and bend over the edge of the bed, lie on the towel.”

#### Good boy, Now follow my instructions

Danny followed my instructions and presented his ass to me. “Spread your legs wider.”

He spread his legs, his feet bracketing the towel. The Vaseline was on the chest of drawers. I realized I had forgotten a latex glove. Oh well, I thought, he should be squeaky clean now. I scooped a dollop of Vaseline and spread it on the shaft of my cock, collecting the excess on my index and middle fingers. Gently, I pushed them into Danny’s ass, smoothly past the sphincter and into the colon, henceforth my slave’s vagina. I worked my fingers around, found his prostate gland and massaged it. Danny moaned with pleasure. I drew my fingers out and was pleased to see they were as clean as when they had gone in, still coated with Vaseline. I wiped them on his ass. Now the head of my penis. I positioned it over Danny’s asshole and bent my knees to achieve the proper angle. I pushed in maybe an inch, until the tip was pressed firmly against his hole, then paused. “Push yourself down on my cock, slave.”

![IMG_0242.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0242.jpg?lossy=1&strip=1&webp=0)

Danny shifted the angle of his torso fractionally, then eased his ass down on the shaft. I watched the head disappear. He drew in a sharp breath. “Breathe out,” I told him. “Breathe out slowly and let it in.” Slowly the shaft disappeared inside my slave, inch by precious inch until my pubic bone pressed against the crack of his ass. I was in, all the way in. And it felt glorious. I bent over and lay the side of my head against Danny’s muscled back, felt us both breathing deeply, and waited, motionless, for a full minute.

“You like having me inside you, don’t you slave,” I murmured into the silence.

“Oh, yes, Mistress,” Danny replied in a throaty, grateful voice.

“Good. Because I like it, too.” I took his waist in my hands and began gently pumping. He moaned in response, pushing his ass back against my crotch, his lower back arched, the muscles rippling. God, what a beautiful sight. My clit began tingling from the impact of the baseplate. I let a minute go by and then I increased the intensity, pumping harder. Danny groaned. Another minute passed and my clit went from tinging to buzzing. Oh, sweet lord, this was too good to be true, this was incredible, I was fucking a man and totally in control, I could fuck him all night if I wanted. His ass was mine. I could fuck him soft and slow, I could fuck him hard and fast . . . whatever turned me on.

Hard and fast, I decided, hard and fast. I began pumping in a frenzy, my body on fire, clit electrified, bed shaking and Danny grunting, his ass rocking with every pelvic thrust. I could feel the climax building, welling in my loins, that electric current forming in the nape of my neck, traveling down my spine, hot flash, wave of heat rippling through my body . . . I heard Danny cry out, in pleasure or pain I couldn’t tell, I didn’t care, I was past that now, this was about me, only me. I pumped harder, desperate for release, until the climax finally came, lifting in a wave and crashing down at the base of my cock. The orgasm exploded out from my clit and pulsed through my body. I collapsed on Danny’s heaving back, wasted.

We lie there utterly still for several minutes, my body fused to Danny’s, the only sound in the room our synchronized breathing, which gradually calmed like the sea after a storm, leaving the gentle lapping of the tide against the shore. I felt my strength returning. I reached under Danny’s groin with a hand and felt for the shaft of his cock. It was still hard. Poor thing, lost his virginity and still horny as hell.

Welcome to my world, slave. Welcome to my world.

### Part 19, Slave Diary

#### Slave Diary

When during one of our weekly Mistress lunches, Rebecca mentioned that reading Hamilton’s diary had always given her great insight and helped her chart his slave training, I nearly fell out of my chair. Why hadn’t I thought of this sooner? Of course, a slave diary! My questionnaires and surveys had their place, but how much more valuable a diary, especially since I would have complete access to it and be privy to my slave’s innermost thoughts?

![IMG_0234.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0234.jpg?lossy=1&strip=1&webp=0)

I decided that I would use the diary to combine two elements of Danny’s training. The first was the content itself: it would be a window on the soul of Daniel Simon Barton, what was he thinking, how was he faring on this journey of ours. The second element would be to turn the diary into a vehicle to teach my slave cursive writing. I find it a travesty that this skill, which has been shown to stimulate the brain, improve thinking and raise SAT scores, has been dropped from most elementary school curriculums.

In my opinion, the age of email, text, tweets and television has turned our population into a bunch of dumb bunnies. No one writes letters anymore. Carrier pigeons are unemployed. The new generations can barely sign their names. While Danny had a knack for building and repairing all things mechanical, his education ended with high school. I felt he could use improvement in his thinking and writing skills. As a start, my dumb bunny sub would learn cursive writing and make his diary entries in his own hand. His signature, which was a sloppy, illegible mess, would become a thing of beauty.

#### Diary Day

Not to be totally hypocritical, I confess to watching television; to wit, I’m a Game of Thrones fanatic. The whole medieval thing pushes my buttons, especially palace intrigue, politics, and of course, the sex. There was no texting or tweeting in those days, it was all leather and quill pen. With that in mind, for the journal in which my sub would make his entries, I went online and purchased Barnes & Noble’s Bombay Black Leather Wrap Journal with Tie, the cover made of soft, genuine black leather; 204 hand-stitched, cream color parchment pages within. The perfect gift for my slave.

As for a method to teach cursive writing, it’s nothing more than practice, practice, practice. You can download free worksheets from numerous websites. How perfect that my slave would be humiliated as he carefully copied, “Emma had a new bicycle. It was bright pink and shiny . . .”

Diary-day arrived on a stormy, rain-swept Thursday in mid-December. I had allowed Danny to take my Civic to work. That evening, promptly at seven, I heard its wheels crunching gravel on the driveway. A few minutes later, my slave was at my feet, naked, collared and wearing his cock cage. After he had adequately worshiped my feet (which were clad in knit wool stockings, nothing sexy, an intentional message), I dropped the worksheets to the floor, along with a ballpoint pen. “Slave, while I finish my show, I want you to practice copying these sentences.”

I watched a re-run of Game of Thrones (a Season Four favorite, “Mockingbird”), periodically glancing down to see my submissive scribe working on his assignment. I found the juxtaposition between the violent drama playing on the TV and the quiet labors of my slave strangely profound. I think it was the domestication of a man into perfect obedience without using sword or fire, rather something seemingly innocuous but equally powerful. By the time the episode concluded (note to self: don’t stand too close to Moon Doors, you might get pushed to your death), Danny had completed a half-dozen sheets.

I stood up and put my foot on his hand. “Slave, your handwriting looks worse than a third grader . . . that is, if they still taught cursive writing in third grade. You have a long way to go.”

“Yes, Mistress.”

I set the leather diary book on top of the sheets of paper. “Do you know what this is, slave?”

“A diary, Mistress?” Danny ventured.

“That’s right, slave. Not just any diary, your diary. It’s for you, a gift from me.”

“Thank you, Mistress.”

“It will be your slave diary, label it as such. You are starting well after we began, so you have some catching up to do. I want you to make your entries in cursive handwriting, with a pen, understood?”

“Yes, Mistress.”

I caressed Danny’s back, fingertips running up and down his spine, thinking. Where did I want him to begin? When we first met? The first time I spanked him? The morning we signed our contract?

![IMG_0172.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0172.jpg?lossy=1&strip=1&webp=0)

#### Crawl Slave

“Slave, I want you to crawl upstairs, lock yourself in your cage, and make your first entry starting with the first gift I gave you as your Mistress. Do you remember?”

Silence from my slave. He shifted on his hands and knees. I could tell he was at a loss.

A little angry, I reached down between the cheeks of his ass and grabbed his balls. I squeezed them, not hard, but firmly. He gave out a little gasp. “Remember now, slave?”

“Uh, the first time you slapped my balls, Mistress?”

I squeezed harder, making my dumb bunny wince. “No, slave, that’s not it . . . this is why I’m having you keep a dairy. You need to pay closer attention and remember these days. Now go upstairs, lock yourself in your cage and see if you can remember. If you get it right, you’ll be given supper at my feet this evening. If you don’t, you’ll go without and spend the night in the cage. Does that seem fair?”

“If it pleases you, Mistress.”

I let go of his balls and gave them a swift kick with my stockinged foot. “Wrong answer, slave.”

Danny’s knees buckled. When he recovered, he responded, “Yes, Mistress, it seems fair.”

“Better. Now crawl upstairs and get started.”

I watched my slave crawl across the living room with the diary in hand, turn out of sight into the hallway. I listened to him go down the hall, up the steps, then heard the cage door swinging open on its squeaky hinges. I bent over and collected the sheets of paper from the floor. His cursive wasn’t all that bad; I saw definite improvement from the first page to the sixth. I wondered if my idea would work. I loved Danny and enjoyed having him as my slave, but our arrangement had already shown me that as much as I enjoyed owning his body, what I wanted equally was the submission of a mind that I could respect as my equal, the way Hamilton had submitted his intelligent, highly educated mind to Rebecca. It seemed to me that I would get bored if Danny was just a dumb bunny submissive. I needed more.

#### Slave Diary

The first time I truly felt like my Mistress’s slave was the day I put on her cock cage. She had left it under the seat of my truck (now her truck), and called me on the phone, instructing me to put it on and then come over to her place. Putting on the cage turned out more difficult than I anticipated. The moment I held it in my hand, I got a hard-on, which made it impossible to get my dick into the cage. I tried taking a cold shower, but that didn’t work, and now I was running late. In desperation, I sat down on my couch and focused on my breathing, a Zen meditation coach taught us to calm our nerves before a race. In my mind, I turned the cage into a Speedo swim suit, just something I had to put on for the race. That helped some. My penis got a little softer and I finally squeezed it into the cage and snapped the leather collar around my balls.

![PD238800metalworxcockcagex2.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/08/PD238800metalworxcockcagex2.jpg?lossy=1&strip=1&webp=0)

Driving over to  Mistress’s house, I found myself becoming super aroused. The pressure of the cage on my cock was extreme and erotic, reminding me of my new circumstances – this wasn’t just bedroom play, this was a reminder of her control outside the bedroom. And I loved it. At her house, I stripped off my clothes, put on her collar and crawled into the living room where she waited for me. That was the beginning of the most amazing 24 hours of my life – amazing because I felt the mind and will of another person completely possess my being. I know that probably sounds strange, and I don’t understand it either. I am adopted, and it wasn’t until age four that I ended up with my parents, who are good people, caring and supportive. They were always loving, it’s nothing they did. It must have been something I didn’t get, something that happened in the first four years of my life, while I was in foster care, but I have no idea what. Or maybe it’s just a void in my soul. Whatever, when I’m Mistress’s slave, the void is filled, my soul rejoices and I feel whole.

#### The 24 hours:

First Mistress let me pick out some sexy dominatrix clothing from an online store. That was hot. Then we went on a date to listen to this musician we both like at the Southern Café. What made it special was I was wearing her cage. Sitting right next to me was this sexy girl, one that in other times and other circumstances I would have hit up, but now all it did was make me want Mistress more. Having other women make me horny and hard for her is counter-intuitive, but that’s what happens, and it’s awesome.

That night she let me take off the cock cage. The sex was incredible. The next morning, I woke up feeling down, as in, “How do we keep this going?” but she had already thought about that, totally surprising me. She led me downstairs to her basement, hung me by my wrists to a pole, and gave me a flogging that left me on fire, totally hot and bothered.

When she finished, she left me there alone and helpless in the basement, which only doubled the intensity of the experience. I hung there for what seemed like hours, a Zen-like experience that I can’t quite explain but every moment only deepened my longing to be hers forever.

That morning, she had me go down on her while she sat on the toilet. She came hard and I loved it. I’ve always wondered if she wasn’t faking her orgasms, but this time her juices squirted into my mouth and there was no doubt. I *love* bringing her to a climax.

A little later, she made me eat like a dog from a bowl at her feet. It was humiliating but at the same time, arousing, because she was reinforcing my status, making it real. That day we did chores, first raking leaves, then I cleaned out  her truck, then we went to my apartment and she let me have it. I can’t say I didn’t deserve it, I’ve been living like a pig for a long time, getting away with it because no one else cared (growing up, my mom picked up after me).

Mistress wasn’t having it and we spent the entire afternoon cleaning house. She worked just as hard as me. When we finished, I thought she was done with me for the day, but she was still thinking about her slave, way ahead of me, getting inside my head before I even knew my own thoughts. I can’t explain just how arousing that is, but it is, like having someone outsmart you in chess.

She stripped down to her panties and bra, looking incredibly sexy, sipping a glass of wine. She ordered me to beat off while she watched. With the cock cage on, the harder I got, the more it squeezed down on me, which only increased the arousal, a wonderful vicious cycle. Suddenly, she made me stop. She took off her panties and stuffed them in my mouth, then unlocked the cage and took my cock in her hands, saying to fuck the hands that spank me. If that wasn’t over the top, she said I couldn’t cum until she let me beg, which of course I couldn’t do with her panties in my mouth.

Just when I thought I would explode, she pulled them out and let me beg to cum. I barely got the words out before I exploded, squirting all over her, dripping on the floor. She had me lick her clean, which I did fine, but then she ordered me to lick my cum off the floor.

The ultimate humiliation. I just couldn’t do it.

#### She insisted.

I thought about refusing, thinking that she might try to beat me into submission and force me to lick the cum up. For the first time since we started, I had reached a hard limit. She could beat me to a pulp and I still wouldn’t do it. I was on the verge of saying my safe word, “red.”

I took a deep breath. I thought about where this was going, how she once asked me if she wanted children, what then? I told her that I would abide by her judgement.

Well, my semen was on the floor, carrying the sperm that will help make our children if Mistress ever decides she wants them. She’s certainly swallowed plenty of it. And then I had an epiphany that licking my cum off the floor was not a humiliation, it was a way of honoring her, of worshiping her.

Licking up my cum suddenly became a way to show her how much I adore her, how grateful I am for her gift (the cock cage being the physical gift, the greater gift her loving attention) and that I want to marry her and have kids. Yes, she’s an amazing Mistress, but more than that, she’s shown she loves me more than anyone I’ve ever known. I don’t know my place in this crazy ball of yarn called planet earth, but I know my place is with Vanessa, preferably at her feet.

Danny had never told me he was adopted. He had never fully revealed his bright and beautiful mind. That night, Daniel Simon Barton, the man I love, sat at the table and ate his supper by my side.

### Part 20, The Dilemma

Finding myself more deeply in love with Danny brought about an unforeseen dilemma in my life. When we started as Mistress and slave, I thought I was doing him a big favor, playing an unnatural role on his behalf, an expression of my love. Then I found myself acting and thinking in new ways that made me feel authentically myself, a stronger, bolder woman, which then led to my relationship with Rebecca evolving from intimate friendship into something deeper, a new kind of love.

![IMG_0136.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0136.jpg?lossy=1&strip=1&webp=0)

Rebecca became the sister I never had, a sister Mistress, unlocking the door to a long-repressed attraction to my own sex. Rebecca was both the key and the door and the room behind the door where this attraction came to life. It was her—I couldn’t imagine wanting to sleep with any other woman.

At first, it was easy to dismiss any guilt I felt over sleeping with Rebecca because I had already begun thinking of Danny as property, my slave, without rights, not to mention I was having sex with a woman, not a man. However, now that I could see our relationship evolving into a (female led) marriage with children, and realizing that he had done me as great a favor as I had done for him, I felt pangs of guilt.

Sleeping with Rebecca began to feel like cheating on Danny. I didn’t want to give up Rebecca, but I could never give up Danny, and I didn’t want to hurt him. If I only had sex with him, it meant denying something wonderful that had come into my life, the intimacy of loving a woman; not just the sex but the treasures of Rebecca’s wisdom and unconditional love. I couldn’t give that up.

#### Complications of Love

I loved them both. How could I give up either one?

And what of Hamilton? Early on, Rebecca had assured me that our relationship did not threaten her marriage; in fact, she revealed to me that she had long ago brought cuckolding into her Mistress-slave relationship, meaning she made Hamilton aware that she was dating other men. Not only did she date and sleep with other men, she sometimes made Hamilton watch her have sex with her “bulls,” a fitting name for unattached younger men.

Frankly, I was shocked to learn this. It sent me into a frenzy of online research to try to understand something that on face value seemed immoral (overlooking of course the hypocrisy that I was having sex with Rebecca, albeit not in front of Hamilton). Rebecca was unapologetic; vanilla rules did not apply to Mistress-slave relationships. She insisted that cuckolding only deepened her relationship with Hamilton, it further cemented his status as her slave, and he willingly embraced it. Who was I to judge?

Except . . . except now I was involved with her. I found myself wondering if she entertained a stable of male and female lovers. I began to struggle with jealousy. Was she “cheating” on me? She had never promised me fidelity; it was I who had begged her to fuck me, teach me, and I did so without condition. Oh, the slippery slope I found myself on! At our next Mistress lunch (in a very private booth), I opened my heart and shared my feelings. I didn’t want secrets; Rebecca needed to know how I felt.

“Vanessa,” she said over her salad, “you have to make a choice. Either you go back to living your life as you did before you discovered your true self, or you embrace it. If you choose to embrace it, then more open lifestyles become possible. You make your own rules: you can have an exclusive relationship with your slave, or you can have a relationship with me and a separate relationship with him, or you can share him with another Mistress—” she raised an eyebrow, “—as you already have with me.”

![YouLookLikeAHornyNinjaTurtle2.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/YouLookLikeAHornyNinjaTurtle2.jpg?lossy=1&strip=1&webp=0)

I nodded. Of course, Rebecca was referring to our tea party the day before Thanksgiving. I had never asked her what she had said or done to Danny when I left them alone in the dungeon, nor had I asked Danny. Strange that I hadn’t, I know. But to ask Danny would have been a sign of weakness, revealing to my slave that I had entrusted him with another Mistress and didn’t already know what had transpired. In Rebecca’s case, it was my desire to honor her, trusting a vastly more experienced Mistress. That, or it was an unconscious fear of what I might learn. I supposed I would learn soon enough in Danny’s diary.

Since Rebecca had raised the subject, I said, “I never have asked you what exactly you said to Danny that day in the dungeon. Do you mind if I ask?”

“I told him I’d cut off his balls if he ever hurt you,” Rebecca replied with a deadpan expression.

That pushed me back in my seat. “Really?”

She flashed an evil grin, the corners of her lips turned up. “Really.”

“I see . . . May I ask another question, about you and Hamilton?”

“Of course.”

#### “Does he know about us?”

Rebecca speared a carrot with her fork and slipped it between her luscious painted lips. She chewed thoughtfully for a moment then replied, “Yes, of course he does.” She studied my reaction, which was transparent, the face of a women who fears she has harmed another woman’s marriage.

“Vanessa, you still don’t get it,” she gently chided me. “A slave—a true male slave—gives up all expectations of a normal relationship. His Mistress is no longer constrained by the normal bounds of fidelity, which is not something he necessarily even wants. For slaves, being emasculated, knowing that his mistress takes other men and women as her lovers, is a huge turn-on. It emphasizes the reality that he is a man without rights, truly nothing more than a slave. Of course, all this is with his consent.”

She took a sip of iced tea and continued, “Dear, no one forces a man to become a slave in the kinky alternate reality we inhabit; it’s a choice. If you think about it, it’s no different than a man who enlists in the military and swears an oath of allegiance to follow orders. And a man who submits to the military is at far greater risk of being thrust into morally abhorrent situations than a Mistress’s slave. In my humble opinion, being ordered to kill complete strangers and sometimes innocent civilians is far worse than a submissive slave being ordered to watch his Mistress make love to another man.”

She had a point. Still, it was a lot to absorb. I needed a drink, something stronger than water. Wine. Too bad we were lunching at Panera. I asked, “So you really think slaves enjoy their Mistresses taking other lovers?”

Rebecca didn’t hesitate. “In Hamilton’s case, yes, absolutely. I can’t speak for Danny. But judging from how he responded when we were together in the dungeon, and how he responded to me when you left us alone . . . yes, I am quite certain he would get off on it.”

I stared at Rebecca, wondering if I dared ask. Yes, I dared. Better to know than be left wondering. “How exactly did he respond, if I might ask?” I inquired as calmly as my courage allowed.

![IMG_0245.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0245.jpg?lossy=1&strip=1&webp=0)

Rebecca put her hand on mine. I admired her perfectly manicured nails. “Vanessa, after you left, I teased your slave. He’s a man, after all, and less capable of resisting my charms than you. And you know how well you’ve fared.” She smiled seductively, challenging me to disagree. I remained silent.

She leaned forward, speaking sotto voice. “When I had him fully aroused, I put my lips to his ears and told him that I enjoyed fucking you, his Mistress, and I would enjoy fucking him, too. I told him it wouldn’t be too long before he’d be sucking my dick while you fucked him in the ass. Do you think his cock went limp in its cage when I said that?”

I knew my slave; he probably came close to a spontaneous orgasm when she spoke that in his ear. Before I could object to Rebecca revealing our sexual relationship without my consent, she sat back and said, “Also, you should know that I told him I loved you . . . I loved you well before he did.”

I was stunned. Rebecca had never spoken the words ‘I love you,’ to me. But she had revealed her love for me to Danny. It took me a while to find my voice. “I didn’t . . . what did he say to that?”

Rebecca smiled. “As you will recall, he was gagged. He just listened, wide-eyed, like a good slave, nodding yes as appropriate. Even if he doesn’t believe I would really cut off his balls, he knows I’m dead serious about looking after you. I don’t think you’ll ever have to worry about him misbehaving.”

Neither of us spoke for several minutes. I couldn’t eat. Rebecca finally broke the silence. “I apologize if I betrayed your trust, Vanessa.”

#### Silence

Another minute passed. Rebecca stabbed a slice of tomato with her fork, but couldn’t seem to find the will to lift it from her plate. I wanted to say I forgave her, but I think she already knew that; besides, what exactly would I forgive? Her honesty? That was one of the things I loved about her; she spoke the truth fearlessly. My silence was more because she had given me so much to think about.

I couldn’t leave her apology unanswered. “Thank you,” I said. “And I love you, too.”

Her beautiful face grew transcendent, gratitude shining from her eyes.

Eventually she went on, “Vanessa, I can tell you this. In my experience, the fact a slave’s Mistress is desired by other men and women only serves to arouse him more, to make him appreciate her worth. The reality that she has a sex life independent of him only serves to reinforce his status as her slave, his absolute lack of rights in all things. All things. Above all else, that’s what they want. They don’t want to be an equal partner in a traditional marriage. And if they do end up in a vanilla marriage, they hate it and spend all their time longing to be the collared slave of a Mistress.” She put her hands together in a Buddhist gesture that I had never seen her use before. “You’ll just have to trust me on this.”

I did trust Rebecca—with my life. After reading Danny’s slave diary, I realized that in many ways, what I was for Danny, Rebecca was for me—the one who made me whole. Knowing that, and knowing that I could never go back to the woman I was, I had an important choice to make.

### Part 21, Vanessa’s Choice

Of all things, a Christmas letter from Mom helped me make up my mind about how to proceed. When Dad died, he left me as co-executor of his estate, the other executor being his lawyer, with instructions to divide the proceeds of his estate equally between his wife and four children.  As you might imagine, this deeply offended my mother. Without explanation, she left for Oregon.

![IMG_0247.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0247.jpg?lossy=1&strip=1&webp=0)

In Oregon, she went to live and work at a wild horse sanctuary, which turned out to be in financial trouble. Before a year had passed, she wrote that she wanted to buy the property to keep it from being sold to a neighboring cattle ranch, which would leave the horses with nowhere to go.

Dad’s lawyer washed his hands of the situation and signed a document that made me sole executor. It fell to me to decide what to do with the proceeds from the insurance policies, a substantial retirement portfolio and the sale of the house. After much soul searching, I fell back on something Dad taught me growing up, “Colonel Whetstone’s difficult decision matrix.” It goes something like this:

**Can you do both?**

A “yes” generally means you have twice as much to do, but also twice as much fun and you don’t have to give up something you love. Dad liked to say, “You can sleep when you die.”

**Choose a life less ordinary.**

Dad cited Charles Lindbergh, who wrote, “I decided that if I could fly for 10 years before I was killed in a crash it would be a worthwhile trade for an ordinary life.”

**Save the living.**

Dad said you shouldn’t pour your lifeblood or money into the past. The dead are dead, be it jobs, people, or relationships, so spend your time and treasure on something living.

I couldn’t do both; I couldn’t split the money as Dad instructed and still help Mom. She had chosen a life less ordinary, and was saving the living. I talked it over with my brothers. We sent her the proceeds of the estate, almost two million dollars. She bought the ranch. Today the horses run wild on 365 acres.

#### Christmas Letter

In her family Christmas letter, Mom expressed her joy and gratitude to me and my brothers. There was a sweet picture of her leaning on a fence, looking out on a beautiful expanse of open land, a herd of mustangs grazing in the distance.  The caption read, “Happy, happy, joy, joy! Love from Oregon.”

That decision turned out well. So, applying Dad’s decision matrix to my situation, I decided that I could do both. If it ended with a crash in ten years, it would be a worthwhile trade for an ordinary life. Studying the calendar, I concluded that (somewhat ironically) the best day to start was Christmas Eve.

Christmas Eve, 2014, fell on a Wednesday. The bank was open until five, but Danny’s auto repair shop closed early, at one p.m. Over breakfast, as he ate cereal and milk from a stainless-steel dog bowl at my feet, I gave my slave instructions to prepare for my arrival with a guest at five thirty. The house should be spotless, a vase of flowers on the dining room table, a tea service for two prepared with the kettle ready on the stove. Further, he should be squeaky clean, inside and out, wearing his collar and cock cage and locked inside the cage upstairs when I got home. Oh, and I wanted a live (with root ball) decorated Christmas tree in the living room. The ornaments, lights and skirt were in boxes in the attic; the wrapped presents in the hall closet should be arrayed under the tree.

Not being an unreasonable Mistress, I gave him ten twenty-dollar bills, 200 dollars to purchase the tree and do any last-minute shopping (I couldn’t imagine he had saved anything from his weekly allowance). He had permission to take my truck so that he could haul the tree home. I knew all that would make for a very busy afternoon of preparation.

#### Bank’s Closed

![IMG_0248.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0248.jpg?lossy=1&strip=1&webp=0)

Rebecca and I closed the bank at five and were out the door by five thirty. We stopped at our local watering hole for a glass of wine and arrived at the house at six fifteen. I was pleased to see that Danny had parked the truck on the lawn, leaving the driveway for my Honda Civic and Rebecca’s BMW coup.

We were stylishly dressed, Rebecca in a charcoal gray pants suit and high heels, me in a skirt and blazer with mid-calf boots. Rebecca carried an overnight valise. This was a sleepover; Hamilton was spending Christmas Eve with his parents, Rebecca would join him tomorrow for Christmas dinner.

Inside the kitchen door, I was pleased to see the tile floor shined, the counters spotless and my silver serving tray prepared for tea service. I led Rebecca to the bedroom where she dropped her valise on the bed, then we moved through the hallway into the living room. A Balsam fir reached nearly to the ceiling, lights twinkling and ornaments artfully hung from the boughs, wrapped presents arrayed on the skirt under the tree. As the saying goes, it was beginning to look a bit like Christmas.

The dining room table was set for two, the centerpiece my crystal flower vase filled with a bright arrangement of red carnations, green spider mums and cedar. Well done, slave. Rebecca nodded with approval. “I don’t think Hamilton could have done any better,” she said. We moved through the dining room to the foyer behind the front door, which also accessed the stairs to the basement. Rebecca said she wanted to make a few advanced preparations in the dungeon.

#### Dungeon Preparations

We went down the creaky stairs. It was dark in the basement; I flicked on the light and we walked over to the framed-in room that serves as my modest dungeon, our heels clicking on the concrete floor. Inside the dungeon, Rebecca showed me the modification she wanted to make, tying the wrist restraints to clothesline, running the lines through the eyelets of the hanging bar and then to cleats mounted on the wall, so that we could raise or lower my slave at will. “It’s difficult to fuck a man when he’s standing straight up,” Rebecca explained. “Better to have him kneeling on the floor, doggy style.”

Ah, the voice of experience. So far, I had only pegged my slave in the bedroom, bent over my bed. Rebecca roamed around the room, fingering the boat hooks and snap links arrayed on the wall, tugging on eyelets, testing their strength. She lifted the flogger from its hook and took a few swings, the blows landing on the leather-upholstered whipping table. She returned the flogger and selected the riding crop, snapping the tip on the palm of her hand. I thought of Danny, locked in his cage two floors above, patiently waiting for whatever fate would bring. Little did he know he was about to become the subject of a second Mistress. We were going to become an extended family. Rebecca interrupted my thoughts, “Have you ever tried either of these on yourself, Vanessa?” she asked.

I shook my head with a mild snort. Of course not.

![IMG_0250.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0250.jpg?lossy=1&strip=1&webp=0)

“Have you ever been spanked?”

Again, I shook my head . . . but I found it unexpectedly arousing that Rebecca would ask.

“You should, you know, just to get a feel for the sensation.”

I gazed at my friend and lover, amazed as ever by her forthright way of addressing these delicate topics. She never dallied around, no subtle hints. “Are you suggesting we do this now?” I stammered.

“No time like the present,” she replied with a grin. “Your slave will never know, he’s in his cage and can certainly wait a little longer. Consider it advanced Mistress training.”

#### Advanced Mistress Training

I felt myself blushing, and not only that, a hot welling in my crotch, evidence that this suggestion aroused me whether I admitted it or not. I recalled the moment that first Sunday morning when I told Danny to lay on my lap, his deer-in-headlights expression when he realized I was seriously about to do the very thing he so longed for, give him a spanking. Now here I was in the same situation, only it was Rebecca, a fellow Mistress, suggesting I try it out. A wave of dizziness swept over me.

“Oh come on, now, tell me you aren’t a little curious,” Rebecca teased me. “Come over here to the table, pull your skirt down, I’ll give you a few swats and we’ll be done with it.”

Sleepwalking, I took the few steps to the whipping table, unzipped my skirt and let it fall to the floor. I picked it up and handed it to Rebecca. “Panties, too,” she said, making a ‘give it here’ gesture with her hand. I dutifully pulled my panties down, awkwardly tugging them over my boots, almost tripping in the process. I handed her the panties. “You might as well take off your jacket.” Rebecca helped me out of the blazer. Now I was standing partially naked, wearing only bra, blouse and boots.

“Bend over, girlfriend,” Rebecca said with a friendly chortle. “This won’t hurt a bit.” She took my hair with a sweep of her hand and moved it to one side. “Are you ready?”

“I guess so,” I semi-squeaked.

“Don’t forget to rate these, one through ten. We’ll begin with the flogger.”

Whack. The flogger came down on my ass, broad and soft, feeling like the brush of a pine bough against your face when you run through a forest. It didn’t hurt a bit. “Uh . . . maybe a three.”

“See what I mean, not so bad. Let’s do it again.”

Whack! This time she came down harder. I flinched. I felt sudden heat rising in the cheeks of my ass, an unexpected tingling in my clit. “That was a five or six,” I said, trying to sound clinical.

“Good. Now I’ll do it that hard a dozen times, so you can feel the sensation building.”

Any thought of protest fled me. I was a willing victim. As the blows from the flogger rained down on my derriere, I felt circuits begin to fire, my cunt moistening, my clit tingling.

After the flogger, Rebecca demonstrated the riding crop. It had a sharper, more defined impact, leaving a stinging sensation, but not unpleasant. No wonder Danny loved it when I punished him.

#### Punishment

“Almost done,” Rebecca announced after she had snapped the riding crop against my ass and thighs a dozen times. “Now for a spanking.”

![IMG_0251.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0251.jpg?lossy=1&strip=1&webp=0)

“A spanking?” I croaked.

“Yes, of course, we can’t leave that out,” Rebecca insisted. “Let’s use the chair.”

I kept a chair in the corner for just this purpose. Rebecca went over to it and sat down, signaling me to join her. Spanking was so much more intimate than a flogger or riding crop; I felt hot juices welling up inside me as I walked over to her. What if I betrayed myself and left a wet spot on her expensive pants? Then I saw her spreading my panties neatly in the center of her lap. How did she know?

I bent over her, awkwardly reaching for the floor while keeping my boots planted on the ground, sort of a downward dog yoga pose. “Other way,” Rebecca instructed. “You know I’m a lefty.”

Oh my god. I did know that. I pushed off the floor and reversed the downward dog, this time facing to her right, my ass to her left. “Go ahead and put your weight on my lap, Vanessa, I won’t break.”

I settled my weight on her lap, which left the toes of my boots touching the floor and took the weight off my hands. I wrapped my fingers around the chair legs, trembling. “Ready?” Rebecca said.

“I guess so,” I choked out, finding it suddenly hard to breathe.

“Say, ‘Yes, Mistress, I’m ready’ . . . it’ll make it more realistic.”

“Yes, Mistress,” I said, repeating the words I’d heard Danny say a thousand times.

Whack! Rebecca’s hand came down on my ass. I let out a yelp. “That one was for not saying exactly what I told you to say,” Rebecca chided me, her tone suddenly firm, sounding just like I did with Danny, the Mistress voice. “You are to say, ‘Yes, Mistress, I’m ready.’”

“Yes, Mistress, I’m ready,” I parroted, wanting to get it over with.

Whack! Rebecca struck my right cheek. I flinched. “What do you say?” she demanded.

I knew the routine. “Thank you, Mistress, may I have another.”

Whack! This time the left cheek. “Thank you, Mistress, may I have another.”

“You may,” Rebecca replied. Whack! Her hand came down hard, across the crack of my ass, stinging both cheeks. I was on the verge of outrage, about to protest and get up, but just then Rebecca’s fingers reached into my wet cunt and felt around, gliding up to my clit. She began tapping it gently, just the way she knew turned me on. “My, my, Vanessa, what have we here?” she cooed. “So wet. It seems we have another one who likes to be spanked.”

“Ummm,” I half-gurgled, half-groaned.

“I’ll keep playing with you if you admit you like to be spanked . . . say it.” Her fingers probed against the base of my clit and circled the shaft. Unfortunately, there was plenty of lubricant to help her achieve maximum effect. My cunt was betraying me. “I like to be spanked,” I moaned. “Please don’t stop.”

#### Whack!

Swiftly her fingers retracted, swept across my cunt and rose clear of my ass. A second later, Whack! Her hand came crashing down on my ass. “Owww!” I cried out. This time it genuinely hurt.

“Vanessa, you should know better,” Rebecca scolded me, rubbing my cheeks with her open palm. “You’re supposed to say, ‘I like to be spanked, Mistress.’”

Goddam it, I couldn’t believe I made such a stupid mistake. I was acting like a dumb—oh, my god, I was acting like a dumb sub. Just like Danny. No wonder he couldn’t think straight; spanking addled your brain. I repeated through gritted teeth, “I like to be spanked, Mistress.”

Whack! Her hand came swiftly down. “Thank you, Mistress, may I have another,” I said.

Whack! “Thank you, Mistress, may I have another.” My ass was on fire.

Whack! “Thank you Mistress, may I have another.”

“No you may not.” I felt Rebecca’s fingers slip between the folds of my cunt, probing, reaching deep into my vagina, pressing against the spot I had never known existed but had been expertly revealed by Rebecca in our lovemaking. She began driving the tips of two fingers against the spot, a vigorous motion that seemed invasive but had proven to produce rapid, repetitive orgasms. I heard myself making primal grunts with each stroke. Heat waves began rocketing from my scalp to my toes and back again. My scalp tingled. I was only seconds from orgasm. Sensing that, Rebecca abruptly stopped. Her fingers retracted, moved to my clit, caressing it with a teasing touch. “Do we want to come, Vanessa?” she cooed.

“Oh, yes, please, don’t stop,” I begged her.

“I will, but first you have to agree to let me spank you a dozen times.”

![IMG_0252.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0252.jpg?lossy=1&strip=1&webp=0)

“Whatever, spank me. Just let me cum, please,” I pleaded.

“Good girl. Now just be silent while I spank you, and then I’ll let you cum, okay?”

“Yes!”

Whack! The hardest blow yet. “That’s, ‘Yes, Mistress!’ Vanessa. Say it!”

“Yes, Mistress!” I cried out, almost shouted.

“Better.”

She began methodically spanking me, one blow to the right check, one to the left cheek, then one across the middle; repeat. Four cycles, twelve blows. I counted them out silently, inwardly screaming in pain, because these blows truly stung, they were almost vicious. Tear welled up, leaking out the corners of my eyes. When Rebecca finished, she began tenderly stroking my burning ass, her hand gliding up to the small of my back and down to my upper thighs. She cooed, “Such a good girl, such a brave girl, you took those so well, I’m so proud of you, my dear, sweet girl.”

Oh, the power of words. How they reach into our souls and strip away the ego, how they strip away the pain, how they make us feel whole. I felt a stirring in my clit, that electric buzzing. Rebecca’s fingers slipped through the crack of my ass, into my cunt, and she probed deep into my vagina, to the place she had been before. “There, there,” she cooed, “are we ready to orgasm now?”

“Yes, please, Mistress,” I whinnied in a little girl voice.

#### What she Want’s to Hear

“That’s what I wanted to hear, dearest.” Her joined fingers found the spot in the deepest part of my vagina, all the way to the cervix, expertly thrusting. I felt the orgasm, which had fled under the barrage of her spanking, coming back stronger, more than just a wave, this time a tsunami. Heaven could wait.

Rebecca kept up her staccato thrusts, unrelenting, and whispered into my ear, “Now all I need to hear you say is that you want me to become your Mistress, Vanessa, that you’ll become my slave, that you want me to own you and Danny. Are you willing to do that, my beloved?”

“Uhh, uhh, uhh,” I heard myself grunting, my body on fire, my soul filled with unquenchable desire. All I wanted was release. Just let me cum, I needed to cum!

“Say it, Vanessa, say it or I’ll stop. Say you’ll be my slave,” Rebecca commanded.

I felt as if I were in a dream. From somewhere deep inside, a voice cried out from the depths of my soul, saying, ‘Don’t do it, you can never be her slave. If you become her slave, then Danny is lost to you, he becomes hers. You are his Mistress; you are her equal!”

“Stop!” I cried out, waking from the dream. “Please, stop!”

Rebecca withdrew her fingers. “What is it?” she said, sounding as if she already knew.

I let go of the chair legs and pushed off the floor, ending on my knees. I laid my head on her lap, my face pressed against my own wet panties. I didn’t care. Rebecca began tenderly stroking my hair. I lifted my head. “I can’t be your slave, Rebecca,” I said. “I’m Danny’s Mistress. We signed a contract. I love him. We’ve found our true selves together. I can be your lover, but not your slave. Danny belongs to me.”

Rebecca kept stroking my hair. A minute passed in silence before she spoke, “I am so proud of you, Vanessa. Now you know how it feels, the intensity of desire that sweeps a man’s will away and allows him to become slave to a Mistress. Now you know how Danny feels.”

“Now I know why he gets so stupid when I spank him,” I muttered. We both laughed.

“You are a born Mistress, Vanessa,” Rebecca said, “And you’ve proven it to yourself.”

I let my head fall back to her lap, wondering if I should be angry over what she had put me through. But then, how else would I know? I had to rise from the depths of desire, reject pleasure in favor of love. I had to find my true strength. “I guess I have,” I finally said. “I know what I am.”

Rebecca caressed my still very inflamed ass. She said, “You know, in my family, we have a tradition, we give the most important gift to each other on Christmas Eve . . . Why don’t we go upstairs and give your slave his Christmas gift?”

I turned my head to see her face. “You mean the gift of a second Mistress?”

Her eyes danced. “But of course—can you think of a better gift for a slave?”

I smiled. “No, I can’t . . . nor a better gift to a Mistress than a second slave.”

Rebecca helped me to my feet. We embraced, sealing the truth between us. We were Mistresses, we were equals. I would share Danny, but he belonged to me.

### Part 22, Christmas Spirit

Christmas,

On the way to the second floor to collect Danny, I detoured through my bedroom for a fresh pair of underwear, then to the bathroom to repair my makeup. Rebecca passed through the kitchen and fired up the burner under the tea kettle. We met in the hallway at the bottom of the stairs. I put my hand on the old crystal knob to open the door, but before I turned it, Rebecca put her hand on mine.

![thumb_DoYouWantTheBigToe2_640x360.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/05/thumb_DoYouWantTheBigToe2_640x360.jpg?lossy=1&strip=1&webp=0)

“One thing before we go up,” she said in a lowered voice.

“Yes?”

“When I get in Mistress mode, you know I tend to take charge. But I want you to know that I know Danny is your slave. If I get carried away, just give me a look and I’ll back off. Okay?”

I looked at her gratefully. “Thank you. To tell you the truth, I’m a little worried about how he’s going to react when he sees that we’re lovers. I know that you sort of told him, but—”

“But nothing,” Rebecca interrupted. “He’s your slave. As you said, he signed a contract. If he is truly your slave, he’ll learn to accept it . . . he’ll have to, because I’m not giving you up.” She took my cheeks in her hands and gave me a passionate, tongue-down-your-throat kiss.

I clung to the doorknob, regaining my composure. Rebecca’s kisses did that. “I’m not giving you up, either,” I said. “Besides, he’s not losing me, he’s gaining you.”

I opened the door and led the way up the stairs.

The key to the cage was on the carpet just inside the threshold. I picked it up and flipped the light switch. “Good evening, slave,” I said, striding into the room. Danny was crouched in the cage, his head lowered and butt elevated. I was pleased to see his cock swelled in its cage. Poor thing, he must have been stroking it for hours, not knowing when I would come up, keeping himself hard for me.

I circled the cage, tapping the key on the wire frame, letting him get an eyeful of Rebecca’s heels and slender pants legs, another woman in the doorway. “As you can see, slave, we have a guest for Christmas Eve. Say hello to Mistress Rebecca.”

Danny cleared his throat. He kept his head lowered and said, “Good evening, Mistress Rebecca.”

“Good evening, Daniel Simon,” Rebecca said formally, surprising me that she used his given names. How did she even know them? Of course, I realized, she’d seen the paperwork for the checking account. I was a little taken aback, but recalled she called her husband/slave “Hamilton.” It reminded me of when mom was mad, she’d call me “Vanessa Prudence.” Honestly, I wished I had thought of it myself.

#### Caged

I reached over the front of the cage, inserted the key in the padlock, unlocked it and swung open the door. “Crawl out and kiss Mistress Rebecca’s shoes properly, slave,” I instructed.

Danny crawled forward to her sharply pointed shoes. Rebecca let him kiss the tips, then tilted the right shoe up on the heel, exposing the sole. “The bottom is filthy, slave, lick it clean,” she ordered.

I watched Danny turn his head, licking the sole with broad strokes of his tongue. Again, why hadn’t I thought of that? I realized I’d be getting a lesson in male domination all night long.

When he had thoroughly licked the sole, Rebecca thrust her spiked heel in his mouth, making him suck it. When she was satisfied, she pulled it out and made him repeat the process with her other shoe, first licking the sole, then sucking the stiletto heel. She caught my eye, waving me over. When she was satisfied with Danny’s performance, she said, “That’ll do, slave. Now do the same with your Mistress’s boots. I’m sure they’re as filthy as mine.”

![cover-pic.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2015/10/cover-pic.jpg?lossy=1&strip=1&webp=0)

Standing beside Rebecca, I tilted my right boot, placing it under Danny’s lips. He commenced licking the sole. I gave him thirty seconds, then put the boot down and raised the left. When he had licked the shoe to my satisfaction, I tussled his hair. “Good boy, now let’s get your leash on.” I kept it hanging on the door knob, so it was an easy reach to snap it to his collar. I gave it a sharp jerk. “Let’s go, boy, the kettle is on and Mistress Rebecca and I are looking forward to our tea.”

Downstairs, Rebecca and I took our places on the couch, admiring the Christmas tree and necking while we waited for Danny to bring in the tea service. When he walked into the room, his erect cock looked like a tent pole under his apron. Rebecca had her tongue down my throat and her hand up my skirt; my panties were already wet again. Our escapade in the dungeon had left me incredibly horny. Eyeing Danny out the corner of her eye, Rebecca snapped her fingers, motioning him over.

While I was embarrassed for Danny to walk in on us engaged in heavy petting, Rebecca was totally at ease. “Put the tray on the table and then get on your hands and knees, slave,” she ordered.

Danny put the tray on the coffee table and dropped down on all fours. “Position Five,” I instructed, wanting him to know I was still in charge. Rebecca poured our cups. We steeped the tea bags, nibbling on tree-shaped cookies that Danny had arrayed on the plate, a festive touch. I tossed a cookie under his chest. “You may eat it, pet,” I said. He picked it up with his lips off the floor and somehow managed to get it in his mouth without using his hands. Good doggy.

We sipped our tea, exchanging knowing, lustful glances. There should be a word for that, maybe there is in some other language, a succinct word for, “exquisite anticipation of wanton sex.”

#### Cme Here , Slave!

Rebecca started it off. “Slave, come here and remove my shoes. I want a foot rub,” she ordered. Danny crawled over to her. He undid the straps and buckles, set the shoes aside and began massaging her feet. Rebecca has beautiful feet, long and slender, with beautifully manicured nails, which I noted were painted a festive green. Rebecca leaned her head back in bliss. Danny massaged her feet just the way I had taught him, gently manipulating each toe, working his thumbs into the spaces between toes, pressing his knuckled fist against the arches. I felt a little jealous that he was devoting his attention to someone else, but it was Christmas; this was my gift to Rebecca.

When Danny had given each foot several minutes of attention, Rebecca opened her eyes and leaned forward, reaching over his head to grasp his collar. She jerked up on it so that he looked her in the eyes. “That was nice, slave. Now do the same thing with your tongue.”

![thumb_EmpressHasTheBestFeetPart22_640x360.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/04/thumb_EmpressHasTheBestFeetPart22_640x360.jpg?lossy=1&strip=1&webp=0)

“Yes, Mistress.” I noted the expression on Danny’s face, the glaze in his eyes; he was already deep into subspace. I felt a pang of disappointment. Silly, but when it had been just the two of us, it was easy to think that his journeys into subspace were entirely due to my effect on him. Obviously not.

Rebecca let go of his collar and leaned back again, enjoying the sensation of Danny lovingly sucking each of her toes, sucking on her big toe, deep throating it like it was her dick, then making firm strokes of his tongue against the bottoms of her feet, then thrusting the tip against her arches. I recalled Danny giving me this same massage once in his apartment and took note; I would demand more tongue-foot massages in the future. When Danny had given each of Rebecca’s feet several minutes of attention, she languorously spread her arms, reaching out to touch my shoulder, her eyes asking, “You, next?”

I shook my head. This was her night. I smiled and gestured with my hands, spreading my fingers wide like they were fireworks going off, signaling ‘let the games begin, the night is yours.’

She mouthed a silent “thank you” and joyfully looped her eyes. That look alone was why I loved her. “Okay, slave, that’s enough,” she announced. “Now, help me undress, I’ve been in this suit all day and it’s getting warm in here. Time to get more comfortable. Move back and stand up.”

#### Assuming the position

Danny crawled backwards a few feet and rose to his feet, assuming position two, his feet spread to shoulder width and hands laced behind his head, as I had trained him to do when he wore his cock cage. His erection was magnificent, lifting the weight of the cage almost horizontal, the head of his dick poking out from under the apron like a camel nosing under a tent. Rebecca leaned off the couch and stood up, took two steps forward until her abdomen was pressed against the apron. She reached behind Danny’s waist and undid the apron strings, letting it fall. Now Danny’s cage cock was pressed against her crotch like a small rocket. “Undress me, slave,” she commanded, like a queen with her handmaiden; obviously, this was something she did all the time with Hamilton. I watched, curious, wondering how Danny would respond. I had never let him undress me. So much to learn.

Danny started with the jacket, which was already unbuttoned. For a moment, he seemed perplexed about how to proceed. Then, keeping his eyes cast down, he circled behind Rebecca, stepping into the narrow space between her and the couch. Standing behind her, he slipped the jacket off her shoulders and pulled it free. She stood there, unmoving, patiently waiting, arms fanned out slightly from her sides. I watched, fascinated; I had no idea the simple act of undressing a woman could be so extremely erotic. Danny neatly folded the jacket and laid it on the couch. He returned to face Rebecca. She wore a silk blouse with buttons up the front. He fumbled clumsily, finding it difficult to push the fat buttons through skinny slots. Eventually he got them all. Her blouse opened, exposing her flat midriff and the taut belly button of a teenage fashion model. Danny grasped the lapels and pulled the blouse apart, exposing Rebecca’s magnificent breasts, uplifted in a sexy, dusk-lavender underwire bra with Chantilly lace trim, the perfect balance of cleavage and exposed flesh. I knew my slave’s eyes were popping out of his head.

“Are you just going to stare or are you going to remove my blouse?” Rebecca rebuked him.

“Sorry, Mistress.” Danny hastily lifted the blouse, forcing Rebecca to raise her arms as he pulled it free. He lay it on her jacket and stood back, awkwardly, unsure of how to proceed. “Get on your knees and undo my pants,” she said patiently, as if speaking to a child.

![thumb_ServingMissKatrinaPart21_640x360.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2015/12/thumb_ServingMissKatrinaPart21_640x360.jpg?lossy=1&strip=1&webp=0)

Danny dropped to his knees. He fumbled with the integral belt and buckle, finally got it undone, then did the buttons. The pants were tight on Rebecca’s hips, making it difficult to pull the waist down. Danny managed to get a grip on the seams and tugged the pants down, exposing Rebecca’s beautifully muscled, slender legs. Placing a hand on Danny’s head, she stepped out the pants. He folded them and added them to the small pile of clothing on the couch. “Now the panties, slave,” Rebecca instructed.

Hands trembling, Danny inserted his fingers into her panties and slipped them down, exposing Rebecca’s immaculately trimmed bush, a sight I knew well. The panties came down to her ankles and she stepped free. She took them from him and dangled them in front of his nose. “I’m giving these to you as a Christmas gift, slave. Open wide.” She pushed the panties into his mouth, filling it.

“Now this—” Rebecca pointed to her crotch, at his eye level, “—is your temple, slave, your place of worship. You will keep your head below it always, unless otherwise instructed. Is that clear?”

“Mff, mff-mff.” Danny acknowledged, bowing down.

“Stand up,” Rebecca ordered. “Put your hands behind your head.”

Danny rose to his feet and assumed the standing position. Rebecca took his rigid cock in her hand and began rubbing the head of his engorged penis against her clit. “Feels nice, doesn’t it?”

“Mff, mff-mff.” Danny gave a muffled reply, his eyes bulging, cheeks puffed out from the panties.

#### Eye’s on the Floor

“Eyes on the floor,” Rebecca commanded. Her left hand gripped the cage, maintaining an oscillating motion of Danny’s glans against her clit. “And don’t you dare even think about cumming. If you feel you are getting close, take a step back, is that understood?”

Danny nodded his head. “Mff, mff-mff.”

“Now I want you to listen, slave, listen carefully. Mistress Vanessa has generously decided to share you with me. That doesn’t mean you’re my slave, but it does mean that from time to time she will loan you out to me, for service or my pleasure or to your further training. Got it?”

Danny nodded his head. Rebecca increased the intensity of the oscillations, her hips and derriere gyrating in rhythm. “Good. Now the most important thing you need to know about our arrangement is that you may never—and I mean never—allow yourself to cum unless you are in the presence of your Mistress, and only with her permission. That means no matter what I do to you when I have possession of you, may not orgasm. You are to pull away or use your safe word. Is that clear?”

Danny nodded his head obediently. “Mff, mff-mff.” I watched his knees. From experience, I knew that the moment they started to buckle, he was on the cusp of an orgasm.

Rebecca pressed her groin against the head of Danny’s cock, putting her weight into it, grinding the glans against her clit. I could see she was getting aroused. Her tops of her breasts threatened to escape her brassier. I knew Danny’s downcast eyes were fixated on them. Then I saw it: Danny’s knees buckled. He stumbled backward, jerking Rebecca’s arm out. She maintained a tight grip on the cock cage, keeping him from falling backwards. He regained his balance, steadied on his feet, lungs heaving.

“I think your slave is ready, Mistress Vanessa,” Rebecca announced. “Why don’t I lead him down to the dungeon while you put on your new strap-on?”

### Part 23, Double Teamed

Not to be poetic, but it was the night before Christmas, and all through the house, not a creature was stirring. We were all downstairs, in the dungeon. I had come down to find Danny strung up from the hanging bar, with clothes lines going through the eyelets to his wrist restraints, the lines drawn tight and tied off to cleats on the wall. He was up on his toes, spreader bar between his ankles, his cock standing straight out in the cage. He still had Rebecca’s panties in his mouth, only she had taped them over with silver duct tape. She was standing behind him, laying into him with the flogger.

![IMG_0256.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0256.jpg?lossy=1&strip=1&webp=0)

I stood at the door for a moment, watching silently. I had put on my new strap-on, a gift from Rebecca, bigger than my first model and with a special feature, a vertically oriented vibrator in a velvet pouch that ran across my clit. I wore black leather, my sexy dominatrix outfit and black patent leather shoes with the highest heels I possessed, four inch spikes. My hair was down, red waves falling across my shoulders. I looked hot: the mirror in the bathroom said so. Black and red, the new Christmas colors.

Rebecca spotted me at the door. “Ah, you look beautiful. And just in time, he’s all warmed up.”

I stepped in front of Danny, letting him get a good look at my breasts, which were pushed up by the skimpy bra, my nipples tauntingly visible under the lace trim. I cupped my hands under them and lifted. “Slave, have you been a good boy? Do you think you deserve to suck on your Mistress’s breasts?”

#### Do you deserve it?

Danny’s eyes were locked on my chest, wide with lust, his nostrils flaring with every strained breath. He nodded emphatically. I reached up to his mouth and peeled a corner of the duct tape, ripping it free. I reached into his mouth and pulled Rebecca’s panties out. Air rushed into his lungs. I flung the panties to a corner and let him catch his breath. “I’ll think about letting you suck my tits after we see how well you suck cock, slave.”

![IMG_0127.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0127.jpg?lossy=1&strip=1&webp=0)

I signaled Rebecca. She adjusted the lines, lowering Danny to his knees. I stepped forward, took his head in both hands, and thrust my cock deep into his mouth. While he was preoccupied, Rebecca slipped out of the room, headed upstairs to don her own strap-on.

I deep-throated Danny with my cock, slow thrusts, letting him suck the dildo in until it touched the back of his throat, then pulling away before he gagged, finding a pleasing rhythm, one that banged the base of the dildo against my clit. I was just starting to get aroused when Rebecca returned. Her strap-on was different than the one she used with me, it was big and black. She wore latex gloves and brandished a tube of lubricant. She positioned herself behind Danny’s ass. “Shall I begin, Mistress Vanessa?”

“Mistress Rebecca is going to lube your ass up now, slave. You just keep sucking,” I said, moving my hips to drive my cock into Danny’s mouth, now slamming the base against his lips. He was struggling to keep up, especially since he couldn’t hold the shaft with his hands still suspended by the lines. I saw him flinch as Rebecca pushed her way in, then shudder with pleasure as she expertly worked her fingers to lubricate his ass. I eased off and pulled my cock out until only half the shaft was in his mouth, letting him absorb what was happening in his behind. Guttural moans escaped from his throat.

“He’s ready,” Rebecca said. She pulled her fingers out of his ass and stripped the gloves from her hands. While she released the lines from the cleats and went to the corner to get the spanking chair, I circled around Danny and positioned myself behind him. “Down on your hands and knees, slave.”

Danny got down on all fours, the lines routed through the hanging bar now slack, but still attached to his wrist restraints. Rebecca set the chair in front of him. She sat on the lip of the seat with her legs spread. I admired her from across the muscled plateau of Danny’s back. She still wore her brassier, which looked purple in the amber glow, her cleavage an inviting shadow between two perfect mounds of sculpted breast, below that, her flat stomach and taut belly button, and below that, protruding from her crotch, a stunning black cock, looking like a dangerous cobra about to strike.

I took my eyes off Rebecca and focused on Danny’s ass, his cheeks spread open by the spreader bar attached to his ankles. I positioned the tip of my cock against his well-lubed, shining hole. Rebecca had left the tube of lubricant on the concrete; I reached down and squeezed lube onto my shaft, spreading it with my hands and then wiping them off on Danny’s cheeks. “Push against my cock, slave,” I ordered.

#### Cock Slave

Danny pushed back against me and the tip slowly slipped into his hole. I loved the sight of it going inside him, first just an inch, then two, then halfway in, then all the way until the base of my cock was nestled in the crack of his ass, thighs pressed against the cheeks of his muscular rear end. What a hot, sexy sight. I pushed, driving him forward on his hands and knees, pushing his face closer to Rebecca’s erect cock. As his face drew near, she took his head in her hands and guided his mouth onto the shaft. She waited for me. I began to gently pump. In matching rhythm, Rebecca let her cock slide in and out the manhole formed by his puckered lips. When I felt we were synchronized, I increased the intensity of my thrusts. Rebecca held his head tightly between her hands, stabilizing him.

![IMG_0242.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0242.jpg?lossy=1&strip=1&webp=0)

We went this way for a few minutes, an even, steady pace, the base of the dildo pushing the leather base pad against my crotch, the leather pad bumping against my cunt. It was dreamy, both the physical sensation and the sight of my cock sliding in and out of my beloved slave’s ass. And then I remembered the vibrator. I reached down and pushed the button. I felt a pleasant buzzing against my clit, a stirring, and resumed thrusting. Rebecca caught my eye and smiled; she nodded her head, signaling me to let go and enjoy myself.

And so I did, my thrusts increasing in tempo, now a trot, cantering toward a distant horizon, down orgasm lane, and then that hot summer feeling flooding over me, the tingle in the nape of my neck, the flash of electric current down my spine, like summer lightning, bringing a sense of urgency to get home, now galloping homeward, seeing my cock slamming against Danny’s ass, my hands on his hips, pelvis thrusting, his body mine, vibrator humming . . . and then, oh my god an explosion from my clit, radiating out through every extremity, knees weak, falling forward on his back, my chin on the groove of his spine between his shoulder blades, my full weight on him, gasping for breath, body flooded with endorphins.

I looked up. Rebecca’s face shined, it glowed; she smiled radiantly at me. She still had her cock in Danny’s mouth, still held his head firmly between her hands, the muscles of her arms taut and strong, those luscious breasts overflowing her bra; oh, my god what a sexy woman. Oh, my god, I had my shaft rammed inside a man. Heaven on earth, good will toward men. Merry fucking Christmas, indeed.

But Christmas is about giving, not just getting. I lifted my belly and breasts off Danny’s glistening back and pulled myself erect, slowly pulling my cock out of his ass. My shaft was clean as a whistle, still coated with lubricant. I looked up with satisfaction and said to Rebecca, “Your turn, sister.”

And then we swapped places on our slave.

### Part 24, Rebecca Rules – I

Saturday, January 11, 2015 (Part 1)

In my first diary entry, I wrote that the day I first put on the cock cage was the first time I felt like Mistress Vanessa’s slave. Yesterday was a day like that, a turning point, the first time I knew that Mistress Vanessa truly thinks of me as a slave. Over breakfast, she informed me that she was loaning me to Mistress Rebecca for the day. She instructed me to write a detailed account in this diary for her to read on Sunday. I’m writing in my cage by flashlight, Saturday night.

![IMG_0234.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0234.jpg?lossy=1&strip=1&webp=0)

After breakfast, she led me by the leash out into the January cold, stark naked, in broad daylight, crawling on hands and knees to her Civic, which she had backed up to the gate, She opened the trunk and ordered me to get in, on my stomach, She hog-tied my arms and legs behind my back and stuck a ball gag in my mouth. I was already wearing the cock cage. There was one other touch; lately she has taken to making me wear a butt-plug, she says to remind me of her cock up my ass. She no longer lets me have intercourse, she only fucks me with her strap-on. That, or she rides my face, her favorite dildo mounted on the face harness. The only time I’m allowed to cum anymore is Sunday mornings, after a spanking for my weekly infractions, If I keep it under 12 infractions, she lets me masturbate and cum into my dog dish, then makes me lap it up.

When we started this, I had no idea how much I would give up to gain a Mistress. Fantasies of female domination are one thing, but if you live them out (and I consider myself fortunate to so so), the irony is that you end up horny day and night, you hardly ever get to cum and you may never put your cock in a woman again. Instead, she puts her cock in you. You’ve begged her to treat you like a slave, and when she does, in time she comes to think of you as her slave, not a boyfriend or lover. You become her pet, her slut, her fuck toy. And as humiliating as all this is, you find yourself loving it, wanting nothing more than to please her, accepting whatever she chooses . . . including choosing other lovers.

#### Mistress Rebecca

![IMG_0257.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0257.jpg?lossy=1&strip=1&webp=0)

Specifically, Mistress Rebecca, her boss and mentor, fortyish, very beautiful, and as it turns out, with long experience with BDSM. At least it’s not a man. I think about that a lot, what if she took a male lover? We never discussed that, it wasn’t written in our contract, but the contract permits all things without restriction. I don’t know what I would do. It would be devastating. I think I might have to call it quits if there was another man in her life. Because in the end, when our contract expires and we decide about marriage, I want to be her man, the father of her children. Yes, I want to be her lifetime slave, and if the price of that is serving another Mistress whom she loves, I think I could live with that because after all, it’s a Mistress – a sacred goddess – not some guy off the street,

I know about cuckolding, how when a Mistress turns a man into her slave, she’s unable to think of him as her equal anymore, she can’t allow him the privilege of being her lover because he is now less than a man, he’s only a slave, so she finds male lovers. I hope and pray this doesn’t become the case with me and my Mistress. I would like to think that in due time, I can prove my devotion and earn my way back as her lover. To me, cuckolding makes more sense when a Mistress takes an older slave, as Mistress Rebecca has done with Hamilton, Maybe he can’t satisfy her the way a younger guy can. Maybe it’s just their deal, it excites him. But for me, someone close to the same age or even younger, as I am with  Mistress, especially if you are going to have kids, it seems to me that . . .

Honestly, I don’t know what I think, I adore her, I love being her slave, I guess she could do whatever she wanted and I would probably accept it, as long as I remained in her life. What does that say about me? What have I become? Some worthless slut, or a man? Is a slave something in between? Is there honor in absolute devotion to a goddess, a woman divine, trusting her to do what’s best for her and accept her terms, whatever they might be? If she took a male lover, would I really call it quits? Or is being her slave more important to me than my male ego?

I don’t know. I guess I’ll know if it happens. I do know that I want to be the father of her children. I want to be with her when she has the baby, I want to change diapers, I want to push our kid in the stroller and on the swing set, I want to go to school plays. I want to go to Little League games. I’m a nut case. She can have a male lover but I insist on raising the kids? Really?

#### The Future

All these thoughts were running through my head as the Civic bumped along the winter streets of Charlottesville. It was normally a 20-minute drive to Mistress Rebecca’s house, except we stopped on the way, Mistress Vanessa left me in the trunk and locked the car, with a beep for the car alarm. That made me feel a little better about not being car-jacked. It was a busy Saturday morning and I could hear car doors opening and closing, voices of people walking by, the normal sounds of a suburban shopping plaza, only there I was, a naked, collared slave, hog-tied in the trunk. Nothing more than property. The feeling only deepened as I waited for what seemed like an hour. I started to shiver. Eventually, I heard the door locks click, felt a moment of panic, fearing it was some stranger who happened to have the same key code. The trunk opened. “Hello, slave,” I heard Mistress’s voice.

Oh, my god, sunlight pouring into the open trunk, I was totally exposed in a public place, it was insane. I prayed Mistress had waited until there was no one nearby. She put some bags in the trunk by my feet. The situation hit me hard: I knew I was more than a car sack, but still, I had become a possession, property, something she stored in a trunk, a slave to dispose of or loan out as she pleased. And this was before we even got to Mistress Rebecca’s house.

There’s a two-car garage behind Mistress Rebecca’s house, accessed via a private alley with an electronic gate. I heard Mistress Vanessa pull up, open the car window and talk into the intercom. “We’re here, special delivery.” A metallic reply. I heard the gate hinge creak and we moved forward, wheels crunching on gravel. We stopped, I heard a garage door open, we moved forward, and then the door closed. I heard a side door open and close. The trunk popped open. I had that feeling on the back of your neck, eyes gazing down on me. The front door opened. I heard Mistress get out and her heels click as she walked around the car to join (I presumed) Mistress Rebecca at the trunk.

#### The Keys

“Here are his keys,” I heard Mistress say.” Those are your groceries. Sorry I took so long; Whole Foods was packed; the lines were a nightmare. I hope Hamilton’s mother is doing better.”

“She’ll be in the hospital a few more days, but Hamilton says she’s feeling much better,”

Hearing that exchange explained a lot. I was relieved; I don’t know how I would have handled being naked in front of another man, even if he was Mistress Rebecca’s slave, That thought had only occurred to me as we approached the house, and it freaked me out. I don’t have a shred of attraction for members of my own sex. I adore women. For me, at the heart of being a slave is goddess worship. If that wasn’t at the core of it, I wouldn’t have the slightest interest in being a slave. It’s all about women. Which begs the question, what if a Mistress has a stable of male slaves and you’re one of them? I was glad I wouldn’t have to deal with that issue today.

I got a glimpse of Mistress Rebecca out the corner of my eye; she was dressed more casually than I had seen her before, a sweatshirt and leggings, platinum blonde hair in a ponytail. I felt Mistress Vanessa unlocking my restraints. She unshackled the ball gag. “Get out, slave,” she ordered. My muscles were cramped and it took a second to climb out of the trunk. I kept my eyes down and got on my knees, not quite sure of which feet to face. Mistress Vanessa was wearing boots; Mistress Rebecca, stylish athletic shoes. Until I heard otherwise, I assumed I was under Mistress Vanessa’s authority. I turned and faced the boots. Good thing, because just then she snapped the leash to my collar.

“Here you go, he’s all yours for the day. Would you like help with the groceries?” Mistress said, handing the leash to Mistress Rebecca.

No, I’ll have your slave carry them in.” I felt a sharp tug on my collar. “Stand up, slave, get the grocery bags.” I clambered to my feet and collected the bags from the trunk. Four brown paper bags filled my arms. Mistress Vanessa caught my eye as I turned. “Daniel Simon, you are now Mistress Rebecca’s slave. You will obey her every command. If she is displeased, she may punish you as she sees fit. If she desires, you will submit your body for her pleasure. The only restriction is the one you already know—you may not orgasm under any circumstances. Is that understood?”

I understood, but I didn’t like it. Christmas Eve, that night of our threesome in the dungeon, Mistress Rebecca had proven a harsh lover. She fucked my ass hard, with none of the gentleness of Mistress Vanessa. It was a night I’ll never forget. My rear end was sore for a week.

“Yes Mistress, I replied, exchanging a brief but meaningful look before lowering my eyes.

Mistress Rebecca jerked my collar and led me away. As we walked into her house, I heard the Civic back out of the garage, wheels on gravel as the car rolled away. I felt abandoned, I dreaded what Mistress Rebecca had in store for me. Not that I’ve been in the military, but I imagine that’s what the first day of boot camp feels like. Anyway, I promised Mistress I would record everything that happened in detail, without editing. So here goes:

#### Assume the position

![IMG_0258.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0258.jpg?lossy=1&strip=1&webp=0)

The first thing Mistress Rebecca did was put me on my hands and knees on her kitchen floor and explain her rules. Nothing too different from Mistress Vanessa, except she said she was “old school” and that I was to acknowledge her and respond to orders with a simple, “Yes, Ma’am.” I suppose it is one less syllable. That, or she just liked the sound of it.

She informed me she preferred to contain my cock with a leather restraint instead of a cock cage. She ordered me to stand up with my hands behind my head. She unlocked the cage and with some difficulty pulled it off. Fortunately, my cock stayed attached. What I thought she had in mind was a leather jock strap or bikini pouch, but she had something entirely different in mind.

She produced a six-foot length of leather cord, and proceeded to expertly coil it around my scrotum, forming a tightly wound leather tube, so that my balls were squeezed into a tight sac at the end of the tube, about three inches from my groin. A single cord cut across the middle, so each ball was a separate hemisphere. The skin was as tight as a drum. She continued to wind the cord around my shaft, up to below the head, tying a knot there and running the free end up and tying it to my collar. When she was done, my dick was standing straight up and my balls stuck straight out.

I admit this was quite arousing and kinky, until she produced a slender rattan cane from a kitchen drawer. She grasped the suede handle and smacked the rod against her palm.

“This looks rather innocuous, slave, but you will soon learn to dread it. Let me show you why.” In the blink of an eye she flicked the cane sharply against my balls. I doubled over in excruciating pain. While I was bent over, she snapped it against my ass. It felt like a hot wire on my skin.

“I expect perfect obedience, slave, and this is my favorite instrument to get it,” she said, flipping the cane and pushing the handle into my mouth. “Not very heavy, is it?” It was only a few ounces. “Hold it like that while I put away the groceries. Just stand there, keep your eyes on the floor.”

#### Stay!

I stood motionless for ten minutes with the cane sticking out of my mouth, keeping my eyes cast down while Mistress Rebecca put her groceries away. It got increasingly difficult to keep the handle in mouth, but I dared not let it fall. Finally, she was done. She stepped in front of me and removed the cane, wiping the handle on the cheeks of my face. She jerked the leash down.

I dropped to my hands and knees. She led me into the dining room, a large room with a crystal chandelier where we had enjoyed Thanksgiving dinner. The table was polished walnut, with heavy legs, big enough to sit a party of twelve. Tall paned windows filled two walls, with views on the garden and the street. “You will be cleaning house today, slave, starting with this room, then working your way clockwise back to the kitchen. In each room, start from the highest point, dusting, then work your way down, cleaning every lamp and light fixture, polishing every piece of furniture, mopping the floors, vacuuming the throw rugs. If you finish on this floor, and I doubt you will, start on the second floor. There is a ladder and all the cleaning supplies you need in the storage room directly across the hall. Pay especial attention to the bathrooms. I expect every toilet to shine.”

“Yes, Ma’am.”

“I’m leaving for the gym and won’t be back for several hours, so you’ll be alone here in the house. There are cameras everywhere linked to the Internet, and I can check on you with at any time, so don’t let me catch you lollygagging. If you need to use the bathroom, use the servant bathroom on the second floor, it’s behind the gray door at the end of the hall. And one other thing— “she whipped the rattan cane across my ass with a stinging blow “—that’s what you can expect for every missed spot or speck of dirt, so I’d encourage you to be fastidious in your cleaning.”

“Yes Ma’am,” I promised, still wincing from the sting of the cane. Holy shit, spanking was a love pat compared to that wicked little rod. Mistress Rebecca unclipped my leash and strode out of the dining room. I crawled out into the hallway to find my cleaning supplies. By the time I emerged from the storage room with ladder, bucket, cleaners, etc., she was gone with a slam of the kitchen door. The leather cord around my cock already chaffed. I wondered how I would survive this day. I didn’t know which was worse, the tedium of cleaning an entire house or the sting of Mistress Rebecca’s rattan cane.

My hand is cramping and the flashlight is almost dead. I’ll finish at first light.

### Part 25, Rebecca Rules – II

January 11, 2015, Diary Entry, Part 2

When Mistress Rebecca returned home, it was past three. I was just starting on the second floor. I had been at it for over six hours, with nothing to eat, drinking tap water from sinks, my cock chafing like hell, the butt plug like a grapefruit up my ass. No way she had spent all that time at the gym. Maybe she went out to lunch with friends, or paid a visit to Mistress Vanessa. She might have driven to Richmond to see her husband. None of my business, I was glad she wasn’t hovering over me.

![IMG_0287.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0287.jpg?lossy=1&strip=1&webp=0)

She found me in the bathroom of the spare bedroom where Mistress Vanessa and I had slipped away during the Halloween party (I had to start somewhere). I was on my knees, cleaning the toilet when she came in. I dropped the sponge and turned to face her, “Good afternoon, Mistress Rebecca,” I said, hoping she was in a good mood.

Smack! Her rattan cane came down on my shoulders. “You have not earned the right to call me by that name, slave. I told you to address me as ‘Ma’am.’ It’s ‘Good afternoon, Ma’am,’ “

“Yes, Ma’am, good afternoon, Ma’am,” I said, trying to sound contrite.

“I bet you’re wondering where I’ve been all day, slave. Did you feel ignored?”

“No, Ma’am.”

“Well, go ahead and guess where I’ve been.”

“The gym, Ma’am?”

Whack! A slice across my shoulders. “Don’t be stupid, slave. I’ve been gone six hours.”

“I don’t know, Ma’am.”

#### My Mistress’s House

“I’ve been at your Mistress’s house, slave. It was nice having the place to ourselves. We spent the afternoon fucking in her bed. Your Mistress loves it when I stick my cock deep inside her and fuck her long and hard. I bet you miss that, don’t you, slave?”

“Yes, Mist—I mean, yes, Ma’am.”

Whack! The cane snapped against my side. “You’ll learn yet, Slave. And you’ll learn your place, too . . . certainly not in your Mistress’s bed, that belongs to me now.” She clipped the leash to my collar. “Drop what you’re doing and follow me, slave.”

![IMG_0289.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0289.jpg?lossy=1&strip=1&webp=0)

She held the leash tight so I was forced to stay close to her leg. I noticed she was wearing brown moccasins and black tights, a change from the morning. I followed her on hands and knees through the bedroom, down the hall and into the master bedroom, which was huge, with bay windows and a sitting room, a massive bathroom with a jacuzzi, twin sinks set in marble counters, toilet, bidet, shower and separate bathtub, a deluxe version of the antique tub at Mistress Vanessa’s house. “I like my bath water hot, slave,” Mistress Rebecca said. “Start the water, then undress me.”

I cranked the hot water, ran it over my hand until it was scalding, fed in some cold water until it was hot but not burning, and plugged the drain. The tub started to fill. I turned to face Mistress Rebecca’s feet, then stood up, getting in a glimpse before casting my eyes down. She wore a different top than the sweatshirt she had on this morning, too. It was a gray knit sweater with faint gold heart in the center that I recognized was  Mistress’s. So, she had been over to the house.

She held her arms out from her sides and regally waited for me to do something. I felt myself growing hard. Undressing a strange woman is bizarre, erotic but disorienting; you have no right to be in the same room with her, let alone undressing her.  I lifted the sweatshirt up from the bottom seam, over her head to pull the arms free. While her head was enveloped I got a good look at her breasts. Holy shit. She was wearing a lacy black bra cut diagonally that showed off the cleavage and it was like a piece of art. I like smaller breasts, but art is art. The tights were next. I peeled them down to the moccasins, helped Mistress Rebecca step out of them, and then tugged the tights off her feet,

Her panties fell around her ankles. I helped her step out of them. “You may kiss my feet, slave,” she said, Her bra dropped on the floor next to my face while I was kissing the tops of her feet. She stepped away and put one foot in the tub, testing the temperature, then the other foot. She settled in the tub with a sigh, the water still running. I collected her clothing and put it up on the countertop, trying to be discreet and keep my cast eyes down, but my brain was buzzing and my cock hard.

“Come over here and scrub me, slave,” she said, a frilly pink mesh nylon scrubber in one hand and a bottle of bath soap in the other. I’ll cut it short here and just say that giving a woman a bath is an incredibly intimate act, and I wished I was doing it with my own Mistress, but she sent me here so I had to believe that whatever I saw or did, she wanted me to experience it. Maybe it was to help me understand her attraction to Mistress Rebecca. I got it.

After the bath, I dried Mistress Rebecca off with a luxurious cotton towel. Then she ordered me to crawl to a corner of her room and face the wall, I heard her dressing. When she was done, she ordered me to turn around and look up. Oh, my god, she was in full dominatrix attire, thigh-high black boots, a black leather corset and a strap-on with a purple dildo, a different cock than the black one she had used on me in the dungeon on Christmas Eve.

“Crawl over here and suck my cock, slave.”

#### Suck it Slave!!

I crawled over, raised up and put her cock in my mouth. She grabbed the back of my head and shoved it as deep as it would go, then pulled it out halfway and began pumping in and out. She went at it for a minute, making me gag a few times, until drool was running down my jaw. She stopped.

![IMG_0290.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0290.jpg?lossy=1&strip=1&webp=0)

Just so you know, this is the cock I fucked your Mistress with this afternoon, slave,” she taunted. “I’m going to fuck you in the ass with it, but first you need to be punished for the dust and smudges you missed downstairs.” She short-leashed me and led me down the hall to a door at the end, then up a set of unfinished wooden steps to the attic. The room was huge, a fully equipped dungeon.

She led me to a stock-like contraption made of oak, a headboard with vertical rails on the sides, married to an angled bondage sawhorse with knee pads, sturdy black leather restraints on each pad. “Get on the bench and lay your neck and wrists in the cutouts, slave,” Mistress Rebecca ordered.

I mounted the padded bench and put my knees on the side pads, a configuration which exposed my ass and cock for easy access. I laid on my chest and put my neck on the center cutout, wrists on the smaller side cutouts. She inserted a board into the vertical rails and slid it down until the cutouts for the head and wrists mated. She hooked side latches and now my head and hands were locked in place, sticking out of the stocks. On the bench side, she cinched down the restraints around my thighs and ankles. I was helpless, completely immobilized, more than anything I’ve ever experienced.

“This is going to hurt, slave, and I don’t want to hear you scream like a girl,” Mistress Rebecca said into my ear, just before she shoved a ball gag into my mouth and buckled the harness around my head. For the first time since I entered the world of BDSM, I was legitimately terrified. And for good reason. Mistress Rebecca gave me a whipping with her rattan cane that had me screaming into the gag and brought tears to my eyes. If I had could have broken free, I would have jumped up and thrown her to the ground and slapped her senseless. My body was jerking violently with every lash.

“That will teach you to be more thorough the next time you clean my house, slave,” Mistress Rebecca said when she finished. Then she pulled out the butt plug. I knew what was coming next.

#### Just when You though you knew…

Or thought I did. I was not prepared for her to unravel the leather cord wound around my dick and begin stroking it, as she stuck her fingers up my ass. Both her hands were lubed, and as much as my butt and back were singing out in pain, I still quickly got hard. While she was stroking me, her fingers probed inside my ass, not sliding in and out or circling the sphincter to loosen me up, but more like a doctor giving a prostrate exam. What was she doing? I soon found out, because she told me.

“You know Mistress Rebecca’s one condition for loaning you out to me, slave. You may not have an orgasm, correct? . . . Oh, poor thing, you can’t answer me, can you? Well, we both know that’s the rule. So just to make sure you don’t have an orgasm while I fuck you, I’m going to milk all the cum out of you before we start. I don’t suppose you’ve ever had this done before, have you, slave? Well, it’s painless. Just be sure to wiggle your toes when you feel like you are about to cum.”

![IMG_0235.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0235.jpg?lossy=1&strip=1&webp=0)

Of course, being gagged, I couldn’t answer, and I didn’t even know what she was talking about, She stroked my cock and massaged my prostrate, and it wasn’t long before I felt a powerful orgasm building, the stored cum of an entire week. The last time I had ejaculated was the previous Sunday morning. In less than a minute, I felt close to exploding, I began furiously wiggling my toes.

“Ah, good, that’s what I wanted to see, dear,” Mistress Rebecca said in a condescending tone. She stopped stroking my shaft and held it firmly, continuing to massage my prostate through my ass, Then, without any sense of sexual release, I began to have this awful, slow-motion ejaculation that felt like a short-circuit. I felt a steady flow of fluid discharging from my cock. An un-orgasm.

“If you wondered, slave, that’s your cum pouring out. I’m milking you, and oh my, there’s such a lot of it. Good thing I have a bowl to catch it all.” The milking went on for a minute, until the flow dried up, and with it, all the stored sexual energy of a week of denial and torment, my yang, my only reply to the yin of female power. Gone. Now what was there to look forward to Sunday?

Mistress Rebecca moved around the stock board so that I could see her. She held a glass bowl in her hand, with a pool of cum on the bottom. “I suppose you think I’m going to pull out your gag and make you drink this, don’t you slave?” she said. “That’s what Mistress Vanessa makes you do every Sunday morning, doesn’t she? Look at me and nod your head, slave.”

I made eye contact and nodded, humiliated that she knew everything, even this. But if she wanted me to swallow all that cum, I wasn’t going to resist. I would do whatever she wanted.

#### Whatever Mistress want’s Mistress Gets!

“Well, that’s not the plan, slave. I’m going to syringe this into a storage cylinder and it will go to the sperm bank first thing Monday morning. And if you are wondering, your Mistress approves. Your sperm belongs to her, and since you’ll never fuck her again, I’ve convinced her that she needs to collect a supply of sperm so when she wants to have children, she can be artificially inseminated.”

Before I could even react, Mistress Rebecca turned and stalked out of the room. She was gone for about five minutes. I wanted to cry. I was totally defeated, But it wasn’t over yet. When she returned to the dungeon, she came up behind me, slapped my butt hard, and then stuck her fingers up my ass. This time she ignored my prostrate, making a few quick thrusts to insert some lube, then pulled her fingers out. A moment later I felt the tip of her cock against my asshole, which involuntarily squeezed down tight in fear. I knew I needed to relax and let her cock slide in, but I just couldn’t do it.

And now bitch,” she said, “I’m going to fuck you hard, like you’ve never been fucked before.” With a single thrust, she shoved her shaft in all the way, ramming it to the hilt. I screamed against the gag. It was rape. Consensual rape, I suppose, I had consented to whatever Mistress Vanessa wanted to do with my body . . . it just never occurred to me that it would be another woman doing it.

I don’t know if Mistress Rebecca came or just got tired, but eventually she stopped. I had lost track of time, going inward in my mind, a place of refuge. I suppose that’s what women being raped do, they disassociate. I felt the cock come out of my ass and then there was silence. I realized that I was alone in the dungeon. A while later, Mistress Rebecca returned, now dressed in blue jeans, boots, and a leather jacket. She released me from the restraints, ungagged me, ordered me to stand and led me by the leash, down the stairs, the hallway, a second flight of stairs to the ground floor, through the kitchen and out to the garage. She opened the trunk to her red BMW coupe.

“Get in, slave, you should just fit,” she said. I crawled in. She looked down on me thoughtfully. “I know this has been a hard afternoon for you slave, but it was necessary for your training. You are no longer a man, you are a slave, and you must learn to think of yourself that way. Your Mistress told me you are keeping a diary. Spare no detail, write it all down. Mistress Vanessa trusts me and knows I have her best interests, and yours, at heart. Before I close the trunk, you will thank me, and when you do, you may address me as Mistress.”

Not at all convinced of her good intentions, I said the words, “Thank you, Mistress Rebecca,” She rewarded me with a genuine smile and slammed down the lid.

On the drive home, curled in a fetal ball in darkness, it felt like I was in a womb, moving from one lifetime to the next. I found myself feeling strangely proud for having earned Mistress Rebecca’s acceptance, the privilege of calling her Mistress. When we arrived home, Mistress Vanessa was waiting to receive me. She took my leash and led me inside, a man surrendered, now only a slave.

### Part 26, To Love and Honor

![IMG_0127.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0127.jpg?lossy=1&strip=1&webp=0)

“Very interesting reading,” I said to Danny, who knelt at my feet at the dining room table. I put the leather volume down on the table. The room was hushed; I heard my father’s clock ticking in the living room. Dust motes swirled by the window, riding invisible currents of a sunny Sunday morning. I watched them spin, thinking about how to respond to the moving narrative I had just read. “You write very well, slave,” I said, caressing Danny’s neck. “You’ve never written before, have you?”

“No, Mistress. Just some stuff in high school. I spent most my time in the machine shop.”

“Well, you have a talent.”

“Thank you, Mistress.”

The ticking of the antique wall clock made me think of my father, always a gruff, stern disciplinarian. Being the first child, I suffered the brunt of his childrearing errors. In time learned to please him, to read his moods. Before long, all it took was a glance. Being the eldest, it fell to me to help care for my three baby brothers. I was tough but fair, like Dad, but with a feminine touch, leveraging the intimacy of being a fellow prison inmate. My brothers adored me. The boys of the neighborhood families that I babysat for adored me, too. I learned I could get them to do almost anything I wanted. Mistress training.

#### Mistress Training

I had never wanted a slave, but now a slave crouched on the hardwood floor at my feet, a man who would give up everything, even his manhood, for the privilege. The remark Danny made about boot camp stuck in my mind. Having never been in the military, never been in boy scouts, he had missed all those coming-of-age, manly tests. Racing in a pool was something girls did, too. I wondered how much of all of this was his way of experiencing something that tested his masculine being to the limit, a vision quest of sorts, a way to fill the gaps in his soul.

There were obvious gaps in his upbringing, the biggest gap being forever lost to time, the four years he spent bouncing around foster homes in Missouri before an older couple adopted him, took him to Florida and a new life. I had quizzed Danny about that period of his life when I learned in his first diary entry that he had been adopted. He knew almost nothing. The birth record had been lost to a flood; all he knew was that he had been born in a small town named Clarksville on the Mississippi River.

I looked down on Danny’s prostrate form, mentally wincing at the welts on his back and shoulders. His ass was crisscrossed with fine red lines. Rebecca had come just short of making him bleed. We had three more months in our contract; I wondered if he could survive them, survive the extreme physicality of Rebecca, who was committed to pushing him to the limit, more for her sake than mine, to make sure he was worthy of being my slave, to make sure he wouldn’t bail out on me when things got difficult.

It seemed to me Danny had been tested to the limit yesterday afternoon. By his own admission, he had returned home a slave, his manhood stripped away, not knowing that I had no intention of getting pregnant by artificial insemination, or that when our contract ended in April, and if we decided to get married, we would resume being lovers again. That it, unless Rebecca’s fears were realized and he ran for the hills. I loved Rebecca, but I knew my Danny. I didn’t think he would run. She said fine, but he didn’t need to know any of this for now, not while he was being tested.

I loved and trusted her, and I wanted her in my life, hopefully forever. I told her about my love for Danny, his childhood, his adoration for me, the love in his heart . . . he wasn’t some guy I found in a bar who wanted to be dominated, he was the man I had fallen in love with before I knew anything about the world of domination and submission. He had introduced it to me, not the other way around. I didn’t think I needed to test his devotion, and while I now thought of myself as a Mistress, the truth was, if it wasn’t with Danny, I didn’t know that I would ever take another slave. Maybe, but not likely.

#### All I Need

![IMG_0291.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0291.jpg?lossy=1&strip=1&webp=0)

Rebecca listened to all this as we lay in my bed yesterday afternoon, slated with our lovemaking, spooned against my back as I rattled on, not saying a word, just listening. Oh, how I love the feeling of her body pressed against mine, more than Danny’s. How interesting that with him, I love to be the one spooned against his back. The quirky power dynamics of spooning, I suppose. When I was done, Rebecca devised the plan which unfolded at her house yesterday, including milking Danny of his sperm. Dumb bunny, he had no idea that sperm banks won’t take frozen sperm; he’d have to go to clinic and sign a release, masturbate in a sterile room and hand his cum in a paper cup to a nurse.

Rebecca had explained all this to me. When she was in her twenties, she had been a nurse; in fact, she had been a nurse supervisor at the local hospital for years before switching careers and getting a degree in finance. She still had contacts at the local fertility clinic, and could easily arrange for Danny and I to make an appointment, even if it was only to save his sperm for posterity, for the unlikely event of his death, or if I had problems getting pregnant. Then she explained about milking the prostrate, that she did it to Hamilton when she wanted to punish him, deny him an orgasm, and that it was much more erotic than masturbating at a fertility clinic.

As first I was skeptical, but eventually I agreed to Rebecca’s plan. She said he should prove himself over the long run, that I should strip everything from him and establish a total slave identity, one that would endure after he was given more freedom. She said it was what she had done with Hamilton, only with him it was stripping away his substantial financial holdings, the accumulated wealth of a lifetime. He had been married and divorced, his kids were fully grown, so wealth and property were the things most integral to his manhood. When he gave that up, he gave up his manhood in favor of slavery to her. Danny had debt, not wealth, so it was his fertility that defined his manhood. Rebecca said that we had three months left and he should emerge having made his choice, trading his manhood for slavery to me.

#### Boot Camp

I knew from Dad that Army boot camp was three months long. How ironic (or maybe serendipity) that Danny had written in his diary that being at Rebecca’s felt like boot camp. So maybe she was right. No—she was right, once again. The next three months would be Danny’s slave boot camp, orchestrated by two women, one who loved him dearly, the other who loved the one who loved him, and wanted to protect her. Together, we would train him to be an obedient slave, train him so well that like my dad and the army, when he finished boot camp, his identity as a slave would be engrained in his bones.

I looked down at Danny’s stainless steel dog bowl. He had scarfed down his oatmeal like someone who hadn’t eaten for twenty-four hours, which was exactly the case; I had led him up to his cage last night and locked him in without feeding him, leaving him with his pee bottle, the diary and a flashlight and instructions to have his account ready by morning. My pet had been ravenous after a long, eventful day of cleaning and sexual torment, then fasting and writing. I was proud of him. He wrote well.

“Slave, even though I had Mistress Rebecca milk you yesterday, I’m going to reward you for the fine job you did in your diary, and let you masturbate this morning. Would you like that?”

A moment of silence. The clock ticked on. I wondered if Danny was calculating whether he had anything left in him, if he would embarrass himself and not be able to get it up.

“Yes, Mistress, I would like to try.”

“Good, only this morning I’m going to give you a special treat. Stay, pet. I’ll be right back.”

I got up and went to the bathroom, took the Vaseline from the medicine cabinet. Then to the closet, where I opened my Mistress’s valise and took out our contract. Back to the dining room, where Danny crouched, waiting. I set the contract on the table, signature page up, then opened the Vaseline and scooped a generous portion into my hand. I positioned myself behind him. “Up on your knees, slave.”

#### On Your Knees Slave!

![IMG_0258.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/12/IMG_0258.jpg?lossy=1&strip=1&webp=0)

Danny rose on his knees, his flaccid cock positioned over the steel bowl. I moved close behind him, dropped down to my knees, straddling his legs, my chest pressed against his back. I reached around his waist and took his cock in my hands, spread the lubricant around, began to stroke him. “Poor slave, you had such a hard day yesterday, didn’t you, it felt like the first day of boot camp, didn’t it?”

A sigh escaped his lungs. “Yes, Mistress.”

I pressed my crotch against the welt-laced cheeks of his masculine ass. “And Mistress Rebecca fucked you so hard, didn’t she, my poor baby is very sore in the ass now, isn’t he?”

Another sigh. “Yes, Mistress.”

“But I told her whip your ass, slave. I told her to fuck you hard. She did it for me.”

Danny’s cock perked, growing ever so firmer in my hands. Oh, the power of words. I remembered that night with Rebecca, the way she seduced me, the spanking and her expert fingers in my vagina, but more than that, it was her words, her voice in my head . . . that was the power.

Danny was breathing deeply now. I kept stroking him. “You were glad to suffer for your Mistress, weren’t you slave? You took Mistress Rebecca’s cane and her cock in devotion to me, didn’t you?”

“Yes, Mistress,” Danny groaned, his cock growing erect. I loved how he responded to my voice.

“And she milked all your cum, didn’t she? Were you afraid I’d be mad at you, slave?”

“Unghh,” Danny moaned, a hesitation in his breath. Then a tortured, “Yes, Mistress.”

“It’s okay, slave, I’m not mad. I told her to do that; it wasn’t really an orgasm, was it?”

“No, Mistress . . .” he said, almost sobbed, his voice tapering off into a sorrowful hiss.

“I told her to collect your sperm, slave. After all, it belongs to me, doesn’t it?”

He replied in a stronger voice, “Yes, Mistress.” His cock was fully rigid now, the shaft arched up, extending well past my hands. I missed having it inside me.

“And you understand why I can never let you fuck me again, don’t you slave?”

Silence. Pre-cum dribbled out the opening of Danny’s glans. I held his shaft in my left hand and slapped my hand hard across his member. He flinched. But still no reply.

“It’s because you are less than a man now, Danny, you are a slave, and I simply can’t have a slave fucking me, it wouldn’t be right. You’re lucky I took a woman for my lover, it could’ve been a man. Do you realize how lucky you are I found a woman to satisfy me?”

The reply was almost immediate. “Yes, Mistress.”

“And you know I love you. I read what you said about wanting to take care of our children, how you want to be a good father. I love that you wrote that, it was very sweet.”

“I meant it, Mistress,” Danny gasped between breaths.

“I know you do, and I appreciate it. But you understand I can’t have this slave’s cock—” I squeezed down hard on his shaft and stopped my stroking, “—I can’t have a slave’s cock inside me. That honor is reserved for my lover, for an equal, for a Mistress. Mistress Rebecca. You understand, don’t you?”

#### Understood

We were motionless for a moment, my body pressed hard against Danny’s, hands gripping his cock, his breathing subsiding. It was like the universe went on pause for a moment. “But I want your sperm, slave, I want my children to come from you. That’s a great honor, isn’t it?”

“Yes, Mistress.” The earth started spinning again. I resumed stroking his shaft.

“Slave, our contract goes another three months, and during those months we’re going to collect as much of your sperm as we can, to make sure I can get inseminated when I decide I want to have a baby. That wasn’t in our contract, but you agree to it now, don’t you?”

I felt Danny’s knees growing weak. “Yes, Mistress,” he said in surrender.

![IMG_0172.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/11/IMG_0172.jpg?lossy=1&strip=1&webp=0)

The contract was on the table directly in front of us. I reached out and snatched it up, placed it on top of Danny’s bowl. “Good. I have your word, but we’ll seal it with your cum. I want you to squirt some on our contract.” I resumed stroking Danny’s engorged member, which now quivered in my hands, on the cusp of an ejaculation. “If you disagree, tell me to stop now, slave.”

I knew Danny was deep into subspace, and maybe I was taking advantage of him, but he was the captain of his own soul. When Rebecca had done this to me, I had found the will to stop, because deep down inside, I am a Mistress, born to lead. If Danny was a slave, born to follow, he would cum.

“Speak now or forever be my slave, Danny,” I purred into his ear, stroking him faster. “If you cum, it means you know you can never fuck me again, that your sperm belongs to me. If you cum, it means you submit yourself to Mistress Rebecca, you accept her as my lover, that you will love and honor us both.”

Danny’s knees buckled.

“Beg me to let you cum because you agree to those terms, slave.”

“Yes, please, Mistress . . . I beg you, oh god, please let me cum.”

That was what I needed to hear. Three strokes later Danny came, an impressive squirt that made up with velocity what it lacked in volume. The cum shot out and arced down to the contract, landing below our signatures, spreading out into a half-dollar sized stain, the perfect mark to seal my slave’s fate.

Later, I asked Rebecca to come over. After we made love, I explained everything to her, and she was pleased, which made me happy. I gave Rebecca one of Danny’s faded blue work shirts, of which I have several and love to wear around the house, comfort clothing. Naked except for the shirts, and with the contract and pen in hand, we padded barefoot down to the dungeon, where I had suspended Danny.

He looked magnificent, his hands cuffed to the overhead hanging bar, ankles cuffed to the spreader bar, balanced on his toes, back arched, body straining, suffering while his Mistresses made love upstairs, directly above his head (oh, serendipity!), so that he could hear just enough to imagine.

Such sweet suffering.

Mistress Rebecca took his balls in her left hand and held his beautifully erect shaft in her right hand, and I listened with pleasure as my slave made a vow to love and honor her as his Mistress and my lover. And then, as Danny watched, she held the contract against his chest and signed below his cum stain.

I put the contract between his teeth while Rebecca and I sealed our new arrangement with a long, passionate kiss. We were now officially a threesome, two Mistresses and a slave.

A somewhat (okay, very) unconventional arrangement, but I had a feeling, call it woman’s intuition, that it was all going to work out just fine.

Next: Daddy’s Girl

### Part 27, Daddy’s Girl

Hamilton’s mother was dying of bone cancer. Her doctors gave her three to six months to live, but she refused to move into a nursing facility or hospice house. She wanted to die at home. Her husband has mild dementia and couldn’t be trusted with her care. At Rebecca’s insistence, they hired a full-time live-in hospice nurse. Ever the good son, reliable husband and adoring slave, Hamilton moved in with mother to care for her to the end. The day he left for Richmond, Rebecca commented that the hardest thing about leading a kinky life is that life gets in the way.

I have married friends who say the same thing about their sex lives and kids.

![IMG_0253.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2017/01/IMG_0253.jpg?lossy=1&strip=1&webp=0)

With Hamilton in Richmond, my slave’s visits to clean Rebecca’s house became a weekly ritual. Saturday morning after breakfast, I would lead him on his leash out to the Civic, put him in the trunk, hogtie and gag him, and then drive to Rebecca’s, stopping on the way to do my weekend shopping. Usually it was just for groceries at Trader Joe’s or Whole Foods, but sometimes there were two and three stops. My slave never seemed to complain. Maybe it was the gag.

#### Housecleaning Services

Rebecca grew to count on Danny’s housecleaning services. She’s a harsher, more sadistic Mistress than me; he always came home with mean-looking welts across his body, marks that made me cringe. Our agreement was that she would not make him bleed, and she kept her word, if only barely. She had her strap-on playtime with him when his cleaning chores were complete (never to her satisfaction) but turned over the harvesting of his semen to me, a task which I easily incorporated into our Sunday ritual. I would alternate, allowing him lick up his cum one Sunday (if he hadn’t gone over his  demerit count), and the next Sunday, I made him ejaculate into a sterile container.

But just to make it real (and why not?), Rebecca arranged an appointment with a former colleague who ran the local fertility clinic/sperm bank. The three of us showed up one Wednesday at lunch and signed forms together. Danny was our sperm donor. If something happened to him, I was the recipient and legal guardian of his sperm. We signed him up for six visits, starting mid-February. The donation took place in an airless, sterile room, and their porno collection was laughable. Even so, Danny was probably the happiest donor to ever walk through their doors, thrilled at the prospect of mid-week release. He was certainly the only donor with a cock cage—the kinky part of the visit. Rebecca set it up so Danny had to ask her nurse friend for the key each time, and then return it. And she knew why.

Three months flew by, and soon it was April 15th, Tax Day and the day our contract came to an end. In preparation, I prepared a financial statement. I had paid off all of Danny’s credit cards and closed the accounts. He was now debt free, with 6,046 dollars in a savings account. I wrote up a release for the savings account and the 2018 dollars in the joint checking account. I wrote up an agreement that gave him title to his truck for one dollar. Finally, there was a document which released any claim to his sperm at the clinic.

#### Free Danny

I put that set of documents in the ‘Free Danny’ folder (which I labeled, ‘FREE’). In the other folder (‘SLAVE’) was a single sheet of paper, a new contract, based on the original. It read:

“I, Daniel Simon Barton, being of sound mind, enter this agreement freely, without any mental reservation or purpose of evasion, to develop my character and to obtain the intellectual, moral, physical and spiritual benefits of a female led marriage with Ms. Vanessa Prudence Whetstone.”

The second paragraph was close to the original contract, only with two life-altering revisions:

“I, Daniel Simon Barton, hereby enter bondage as the slave of Ms. Vanessa Prudence Whetstone. This agreement shall be binding on both parties, and exist from the date of signing, April 15, 2015, in perpetuity. Under the terms of this agreement, Ms. Whetstone shall have full authority over me in all matters, private and public, encompassing all human activities, without restriction. This authority may be granted from time to time to Mrs. Rebecca Ray Fowler Hamilton, Ms. Whetstone’s fellow Mistress. Nothing shall be excluded from the scope of Ms. Whetstone’s authority, which is absolute.”

The third paragraph was condensed, the details of BDSM sexual practices now a given:

“I, Daniel Simon Barton, agree that Ms. Whetstone has total power over my sexual activities, any form of sexual release, specifically orgasms, and may practice female domination in all its forms, may cage, defile, humiliate or punish me, with or without cause, as she sees fit.”

The fourth and final paragraph was a life sentence into female-dominated slavery:

“I agree to Ms. Whetstone’s complete authority over all my daily activities, the use of my time and planning future events. I agree to her authority over my finances, that all my earnings and any savings belong to Ms. Whetstone, that she will issue me a weekly allowance, the amount set at her discretion, and that purchases of 40 dollars or more must be approved in advance. I agree that Ms. Whetstone owns and may dispose of all and any property in my possession. Finally, I agree that Mr. Whetstone has sole rights to my sperm and may inseminate herself with it at her sole discretion for the purposes of having a child. I agree that she has sole custody and is the legal guardian of any child so conceived with my sperm and that the child will take her last name, Whetstone. In agree that in the event of my death, she inherits all my earthly possessions, financial interests, intellectual rights and physical property.”

----------

Mr. Daniel Simon Barton

----------

Ms. Vanessa Prudence Whetstone

----------

Mrs. Rebecca Ray Fowler Hamilton

Signed this ___ day of April, 2015.

![IMG_0133.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2017/01/IMG_0133.jpg?lossy=1&strip=1&webp=0)

#### Contract

I knew the contract wasn’t legally binding in a court of law, but it was morally binding, and as far as I’m concerned, that carries more weight in a world where legal documents often are less reliable than a fly, where men and women lie to each other about what they truly want; they marry phantoms and then are dismayed when it all falls apart, leading to a national divorce rate of 50%. Our agreement would be honest and real, the foundation of a female led marriage, and if Danny agreed to it, I thought we had a chance of enduring happiness, if not in perpetuity, then say, 40 years.

40 years is long enough in my book. Not many people have a chance at that kind of happiness.

April 15th 2015 being a Wednesday, that morning, while he ate his breakfast from his bowl at my feet, I told Danny we would have to put off formal discussion of the end of our agreement, and the beginning of a new agreement, until Saturday. Was he willing to extend his slavery until noon Saturday? Of course, he agreed. I sent him off to work, telling him to think long and hard over the next few days about what he wanted to do, and plan on spending Friday night in his cage. It might be the last time.

Saturday morning, for the first time in months, Rebecca came over to our house. Danny served us breakfast in his cute maid apron, and then he ate from his bowl at his place by my feet. When we had eaten, I took away his bowl, brought out the two envelopes and placed them on the table. Rebecca went through them first, reading carefully. “I’m prepared to sign,” she said, smiling.

I put the documents back in the envelopes.

Now it was Danny’s turn. I ran my fingers through his black hair. It was long again, the way I liked it. “Slave, you have a very important choice to make this morning,” I said, placing the 8 x 11” envelope labeled ‘FREE’ on the floor. “This choice returns you to the freedom you enjoyed from the day we first met until the day we signed our agreement, which as you know, expires at noon. You’ll note that you are now completely free of debt, you have over six thousand dollars in savings, about two thousand dollars in checking, and you’ll get back clear title to your truck.”

Danny looked over the documents in less than thirty seconds.

I placed the second envelope in front of him, labeled “SLAVE.”

#### Slave

![0WBB2B.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2017/01/0WBB2B.jpg?lossy=1&strip=1&webp=0)

“This choice makes you a slave for life. The contract inside describes a female led marriage, in which I own and control everything, including you. If you sign it, I will marry you. I will be your Mistress above all else, and your wife only for legal purposes. Also, if you sign this contract, it gives me all rights to your sperm, and I plan on having a baby soon. We would have our wedding in July. Go ahead and read.”

Danny pulled out the sheet of paper. He spent three long minutes reading it. I wondered what he was thinking. I resisted the urge to stroke his back. I so wanted his cock inside me, but he was going to have to agree to terms that stripped him of absolutely all rights, terms that made him my slave forever, and then slowly discover that I was a loving Mistress who granted privileges and freedoms as a reward for good behavior. And God knows Danny was a good man, inclined to good behavior.

“Well?”

“May I speak freely, Mistress?”

“You may.”

“Mistress, you know I love being your slave. I love being in your home. I love making a living and knowing that it goes to you. I love knowing that you’ll make the important decisions. You are a smart and beautiful woman, and you are blessed with the wisdom and love of Mistress Rebecca. The two of you are always going to make wise decisions . . . decisions that are the best for everyone.”

I looked at Rebecca. I could tell she was pleased to hear Danny’s respect for her spoken aloud.

“I only have one modification . . . a request if I may . . . a change to the contract.”

“And what is that, slave?” I asked, sincerely curious, thinking it better not be an exclusion about cross-dressing or not wearing ladies’ underwear.

“You know I was adopted, Mistress.”

“Yes, of course I do.”

“I don’t know the name of my biological mother or father. I love my adopted parents, but as you know, we aren’t all that close and I don’t feel any deep attachment to my adopted name, Barton.”

I looked over at Rebecca. She shot back a quizzical look. “So what are you saying?”

“I’m saying that if we get married, I’d like it if we had the same name. For the child’s sake, for my sake, I would like it if I took your name, Whetstone.”

#### Mr. & Mrs. Whetstone

I stared at the back of his head, tears welling in my eyes. Oh, this man, this man. I looked across the table at Rebecca. She shook her head incredulously, meaning ‘yes, take this guy, he’s the one.’

“Rise up, slave, on your knees, face me.”

Danny lifted off the floor and turned to face me. For once he was flaccid in his cage, and it didn’t bother me a bit. I took his face in my hands and kissed him. “Daniel Simon, will you be my slave?”

He gave me his happy, boyish grin. “Mistress Vanessa, I would love to be your slave forever.”

We signed the document, the three of us, and then I led Danny upstairs by his leash to his cage and locked him away with his diary. “I want you to write about this day, slave. I want you to write about the last six months and what it meant to you, and what you hope for the future. And while you’re writing that, Mistress Rebecca and I are going to start planning our wedding.”

No happier slave has ever said the words, “Yes, Mistress.”

We were married in the Great Meadow at Shenandoah National Park, on a sunny afternoon in July when the sky was the palest shade of blue and tall thunderclouds decorated the Blue Ridge Mountains. It was a small ceremony, the four of us and close friends, a party of twelve. Hamilton was Danny’s best man; Rebecca was my maid of honor.  We exchanged vows in front of a Lutheran minister, a favorite of gay and lesbian couples in Charlottesville. It seemed fitting, since ours is an alternate lifestyle marriage. Danny got three platinum rings, one for each nipple (the piercing ceremony took place that morning), and the larger one that I slid on his ring finger. And yes, he legally changed his name to mine.

![IMG_0293.jpg](https://858472.smushcdn.com/1867514/wp-content/uploads/2016/08/IMG_0293.jpg?lossy=1&strip=1&webp=0)

That night we stayed at a lodge on Skyline Drive, just the two of us. We stayed up until midnight, enjoying a small band that happened to be singing downstairs on a one-night stand. The lead singer reminded me of Danny Schmidt, the same songs of life, the bittersweet lyrics. Yes, it’s a hard road to find a good man, a hard road to keep a good woman, and a hard road to raise a good child in this crazy world we live in, but Danny and I agreed it’s worth a try. We did it the new-fashioned way, me on top.

Our daughter came into the world eight months later, a month early, hell bent to get out of the womb, six pounds seven, and wouldn’t you know, a daddy’s girl, born on his birthday, March 21st.

We named her Daniella Prudence Whetstone.
